#include "EngineXmlConfig.h"

#include <QDebug>
#include <QMessageBox>
#include <QFile>
#include <QDir>
#include <QFileInfoList>
#include <QMessageBox>
#include <QSharedMemory>
#include <QSystemSemaphore>
#include <QDateTime>
#include <QDebug>
#include <QtNetwork/QHostInfo>
#include <QtNetwork/QNetworkInterface>
#include <QProcess>
#include <QXmlStreamWriter>
#include <iostream>

EngineXmlConfig::EngineXmlConfig()
{
}


EngineXmlConfig::~EngineXmlConfig()
{
}
QString EngineXmlConfig::getMyIp()
{
	QString ip = "";
	QProcess cmd_pro;
	QString cmd_str = QString("ipconfig");
	cmd_pro.start("cmd.exe", QStringList() << "/c" << cmd_str);
	cmd_pro.waitForStarted();
	cmd_pro.waitForFinished();
	QString result = cmd_pro.readAll();
	QString pattern("[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}");
	QRegExp rx(pattern);

	int pos = 0;
	bool found = false;
	while ((pos = rx.indexIn(result, pos)) != -1) {
		QString tmp = rx.cap(0);
		//skip mask eg:255.255.255.0
		if (-1 == tmp.indexOf("255")) {
			if (ip != "" && -1 != tmp.indexOf(ip.mid(0, ip.lastIndexOf(".")))) {
				found = true;
				break;
			}
			ip = tmp;
		}
		pos += rx.matchedLength();
	}

	//qDebug() << "ip: " << ip;
	return ip;
}
bool EngineXmlConfig::NewXmlFile(std::string enginconfPath)
{
	QString machineName = QHostInfo::localHostName();		//本机名
	std::string path = enginconfPath + "/HostName_" + machineName.toStdString() + ".xml";
	std::cout << "Generate engine file " << path << std::endl;
	QFile file(path.c_str());
	if (!file.open(QFile::ReadWrite | QIODevice::Truncate))
	{
		//qDebug() << "Error: cannot open file";
		return  false;
	}


	QXmlStreamWriter stream(&file);
	stream.setAutoFormatting(true);
	stream.writeStartDocument();
	stream.writeStartElement("Engine");
	stream.writeTextElement("IP", getMyIp());
	stream.writeTextElement("Status", "Ready");
	QDateTime current_date_time = QDateTime::currentDateTime();
	QString current_date = current_date_time.toString("yyyy.MM.dd hh:mm:ss.zzz ddd");
	stream.writeTextElement("StartTime", current_date);
	
	stream.writeEndElement();
	stream.writeEndDocument();
	file.close();
	return true;
}

bool EngineXmlConfig::DeleteXmlFile(std::string enginconfPath)
{
	QString machineName = QHostInfo::localHostName();		//本机名
	std::string path = enginconfPath + "/HostName_" + machineName.toStdString() + ".xml";
	if (QFile::exists(QString::fromStdString(path))) {
		QFile::remove(QString::fromStdString(path));
	}
	return true;
}

QString EngineXmlConfig::getData(std::string enginconfPath, std::string name)
{
	QString data;
	QFile file(enginconfPath.c_str());
	if (!file.open(QFile::ReadWrite | QIODevice::Truncate))
	{
		//qDebug() << "Error: cannot open file";
		return  "false";
	}
	QXmlStreamReader reader(&file);
	while (!reader.atEnd())
	{
		//reader.readNext();//读取下一个开始元素
		reader.readNextStartElement();//读取下一个开始元素
									  //判断是否是节点的开始
		if (reader.isStartElement())
		{
			QString strElementName = reader.name().toString();
			if (QString::compare(strElementName, "Engine") == 0)
			{
				//qDebug() << QString::fromLocal8Bit("**********<Information>********** ");
			}
			else if (QString::compare(strElementName, QString::fromStdString(name)) == 0)
			{
				data= reader.readElementText();
				break;
			}
			
		}
	}
	file.close();
    return data;
// 	QXmlStreamWriter stream(&file);
// 	stream.setAutoFormatting(true);
// 	stream.writeStartDocument();
// 	stream.writeStartElement("Engine");
// 	stream.writeTextElement("IP", getMyIp());
// 	stream.writeTextElement("Status", "Ready");
// 	QDateTime current_date_time = QDateTime::currentDateTime();
// 	QString current_date = current_date_time.toString("yyyy.MM.dd hh:mm:ss.zzz ddd");
// 	stream.writeTextElement("StartTime", current_date);

//	stream.writeEndElement();


}
