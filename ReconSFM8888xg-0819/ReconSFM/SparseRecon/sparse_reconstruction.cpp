#include "sparse_reconstruction.h"
#include "base/undistortion.h"
#include "base/gps.h"
#include "controllers/incremental_mapper.h"
#include "controllers/hierarchical_mapper.h"
#include "controllers/bundle_adjustment.h"
#include "feature/extraction.h"
#include "feature/matching.h"
#include "util/misc.h"
#include "util/option_manager.h"
#include <QDebug>
#include <QMessageBox>
#include <QFile>
#include <QDir>
#include <QFileInfoList>
#include <QMessageBox>
#include <QSharedMemory>
#include <QSystemSemaphore>
#include <QDateTime>
#include <QDebug>
#include <QtNetwork/QHostInfo>
#include <QtNetwork/QNetworkInterface>
#include <QProcess>
#include <io.h>
#include <base/gcp.h>
#include <controllers/bundle_adjustment_with_gcp.h>
namespace sfmrecon {

	SparseReconstructionController::SparseReconstructionController(
		const Options& options, ReconstructionManager* reconstruction_manager)
		: options_(options),
		reconstruction_manager_(reconstruction_manager),
		active_thread_(nullptr) {
		CHECK(ExistsDir(options_.workspace_path));
		CHECK(ExistsDir(options_.image_path));
		CHECK_NOTNULL(reconstruction_manager_);

		option_manager_.AddAllOptions();
		*option_manager_.image_path = options_.image_path;
		std::string path = options_.workspace_path;
		int lastpos = path.find_last_of('/');
		std::string lastname(path.substr(lastpos + 1));
		*option_manager_.database_path =
			JoinPaths(options_.workspace_path, lastname + ".db");
		option_manager_.ModifyForIndividualData();
		CHECK(ExistsCameraModelWithName(options_.camera_model));
		option_manager_.ModifyForHighQuality();
		option_manager_.sift_extraction->num_threads = options_.num_threads;
		option_manager_.sift_matching->num_threads = options_.num_threads;
		option_manager_.mapper->num_threads = options_.num_threads;

		ImageReaderOptions reader_options = *option_manager_.image_reader;
		reader_options.database_path = *option_manager_.database_path;
		reader_options.image_path = *option_manager_.image_path;

		reader_options.single_camera = options_.single_camera;
		reader_options.camera_model = options_.camera_model;
		reader_options.gps_file = options_.gps_file_path;
		reader_options.focal_length_file = options_.focal_file_path;
		reader_options.single_camera_per_folder = options_.single_camera_per_folder;

		option_manager_.sift_extraction->use_gpu = options_.use_gpu;
		option_manager_.sift_extraction->max_num_features = options_.max_feature_point;
		option_manager_.sift_matching->use_gpu = options_.use_gpu;

		option_manager_.sift_extraction->gpu_index = options_.gpu_index;
		option_manager_.sift_matching->gpu_index = options_.gpu_index;

	
		hierarchical_options.database_path= *option_manager_.database_path;
		hierarchical_options.image_path = options_.image_path;


		feature_extractor_.reset(new SiftFeatureExtractor(
			reader_options, *option_manager_.sift_extraction));

		exhaustive_matcher_.reset(new ExhaustiveFeatureMatcher(
			*option_manager_.exhaustive_matching, *option_manager_.sift_matching,
			*option_manager_.database_path));

		if (!options_.vocab_tree_path.empty()) {
			option_manager_.sequential_matching->loop_detection = true;
			option_manager_.sequential_matching->vocab_tree_path =
				options_.vocab_tree_path;
		}

		sequential_matcher_.reset(new SequentialFeatureMatcher(
			*option_manager_.sequential_matching, *option_manager_.sift_matching,
			*option_manager_.database_path));

		spatial_matcher_.reset(new SpatialFeatureMatcher(
			*option_manager_.spatial_matching, *option_manager_.sift_matching,
			*option_manager_.database_path));

		if (!options_.vocab_tree_path.empty()) {
			option_manager_.vocab_tree_matching->vocab_tree_path =
				options_.vocab_tree_path;
			vocab_tree_matcher_.reset(new VocabTreeFeatureMatcher(
				*option_manager_.vocab_tree_matching, *option_manager_.sift_matching,
				*option_manager_.database_path));
		}
	}

	void SparseReconstructionController::Stop() {
		if (active_thread_ != nullptr) {
			active_thread_->Stop();
		}
		Thread::Stop();
	}

	void SparseReconstructionController::Run() {
		if (IsStopped()) {
			return;
		}
		/********************************/
		if (options_.at_file_path.empty() || _access(options_.at_file_path.c_str(), 00)) {
			RunFeatureExtraction();

		if (IsStopped()) {
			return;
		}

		RunFeatureMatching();

		if (IsStopped()) {
			return;
		}

		if (options_.sparse) {
			RunSparseMapper();
		}

		if (options_.dense) {
			RunDenseMapper();
		}
		}
		if (!options_.gcp_file_path.empty() && !_access(options_.gcp_file_path.c_str(), 00)) {
			reconstruction_manager_->Add();
			reconstruction_manager_->Get(0).LoadFromXMLFile(options_.at_file_path,options_.image_path);

			RunReSparseMapper();
		}
		/********************************/
	}

	void SparseReconstructionController::RunFeatureExtraction() {
		CHECK(feature_extractor_);
		active_thread_ = feature_extractor_.get();
		feature_extractor_->Start();
		feature_extractor_->Wait();
		feature_extractor_.reset();
		active_thread_ = nullptr;
	}

	void SparseReconstructionController::RunFeatureMatching() {
		Thread* matcher = nullptr;
		if (options_.data_type == DataType::VIDEO) {
			matcher = sequential_matcher_.get();
		}
		else if (options_.data_type == DataType::INDIVIDUAL ||
			options_.data_type == DataType::INTERNET) {
			Database database(*option_manager_.database_path);
			const size_t num_images = database.NumImages();
		/*	if (ExistsFile(options_.gps_file_path))
			{
				matcher = spatial_matcher_.get();
			}
			else */if (options_.vocab_tree_path.empty() || num_images < 50)/********************************/
			{
				matcher = exhaustive_matcher_.get();
			}
			else {
				matcher = vocab_tree_matcher_.get();
			}
		}

		CHECK(matcher);
		active_thread_ = matcher;
		matcher->Start();
		matcher->Wait();
		exhaustive_matcher_.reset();
		sequential_matcher_.reset();
		vocab_tree_matcher_.reset();
		spatial_matcher_.reset();
		active_thread_ = nullptr;
	}

	void SparseReconstructionController::RunSparseMapper() {
		const auto sparse_path = JoinPaths(options_.workspace_path, "sparse");
		if (ExistsDir(sparse_path)) {
			auto dir_list = GetDirList(sparse_path);
			std::sort(dir_list.begin(), dir_list.end());
			if (dir_list.size() > 0) {
				std::cout << std::endl
					<< "WARNING: Skipping sparse reconstruction because it is "
					"already computed"
					<< std::endl;
				for (const auto& dir : dir_list) {
					reconstruction_manager_->Read(dir);
				}
				return;
			}
		}
		/********************************/
		//share focal length
		IncrementalMapperOptions *mapper_option = option_manager_.mapper.get();
		if (!_access(options_.focal_file_path.c_str(), 00)) {
			mapper_option->ba_refine_focal_length = false;
		}
#if 0
		IncrementalMapperController mapper(
			option_manager_.mapper.get(), *option_manager_.image_path,
			*option_manager_.database_path, reconstruction_manager_);
#else
		SceneClustering::Options clustering_options;
		clustering_options.image_overlap = 100;
		clustering_options.leaf_max_num_images =1000;
			
		HierarchicalMapperController mapper(hierarchical_options,clustering_options,
		*option_manager_.mapper.get(), reconstruction_manager_);
#endif
		/********************************/
		active_thread_ = &mapper;
		mapper.Start();
		mapper.Wait();

		//reconstruction_manager_->Write(*option_manager_.image_path.get(), &option_manager_);
		active_thread_ = nullptr;
		/********************************/
		int lastpos = options_.workspace_path.find_last_of('/');
		std::string lastname(options_.workspace_path.substr(lastpos + 1));
		std::string dense_path = JoinPaths(options_.workspace_path, "/" + lastname + "_AT");
		CreateDirIfNotExists(dense_path);
		if (reconstruction_manager_->Size() > 0) {
      std::vector<int> recon_size;
      for (size_t i = 0; i < reconstruction_manager_->Size(); ++i)
      {
        recon_size.push_back(reconstruction_manager_->Get(i).Images().size());
      }
      auto maxPosition = max_element(recon_size.begin(), recon_size.end());
      int ij = maxPosition - recon_size.begin();
			/*reconstruction_manager_->Get(0).ExportXMLFile(dense_path + "/" + lastname + "_AT_bak.xml", options_.image_path + "/");
			int num_filtered = reconstruction_manager_->Get(0).FilterAllPoints3D(1, 5);
			std::cout << "before export xml filter observation: " << num_filtered << std::endl;
			num_filtered = reconstruction_manager_->Get(0).FilterPoints3D(2);
			std::cout << "before export xml filter point3d: " << num_filtered << std::endl;*/ 
			reconstruction_manager_->Get(ij).ExportXMLFile(dense_path + "/" + lastname + "_AT.xml", options_.image_path + "/");
		}
	}
		/********************************/
	void SparseReconstructionController::RunDenseMapper() {
		std::vector<int> recon_size;
		for (size_t i = 0; i < reconstruction_manager_->Size(); ++i)
		{
			recon_size.push_back(reconstruction_manager_->Get(i).Images().size());
		}
		auto maxPosition = max_element(recon_size.begin(), recon_size.end());
		int ij = maxPosition - recon_size.begin();

		std::cout <<"Max Position is:" <<ij << std::endl;
		if (IsStopped()) {
			return;
		}
		/********************************/
		Reconstruction reconstruction = reconstruction_manager_->Get(ij);
		
		if (ExistsFile(options_.gps_file_path)) {
			RunGeoPos(reconstruction);
		}

		int lastpos = options_.workspace_path.find_last_of('/');
		std::string lastname(options_.workspace_path.substr(lastpos + 1));
		std::string dense_path = JoinPaths(options_.workspace_path, "/" + lastname + "_AT");
		CreateDirIfNotExists(dense_path);

		reconstruction.ExportGPSXMLFile(dense_path + "/" + lastname + "_GPS.xml", options_.image_path + "/");
		std::string bundle_path = dense_path + "/" + lastname + ".out";
		std::string bundle_path_list = dense_path + "/" + lastname + "_list.txt";
		reconstruction.ExportBundler(bundle_path, bundle_path_list);

		//UndistortCameraOptions undistortion_options;
		//XMLUndistorter undistorter(undistortion_options,
		//	reconstruction,
		//	*option_manager_.image_path, dense_path);
		//active_thread_ = &undistorter;
		//undistorter.Start();
		//undistorter.Wait();
		//active_thread_ = nullptr;
		////}
		//reconstruction.Write(dense_path);
	}

	void SparseReconstructionController::RunReSparseMapper() {
		assert(reconstruction_manager_->Size() > 0);
		Reconstruction& reconstruction = reconstruction_manager_->Get(0);
		if (!reconstruction.LoadGCPInfoFromTextFile(options_.gcp_file_path)) {
			std::cerr << "load GCP failed" << std::endl;
			return;
		}

		BundleAdjustmentWithGCPController gcp_controller(option_manager_, &reconstruction);
		gcp_controller.Start();
		gcp_controller.Wait();

		int lastpos = options_.workspace_path.find_last_of('/');
		std::string lastname(options_.workspace_path.substr(lastpos + 1));
		std::string dense_path = options_.workspace_path + "/" + lastname + "_AT";
		CreateDirIfNotExists(dense_path);
		reconstruction.ExportGPSXMLFile(dense_path + "/" + lastname + "_AT_GPS_GCP.xml", options_.image_path + "/");
	}
	/********************************/
	bool SparseReconstructionController::RunGeoPos(Reconstruction &reconstruction)
	{
		//std::string input_path;
		//std::string ref_images_path;
		//std::string output_path;
		int min_common_images = 3;
		bool robust_alignment = false;
		RANSACOptions ransac_options;
		ransac_options.max_error = 7.5;
		OptionManager options;
		GPSTransform gps_transform;

		if (!ExistsFile(options_.gps_file_path))
		{
			std::cout << "Please Make Sure POS File is Existing!" << std::endl;
			return EXIT_FAILURE;
		}

		if (robust_alignment && ransac_options.max_error <= 0) {
			std::cout << "ERROR: You must provide a maximum alignment error > 0"
				<< std::endl;
			return EXIT_FAILURE;
		}

		std::vector<std::string> ref_image_names;
		std::vector<Eigen::Vector3d> ref_locations;
		//std::vector<Eigen::Vector3d> geo_locations;
		//FILE *mfile = fopen(options_.gps_file_path.c_str(), "r");
		//if (mfile == NULL)
		//	return false;
		//std::vector<GPSInfo> vec_PosImage;
		//int i = 0;
		//while (!feof(mfile))
		//{
		//	GPSInfo mPos;
		//	char mImag[50];
		//	Eigen::Vector3d Kpos;
		//	Eigen::Vector3d KIMU;
		//	int kflag;
		//	fscanf(mfile, "%s\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%d\n", mImag, &(Kpos[0]), &(Kpos[1]), &(Kpos[2]), &(KIMU[0]), &(KIMU[1]), &(KIMU[2]), &(kflag));
		//	sprintf(mPos.m_sImageName, mImag);
		//	mPos.PosInfo = Kpos;
		//	mPos.IMUInfo = KIMU;
		//	if (kflag == 0)
		//		mPos.flags = false;
		//	else
		//		mPos.flags = true;
		//	vec_PosImage.push_back(mPos);
		//	ref_image_names.push_back(mPos.m_sImageName);
		//	/*	ref_image_names.push_back(mPos.m_sImageName);
		//		geo_locations.push_back(Kpos);*/
		//}
		//fclose(mfile);

		//int count = vec_PosImage.size();
		//Eigen::Vector3d ells;
		//for (i = 0; i < count; i++)
		//{
		//	Eigen::Vector3d gps_info = vec_PosImage[i].PosInfo;
		//	Eigen::Vector3d   xyzs;
		//	if (vec_PosImage[i].flags)
		//	{
		//		ells(0) = gps_info(0);
		//		ells(1) = gps_info(1);
		//		ells(2) = gps_info(2);
		//		xyzs = gps_transform.lla_to_utm(ells);
		//	}
		//	else
		//	{
		//	//	xyzs.resize(1);
		//		xyzs(0) = gps_info(0);
		//		xyzs(1) = gps_info(1);
		//		xyzs(2) = gps_info(2);
		//	}
		//	ref_locations.push_back(xyzs);
		//}

		//std::cout << StringPrintf(" => Using %d reference images",
		//	ref_image_names.size())
		//	<< std::endl;

		for (auto& image : reconstruction.Images())
		{
			if (image.second.HasTvecPrior() && image.second.TvecPrior(2) > 0)
			{
				ref_image_names.push_back(image.second.Name());
				ref_locations.push_back(image.second.TvecPrior());
			}
			else
				continue;
		}

		bool alignment_success = reconstruction.Align(ref_image_names, ref_locations, min_common_images);
		//bool alignment_success = reconstruction.AlignRobust(ref_image_names, ref_locations, min_common_images, ransac_options);
		if (alignment_success) {
			std::cout << " => Alignment succeeded" << std::endl;
	/*		std::vector<double> errors;
			errors.reserve(ref_image_names.size());

			for (size_t i = 0; i < ref_image_names.size(); ++i) {
				const Image* image = reconstruction.FindImageWithName(ref_image_names[i]);
				if (image != nullptr) {
					errors.push_back((image->ProjectionCenter() - ref_locations[i]).norm());
				}
			}

			std::cout << StringPrintf(" => Alignment error: %f (mean), %f (median)",
				Mean(errors), Median(errors))
				<< std::endl;*/
		}
		else {
			std::cout << " => Alignment failed" << std::endl;
		}
		return EXIT_SUCCESS;
	}



	bool SparseReconstructionController::RunReGen(Reconstruction &reconstruction)
	{
		return reconstruction.AlignGen();
	}

	bool SparseReconstructionController::RunInvPos(Reconstruction &reconstruction)
	{
		return reconstruction.AlignInv();
	}


	void SparseReconstructionController::writeXml(Options temp)
	{
		//Options temp;
		QFile file("C:\\Hello.xml");
		if (!file.open(QFile::ReadWrite | QIODevice::Truncate))
		{
			//qDebug() << "Error: cannot open file";
			return;
		}


		QXmlStreamWriter stream(&file);
		stream.setAutoFormatting(true);
		stream.writeStartDocument();

		stream.writeStartElement("Information");
		stream.writeTextElement("workspace_path", QString::fromStdString(temp.workspace_path));
		stream.writeTextElement("image_path", QString::fromStdString(temp.image_path));
		stream.writeTextElement("vocab_tree_path", QString::fromStdString(temp.vocab_tree_path));
		stream.writeTextElement("gps_file_path", QString::fromStdString(temp.gps_file_path));
		if (temp.data_type== DataType::INDIVIDUAL)
		{
			stream.writeTextElement("data_type", "0");
		}
		else if(temp.data_type == DataType::VIDEO)
		{
			stream.writeTextElement("data_type", "1");
		}
		else
		{
			stream.writeTextElement("data_type", "2");

		}
		if (temp.quality == Quality::LOW)
		{
			stream.writeTextElement("quality", "0");
		}
		else if (temp.quality == Quality::MEDIUM)
		{
			stream.writeTextElement("quality", "1");
		}
		else if(temp.quality == Quality::HIGH)
		{
			stream.writeTextElement("quality", "2");

		}
		else
		{
			stream.writeTextElement("quality", "3");
		}
		if (temp.single_camera )
		{
			stream.writeTextElement("single_camera", "true");
		}
		else {
			stream.writeTextElement("single_camera", "false");
		}
		if (temp.single_camera_per_folder)
		{
			stream.writeTextElement("single_camera_per_folder", "true");
		}
		else {
			stream.writeTextElement("single_camera_per_folder", "false");
		}
		stream.writeTextElement("camera_model", QString::fromStdString(temp.camera_model));
		if (temp.sparse)
		{
			stream.writeTextElement("sparse", "true");
		}
		else {
			stream.writeTextElement("sparse", "false");
		}
		if (temp.dense)
		{
			stream.writeTextElement("dense", "true");
		}
		else {
			stream.writeTextElement("dense", "false");
		}
		stream.writeTextElement("num_threads", QString("%1").arg(temp.num_threads));
		if (temp.use_gpu)
		{
			stream.writeTextElement("use_gpu", "true");
		}
		else {
			stream.writeTextElement("use_gpu", "false");
		}
		stream.writeTextElement("gpu_index", QString::fromStdString(temp.gpu_index));
		stream.writeTextElement("max_feature_point", QString("%1").arg(temp.max_feature_point));

		stream.writeEndElement();


		

		stream.writeEndDocument();
		file.close();
	}

	void SparseReconstructionController::readXml(QString path, Options temp)
	{
		//Options temp;
		QFile file(path);
		if (file.open(QIODevice::ReadOnly | QIODevice::Text))
		{

			//构建QXmlStreamReader对象
			QXmlStreamReader reader(&file);

			while (!reader.atEnd())
			{
				//reader.readNext();//读取下一个开始元素
				reader.readNextStartElement();//读取下一个开始元素
											  //判断是否是节点的开始
				if (reader.isStartElement())
				{
					QString strElementName = reader.name().toString();
					if (QString::compare(strElementName, "Information") == 0)
					{
						//qDebug() << QString::fromLocal8Bit("**********<Information>********** ");
					}
					else if (QString::compare(strElementName, QStringLiteral("workspace_path")) == 0)
					{
						temp.workspace_path = reader.readElementText().toStdString();
					}
					else if (QString::compare(strElementName, QStringLiteral("image_path")) == 0)
					{  // 方式
						temp.image_path = reader.readElementText().toStdString();
					}
					else if (QString::compare(strElementName, QStringLiteral("vocab_tree_path")) == 0)
					{
						temp.vocab_tree_path = reader.readElementText().toStdString();
					}
					else if (QString::compare(strElementName, QStringLiteral("gps_file_path")) == 0)
					{
						temp.gps_file_path = reader.readElementText().toStdString();
					}
					else if (QString::compare(strElementName, QStringLiteral("data_type")) == 0)
					{

						if (reader.readElementText() == "0")
						{
							temp.data_type = DataType::INDIVIDUAL;
						}
						else if (reader.readElementText() == "1")
						{
							temp.data_type = DataType::VIDEO;
						}
						else
						{
							temp.data_type = DataType::INTERNET;

						}
					}
					else if (QString::compare(strElementName, QStringLiteral("quality")) == 0)
					{

						if (reader.readElementText() == "0")
						{
							temp.quality = Quality::LOW;
						}
						else if (reader.readElementText() == "1")
						{
							temp.quality = Quality::MEDIUM;
						}
						else if (reader.readElementText() == "2")
						{
							temp.quality = Quality::HIGH;

						}
						else
						{
							temp.quality = Quality::EXTREME;
						}
					}
					else if (QString::compare(strElementName, QStringLiteral("single_camera")) == 0)
					{

						if (reader.readElementText() == "true")
						{
							temp.single_camera = 1;
						}
						else if (reader.readElementText() == "false")
						{
							temp.single_camera = 0;
						}
					}
					else if (QString::compare(strElementName, QStringLiteral("single_camera_per_folder")) == 0)
					{

						if (reader.readElementText() == "true")
						{
							temp.single_camera_per_folder = 1;
						}
						else if (reader.readElementText() == "false")
						{
							temp.single_camera_per_folder = 0;
						}
					}
					else if (QString::compare(strElementName, QStringLiteral("camera_model")) == 0)
					{
						temp.camera_model = reader.readElementText().toStdString();
					}
					else if (QString::compare(strElementName, QStringLiteral("sparse")) == 0)
					{

						if (reader.readElementText() == "true")
						{
							temp.sparse = 1;
						}
						else if (reader.readElementText() == "false")
						{
							temp.sparse = 0;
						}
					}
					else if (QString::compare(strElementName, QStringLiteral("dense")) == 0)
					{

						if (reader.readElementText() == "true")
						{
							temp.dense = 1;
						}
						else if (reader.readElementText() == "false")
						{
							temp.dense = 0;
						}
					}
					else if (QString::compare(strElementName, QStringLiteral("num_threads")) == 0)
					{


						temp.num_threads = reader.readElementText().toInt();

					}
					else if (QString::compare(strElementName, QStringLiteral("use_gpu")) == 0)
					{

						if (reader.readElementText() == "true")
						{
							temp.use_gpu = 1;
						}
						else if (reader.readElementText() == "false")
						{
							temp.use_gpu = 0;
						}
					}
					else if (QString::compare(strElementName, QStringLiteral("gpu_index")) == 0)
					{
						temp.gpu_index = reader.readElementText().toStdString();
					}
					else if (QString::compare(strElementName, QStringLiteral("max_feature_point")) == 0)
					{


						temp.max_feature_point = reader.readElementText().toInt();

					}
				}
			}


			file.close();
		}
		
	}




#define ONE_ROW_MAX_CHR 256
	static int del_assigned_row(FILE *fp_in, char *path, int assign_line)
	{
		int lines = 1;
		char path_tmp[256] = { 0 };
		char line[ONE_ROW_MAX_CHR] = { 0 };
		FILE *fp_out = NULL;

		sprintf(path_tmp, "%s.tmp", path);
		//fp_in = fopen(path, "r");
		if (fp_in == NULL) {
			printf("open %s failed!\n", path);
			return -1;
		}
		fp_out = fopen(path_tmp, "a+");
		if (fp_out == NULL) {
			printf("create tmp file %s failed!\n", path_tmp);
			return -1;
		}

		while (fgets(line, ONE_ROW_MAX_CHR, fp_in) != NULL)
		{
			if (lines != assign_line) {
				fwrite(line, 1, strlen(line), fp_out);
			}
			++lines;
			memset(line, 0, ONE_ROW_MAX_CHR);
		}


		if (fclose(fp_out) != 0) {
			printf("close file %s failed!\n", path_tmp);
			return -2;
		}

		if (fclose(fp_in) != 0) {
			printf("close file %s failed!\n", path);
			return -2;
		}
		QDateTime current_date_time = QDateTime::currentDateTime();
		QString current_time = current_date_time.toString("hh:mm:ss.zzz ");
		qDebug() << current_time;
		remove(path);
		rename(path_tmp, path);
		current_date_time = QDateTime::currentDateTime();
		current_time = current_date_time.toString("hh:mm:ss.zzz ");
		qDebug() << current_time;
		return 0;
	}



	void SparseReconstructionController::CopyDir(QString src, QString dst)
	{
		QDir dir(src);
		if (!dir.exists())
			return;

		foreach(QString d, dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot)) {
			QString dst_path = dst + QDir::separator() + d;
			dir.mkpath(dst_path);
			CopyDir(src + QDir::separator() + d, dst_path);
		}

		foreach(QString f, dir.entryList(QDir::Files)) {
			int flag = QFile::copy(src + QDir::separator() + f, dst + QDir::separator() + f);
			if (flag == 1)
			{
				qDebug() << QStringLiteral("复制成功");
			}
			else {
				qDebug() << src + QDir::separator() + f;
				qDebug() << QStringLiteral("复制失败");
			}

		}
	}




	bool SparseReconstructionController::Copyfile(QString filepath, QString despath)
	{
		int flag = QFile::copy(filepath, despath);
		return flag;
	}

	char* SparseReconstructionController::getTask(char* filename) {
		QDateTime current_date_time = QDateTime::currentDateTime();
		QString current_time = current_date_time.toString("hh:mm:ss.zzz ");
		qDebug() << current_time;
		int i = 0;
		FILE *fp = NULL;
		char str[100];
		char strlen = 0;
		//char *filename = "d:/test2/test.txt";
		fopen_s(&fp, filename, "a+");
		if (fp)
		{

			while (!feof(fp))
			{
				fgets(str, 10, fp);//中间的参数是指定读多少。
								   //不懂了
				printf("%s", str);
				break;//循环一次读一行。记得要加break；
			}
			del_assigned_row(fp, filename, 0);
			return str;
		}
		else {
			return "null";
		}

	}



	std::string SparseReconstructionController::getHostName()
	{

		QString machineName = QHostInfo::localHostName();		//本机名
		return machineName.toStdString();
	}

	std::string SparseReconstructionController::getHostIp()
	{
		QString ip = "";
		QProcess cmd_pro;
		QString cmd_str = QString("ipconfig");
		cmd_pro.start("cmd.exe", QStringList() << "/c" << cmd_str);
		cmd_pro.waitForStarted();
		cmd_pro.waitForFinished();
		QString result = cmd_pro.readAll();
		QString pattern("[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}");
		QRegExp rx(pattern);

		int pos = 0;
		bool found = false;
		while ((pos = rx.indexIn(result, pos)) != -1) {
			QString tmp = rx.cap(0);
			//跳过子网掩码 eg:255.255.255.0
			if (-1 == tmp.indexOf("255")) {
				if (ip != "" && -1 != tmp.indexOf(ip.mid(0, ip.lastIndexOf(".")))) {
					found = true;
					break;
				}
				ip = tmp;
			}
			pos += rx.matchedLength();
		}

		//qDebug() << "ip: " << ip;
		return ip.toStdString();
	}

} //
