﻿#include "SlaveProgress.h"
#include "JobConfig.h"
#include "base/undistortion.h"
#include "base/gps.h"
#include "feature/extraction.h"
#include "feature/matching.h"
#include "util/misc.h"
#include "util/option_manager.h"
#include "io.h"
#include "JobList.h"
#include<chrono>
#include <atomic>
#include <thread>
#include <QDebug>
#include <QMessageBox>
#include <QFile>
#include <QDir>
#include <QFileInfoList>
#include <QMessageBox>
#include <QSharedMemory>
#include <QSystemSemaphore>
#include <QDateTime>
#include <QDebug>
#include <QXmlStreamWriter>
#include <unordered_map>
#include <base/gcp.h>
#include <controllers/bundle_adjustment_with_gcp.h>
#include "util/misc.h"
#include "FileMonotor.h"
#include <math.h>
#include <boost/filesystem.hpp>
#include <base/at_report.h>
#include <base/at_gcp_report.h>
#include "base/projection.h"

namespace sfmrecon {

	SlaveProgress::SlaveProgress(ReconstructionManager* reconstruction_manager, inputOptions options)
		: reconstruction_manager_(reconstruction_manager),
		active_thread_(nullptr) {
		CHECK_NOTNULL(reconstruction_manager_);
		mParamfile = "";
		//joinDistribute(mWorkSpace);
		mInputOptions.output_path = options.output_path;
		mInputOptions.project_path = options.project_path;
		//std::cerr << "construct setting_jobqueue_path: " << options.setting_jobqueue_path << std::endl;
		mInputOptions.setting_jobqueue_path = options.setting_jobqueue_path;
		mWorkSpace = options.output_path;
	}

	SlaveProgress::~SlaveProgress()
	{

	}

	bool SlaveProgress::forceRollBack() {
		std::string jobPath = mInputOptions.project_path + "/jobs";
		std::cout << "jobPath:" << jobPath << std::endl;
		mJobList.initial(jobPath);
		rollBack();
		return true;
	}

	bool SlaveProgress::rollBack() {
		std::cout << "call slave rollback" << std::endl;
		if (mJobList.isEmptyList()) {
			std::cout << "JobList is empty" << std::endl;
			return false;
		}
		//clearEngineFile();
		std::string outputPath = mInputOptions.output_path + "/workspace/currentPath";
		//return 
		//mJobList.rollBack();
		mJobList.rollBackSingle(outputPath);
		clearEngineFile();
		return true;
	}

	bool SlaveProgress::clearEngineFile() {
		std::cout << "call clearEngineFile" << std::endl;
		std::string outputPath = mInputOptions.output_path + "/workspace";

		if (!ExistsDir(outputPath)) {
			std::cout << "no local workspace" << outputPath << std::endl;
			return false;
		}
		QDir dir;
		dir.setPath(QString::fromStdString(outputPath));
		std::cout << "clear dir:" << outputPath << std::endl;
		QFileInfoList list = dir.entryInfoList();
		for (int i = 0; i < list.count(); i++) {
			QFileInfo fileInfo = list.at(i);
			if ( fileInfo.fileName() == "." | fileInfo.fileName() == "..")
			{
				continue;
			}
			QString Firstfilename = fileInfo.absoluteFilePath();
			//std::string at_file_name = Firstfilename.toStdString();
			std::cout << "remove :" << fileInfo.filePath().toStdString() << std::endl;
			if (fileInfo.isFile()) {
				QFile tmpfile(Firstfilename);
				tmpfile.remove();
			}
			if (fileInfo.isDir()) {
				QDir dirTemp(Firstfilename);
				dirTemp.removeRecursively();
			}
			
		}

		//dir.removeRecursively();
		CreateDirIfNotExists(outputPath);

		return true;
	}
	
	bool SlaveProgress::setting(std::string jobQueuePath, std::string outputPath) {
		std::string slash = "/";
		CreateDirIfNotExists(jobQueuePath);

		std::string ArchivePath = JoinPaths(jobQueuePath, slash + "Archive");
		CreateDirIfNotExists(ArchivePath);
		std::string CancelledPath = JoinPaths(jobQueuePath, slash + "Cancelled");
		CreateDirIfNotExists(CancelledPath);
		std::string CompletedPath = JoinPaths(jobQueuePath, slash + "Completed");
		CreateDirIfNotExists(CompletedPath);
		std::string EnginesPath = JoinPaths(jobQueuePath, slash + "Engines");
		CreateDirIfNotExists(EnginesPath);
		std::string FailedPath = JoinPaths(jobQueuePath, slash + "Failed");
		CreateDirIfNotExists(FailedPath);
		std::string PendingPath = JoinPaths(jobQueuePath, slash + "Pending");
		CreateDirIfNotExists(PendingPath);
		std::string PendingHigh = JoinPaths(PendingPath, slash + "High");
		CreateDirIfNotExists(PendingHigh);
		std::string PendingLow = JoinPaths(PendingPath, slash + "Low");
		CreateDirIfNotExists(PendingLow);
		std::string PendingNormal = JoinPaths(PendingPath, slash + "Normal");
		CreateDirIfNotExists(PendingNormal);
		std::string RunningPath = JoinPaths(jobQueuePath, slash + "Running");
		CreateDirIfNotExists(RunningPath);

		EngineXmlConfig enginexml;
		std::string enginexmlPath = jobQueuePath + "/Engines";
		enginexml.NewXmlFile(enginexmlPath);
		genEngineConfig(outputPath, jobQueuePath);

		return true;
	}

	bool SlaveProgress::refreshEngine() {
		srand((int)time(0));
		int time = rand() % 10 + 1;
		int sleepSecond = time * 1000;
		//std::cout << "main loop no job, sleep 0-10s:" << sleepSecond << std::endl;
		std::this_thread::sleep_for(std::chrono::milliseconds(sleepSecond));                      //no job , sleep
		std::string outputPath = options_.output_path;
		int lastpos = outputPath.find_last_of('/');
		std::string lastname(outputPath.substr(0, lastpos));
		std::string usrworkspace = lastname;
		//std::cerr << "refresh from jobQueuePath " << options_.jobQueuePath << std::endl;
		//refreshEngine(usrworkspace, options_.jobQueuePath);
		bool result = false;
		result = genEngineConfig(usrworkspace, options_.jobQueuePath);
		if (result)
			result = setEngineConfig();
		return result;
	}

	//join distribute system, read engin conf, find param xml file path, and load config
	bool SlaveProgress::joinDistribute(std::string outputPath) {
		std::string currentPath = outputPath;
		std::string slash = "/";
		std::string subpath = "engine.xml";
		std::string enginconfPath = JoinPaths(currentPath, slash + subpath);

		setting(mInputOptions.setting_jobqueue_path, mInputOptions.output_path);
		//get config param 
		mParamfile = enginconfPath;

		return true;
	}

	bool SlaveProgress::initialEngine() {
		//initial the param

		CHECK(ExistsDir(options_.workspace_path));
		//CHECK(ExistsDir(options_.image_path));

		option_manager_.AddAllOptions();

		*option_manager_.image_path = options_.image_path;            //----------------------
		*option_manager_.database_path = options_.db_path;

		option_manager_.ModifyForIndividualData();
		CHECK(ExistsCameraModelWithName(options_.camera_model));
		option_manager_.ModifyForHighQuality();
		option_manager_.sift_extraction->num_threads = options_.num_threads;
		option_manager_.sift_matching->num_threads = options_.num_threads;
		option_manager_.mapper->num_threads = options_.num_threads;



		option_manager_.sift_extraction->use_gpu = options_.use_gpu;
		option_manager_.sift_extraction->max_num_features = options_.max_feature_point;
		option_manager_.sift_matching->use_gpu = options_.use_gpu;

		option_manager_.sift_extraction->gpu_index = options_.gpu_index;
		option_manager_.sift_matching->gpu_index = options_.gpu_index;

		return true;
	}

	bool SlaveProgress::genEngineConfig(std::string outputPath, std::string jobQueuePath) {
		//std::cerr << "genEngineConfig jobQueuePath: " << jobQueuePath << std::endl;
		std::string currentPath = outputPath;
		std::string slash = "/";
		std::string subpath = "workspace";
		std::string workPath = JoinPaths(outputPath, slash + subpath);
		CreateDirIfNotExists(workPath);
		//write conf
		std::string enginsubpath = "engine.xml";
		std::string enginconfPath = JoinPaths(currentPath, slash + enginsubpath);

		std::string projectInfo = jobQueuePath + "/Running/" + options_.projectName + "_AT-file.xml";
		std::string projectPath;
		while (!ExistsFile(projectInfo))  //running is empty
		{
				
				bool getAProject = false;
				QDir destinatePath;
				QDir dirRun(QString::fromStdString(jobQueuePath + "/Running/"));
				if (dirRun.count() > 2) { //high not empty 
					std::cout << "Running has one" << std::endl;
					destinatePath = dirRun;
					getAProject = true;
				}
				else {
					//std::cout << "no project:" << std::endl;
					QDir dirHigh(QString::fromStdString(jobQueuePath + "/Pending/High"));
					if (dirHigh.count() > 2) { //high not empty 
						std::cout << "Pending/High has one" << std::endl;
						destinatePath = dirHigh;
						getAProject = true;
					}
					else {
						QDir dirNormal(QString::fromStdString(jobQueuePath + "/Pending/Normal"));
						if (dirNormal.count() > 2) { //Normal not empty 
							destinatePath = dirNormal;
							getAProject = true;
						}
						else {
							QDir dirLow(QString::fromStdString(jobQueuePath + "/Pending/Low"));
							if (dirLow.count() > 2) { //Low not empty 
								destinatePath = dirLow;
								getAProject = true;
							}
						}
					}
				}
				
				if (getAProject) {  //get a project
					QFileInfoList list = destinatePath.entryInfoList();
					for (int i = 0; i < list.count(); i++) {
						QFileInfo fileInfo = list.at(i);
						if (fileInfo.isDir() | fileInfo.fileName() == "." | fileInfo.fileName() == "..")
						{
							continue;
						}
						QString Firstfilename = fileInfo.absoluteFilePath();
						std::string at_file_name = Firstfilename.toStdString();
						int lastpos = at_file_name.find_last_of('/');
						std::string lastName(at_file_name.substr(lastpos + 1));
						std::cout << "lastName:" << lastName << std::endl;
						std::string toconf = JoinPaths(jobQueuePath, slash + "/Running/" + lastName);
						if (Firstfilename.toStdString().find("/Running") == std::string::npos) {
							if (QFile::exists(QString::fromStdString(toconf))) {
								QFile::remove(QString::fromStdString(toconf));
							}
							QDir dir;
							dir.rename(Firstfilename, QString::fromStdString(toconf));
						}
						
						std::string newProjectName = StringReplace(lastName, "_AT-file.xml", "");
						std::cout << "newProjectName:" << newProjectName << std::endl;
						options_.projectName = newProjectName;
						projectInfo = jobQueuePath + "/Running/" + newProjectName + "_AT-file.xml";
						break;
					}
					break;
				}
				else {
					//std::cerr << "Error: Cannot read project conf file " << projectInfo << std::endl;
					srand((int)time(0));
					int time = rand() % 10 + 1;
					int sleepSecond = time * 1000;
					//std::cerr << "no project, sleep 0-10s:" << sleepSecond << std::endl;
					std::this_thread::sleep_for(std::chrono::milliseconds(sleepSecond));                      //no job , sleep
				}
			
			
		}
		QFile projectfile(projectInfo.c_str());
		projectfile.open(QFile::ReadOnly | QFile::Text);
		QXmlStreamReader projectreader(&projectfile);

		while (!projectreader.atEnd())
		{
			projectreader.readNext();//读取下一个开始元素
									 //如果当前记号为StartElement，返回true
			if (projectreader.isStartElement())
			{
				if (projectreader.name() == "Job") {
					continue;
				}
				else if (projectreader.name() == "Project")
				{
					projectPath = projectreader.readElementText().toStdString();
					std::cout << "projectPath :" << projectPath << std::endl;
				}
			}
			else {
				continue;
			}

		}

		projectfile.close();
		if (projectreader.hasError())
		{
			std::cerr << "Error: Failed to parse file " << projectInfo << ": " << projectreader.errorString().toStdString() << std::endl;
			return false;
		}
		else if (projectfile.error() != QFile::NoError)
		{
			std::cerr << "Error: Cannot read file " << projectInfo << ": " << projectfile.errorString().toStdString() << std::endl;
			return false;
		}

		//TODO write enginxmlPath, projectPath, jobQueuePath, outputPath to confPath
		//-------------------------
		std::string confPath = projectPath + "/conf/masteroption.xml";
		QFile file(confPath.c_str());
		Options temp;
		if (file.open(QIODevice::ReadOnly | QIODevice::Text))
		{

			//构建QXmlStreamReader对象
			QXmlStreamReader reader(&file);

			while (!reader.atEnd())
			{
				//reader.readNext();//读取下一个开始元素
				reader.readNextStartElement();//读取下一个开始元素
											  //判断是否是节点的开始
				if (reader.isStartElement())
				{
					QString strElementName = reader.name().toString();
					if (QString::compare(strElementName, "Information") == 0)
					{
						//qDebug() << QString::fromLocal8Bit("**********<Information>********** ");
					}
					else if (QString::compare(strElementName, QStringLiteral("workspacePath")) == 0)
					{
						temp.workspace_path = reader.readElementText().toStdString();
					}
					else if (QString::compare(strElementName, QStringLiteral("imagePath")) == 0)
					{  // 方式
						temp.image_path = reader.readElementText().toStdString();
					}
					else if (QString::compare(strElementName, QStringLiteral("vocab_tree_path")) == 0)
					{
						temp.vocab_tree_path = reader.readElementText().toStdString();
					}
					else if (QString::compare(strElementName, QStringLiteral("gpsPath")) == 0)
					{
						temp.gps_file_path = reader.readElementText().toStdString();
                    }
                    else if (QString::compare(strElementName, QStringLiteral("gcpPath")) == 0)
                    {
                        temp.gcp_file_path = reader.readElementText().toStdString();
                    }
                    else if (QString::compare(strElementName, QStringLiteral("atReportPath")) == 0)
                    {
                        temp.at_report_file_path = reader.readElementText().toStdString(); 
                    }
					else if (QString::compare(strElementName, QStringLiteral("atPath")) == 0)
					{
						temp.at_file_path = reader.readElementText().toStdString();
					}
					else if (QString::compare(strElementName, QStringLiteral("focalPath")) == 0)
					{
						temp.focal_file_path = reader.readElementText().toStdString();
					}
					else if (QString::compare(strElementName, QStringLiteral("db_path")) == 0)
					{
						temp.db_path = reader.readElementText().toStdString();
					}
					else if (QString::compare(strElementName, QStringLiteral("jobPath")) == 0)
					{
						temp.job_path = reader.readElementText().toStdString();
					}
					else if (QString::compare(strElementName, QStringLiteral("projectPath")) == 0)
					{
						temp.project_path = reader.readElementText().toStdString();

					}
					else if (QString::compare(strElementName, QStringLiteral("projectName")) == 0)
					{
						temp.projectName = reader.readElementText().toStdString();

					}
					else if (QString::compare(strElementName, QStringLiteral("jobQueuePath")) == 0)
					{
						temp.jobQueuePath = reader.readElementText().toStdString();
						//std::cerr << "read jobQueuePath " << temp.jobQueuePath << std::endl;
					}
					else if (QString::compare(strElementName, QStringLiteral("feedbackPath")) == 0)
					{
						temp.feedback_path = reader.readElementText().toStdString();

					}
					else if (QString::compare(strElementName, QStringLiteral("data_type")) == 0)
					{

						if (reader.readElementText() == "0")
						{
							temp.data_type = DataType::INDIVIDUAL;
						}
						else if (reader.readElementText() == "1")
						{
							temp.data_type = DataType::VIDEO;
						}
						else
						{
							temp.data_type = DataType::INTERNET;

						}
					}
					else if (QString::compare(strElementName, QStringLiteral("quality")) == 0)
					{

						if (reader.readElementText() == "0")
						{
							temp.quality = Quality::LOW;
						}
						else if (reader.readElementText() == "1")
						{
							temp.quality = Quality::MEDIUM;
						}
						else if (reader.readElementText() == "2")
						{
							temp.quality = Quality::HIGH;

						}
						else
						{
							temp.quality = Quality::EXTREME;
						}
					}
					else if (QString::compare(strElementName, QStringLiteral("single_camera")) == 0)
					{

						if (reader.readElementText() == "true")
						{
							temp.single_camera = 1;
						}
						else if (reader.readElementText() == "false")
						{
							temp.single_camera = 0;
						}
					}
					else if (QString::compare(strElementName, QStringLiteral("single_camera_per_folder")) == 0)
					{

						if (reader.readElementText() == "true")
						{
							temp.single_camera_per_folder = 1;
						}
						else if (reader.readElementText() == "false")
						{
							temp.single_camera_per_folder = 0;
						}
					}
					else if (QString::compare(strElementName, QStringLiteral("camera_model")) == 0)
					{
						temp.camera_model = reader.readElementText().toStdString();
					}
					else if (QString::compare(strElementName, QStringLiteral("sparse")) == 0)
					{

						if (reader.readElementText() == "true")
						{
							temp.sparse = 1;
						}
						else if (reader.readElementText() == "false")
						{
							temp.sparse = 0;
						}
					}
					else if (QString::compare(strElementName, QStringLiteral("dense")) == 0)
					{

						if (reader.readElementText() == "true")
						{
							temp.dense = 1;
						}
						else if (reader.readElementText() == "false")
						{
							temp.dense = 0;
						}
					}
					else if (QString::compare(strElementName, QStringLiteral("num_threads")) == 0)
					{


						temp.num_threads = reader.readElementText().toInt();

					}
					else if (QString::compare(strElementName, QStringLiteral("use_gpu")) == 0)
					{

						if (reader.readElementText() == "true")
						{
							temp.use_gpu = 1;
						}
						else if (reader.readElementText() == "false")
						{
							temp.use_gpu = 0;
						}
					}
					else if (QString::compare(strElementName, QStringLiteral("gpu_index")) == 0)
					{
						temp.gpu_index = reader.readElementText().toStdString();
					}
					else if (QString::compare(strElementName, QStringLiteral("max_feature_point")) == 0)
					{
						temp.max_feature_point = reader.readElementText().toInt();
					}
					else if (QString::compare(strElementName, QStringLiteral("numCurProcess")) == 0)
					{
						temp.numCurProcess = reader.readElementText().toInt();
					}
				}
			}


			file.close();
		}

		temp.output_path = workPath;
		temp.enginconfPath = enginconfPath;
		//write slave conf xml
		wirtexml(temp);
		return true;
	}

	/**
	set distribute system config
	get distribute config, and join into the distribute system
	*/
	bool SlaveProgress::setEngineConfig() {
		if (mParamfile == "" || !ExistsFile(mParamfile)) {
			std::cout << "ERROR: You should run master and create a project first!" << std::endl;
			Thread::Stop();
			return false;
		}

		//TODO read project config path, and set config-------------

		//std::cerr << "set from mParamfile: " << mParamfile << std::endl;
		QFile file(mParamfile.c_str());
		if (file.open(QIODevice::ReadOnly | QIODevice::Text))
		{

			//构建QXmlStreamReader对象
			QXmlStreamReader reader(&file);

			while (!reader.atEnd())
			{
				//reader.readNext();//读取下一个开始元素
				reader.readNextStartElement();//读取下一个开始元素
											  //判断是否是节点的开始
				if (reader.isStartElement())
				{
					QString strElementName = reader.name().toString();
					if (QString::compare(strElementName, "Information") == 0)
					{
						//qDebug() << QString::fromLocal8Bit("**********<Information>********** ");
					}
					else if (QString::compare(strElementName, QStringLiteral("workspacePath")) == 0)
					{
						options_.workspace_path = reader.readElementText().toStdString();
					}
					else if (QString::compare(strElementName, QStringLiteral("imagePath")) == 0)
					{
						options_.image_path = reader.readElementText().toStdString();
					}
					else if (QString::compare(strElementName, QStringLiteral("vocab_tree_path")) == 0) 
					{
						options_.vocab_tree_path = reader.readElementText().toStdString();
					}
					else if (QString::compare(strElementName, QStringLiteral("gps_file_path")) == 0)
					{
						options_.gps_file_path = reader.readElementText().toStdString();
                    }
                    else if (QString::compare(strElementName, QStringLiteral("gcp_file_path")) == 0)
                    {
                        options_.gcp_file_path = reader.readElementText().toStdString();
                    }
                    else if (QString::compare(strElementName, QStringLiteral("at_report_file_path")) == 0)
                    {
                        options_.at_report_file_path = reader.readElementText().toStdString();
                    }
					else if (QString::compare(strElementName, QStringLiteral("at_file_path")) == 0)
					{
						options_.at_file_path = reader.readElementText().toStdString();
					}
					else if (QString::compare(strElementName, QStringLiteral("focal_file_path")) == 0)
					{
						options_.focal_file_path = reader.readElementText().toStdString();
					}
					else if (QString::compare(strElementName, QStringLiteral("db_path")) == 0)
					{
						options_.db_path = reader.readElementText().toStdString();
					}
					else if (QString::compare(strElementName, QStringLiteral("jobPath")) == 0)
					{
						options_.job_path = reader.readElementText().toStdString();
					}
					else if (QString::compare(strElementName, QStringLiteral("projectPath")) == 0)
					{
						options_.project_path = reader.readElementText().toStdString();
					}
					else if (QString::compare(strElementName, QStringLiteral("projectName")) == 0)
					{
						options_.projectName = reader.readElementText().toStdString();
					}
					else if (QString::compare(strElementName, QStringLiteral("jobQueuePath")) == 0)
					{
						options_.jobQueuePath = reader.readElementText().toStdString();
					}
					else if (QString::compare(strElementName, QStringLiteral("outputPath")) == 0)
					{
						options_.output_path = reader.readElementText().toStdString();
					}
					else if (QString::compare(strElementName, QStringLiteral("feedback_path")) == 0)
					{
						options_.feedback_path = reader.readElementText().toStdString();
					}
					else if (QString::compare(strElementName, QStringLiteral("data_type")) == 0)
					{

						if (reader.readElementText() == "0")
						{
							options_.data_type = DataType::INDIVIDUAL;
						}
						else if (reader.readElementText() == "1")
						{
							options_.data_type = DataType::VIDEO;
						}
						else
						{
							options_.data_type = DataType::INTERNET;

						}
					}
					else if (QString::compare(strElementName, QStringLiteral("quality")) == 0)
					{

						if (reader.readElementText() == "0")
						{
							options_.quality = Quality::LOW;
						}
						else if (reader.readElementText() == "1")
						{
							options_.quality = Quality::MEDIUM;
						}
						else if (reader.readElementText() == "2")
						{
							options_.quality = Quality::HIGH;

						}
						else
						{
							options_.quality = Quality::EXTREME;
						}
					}
					else if (QString::compare(strElementName, QStringLiteral("single_camera")) == 0)
					{

						if (reader.readElementText() == "true")
						{
							options_.single_camera = 1;
						}
						else if (reader.readElementText() == "false")
						{
							options_.single_camera = 0;
						}
					}
					else if (QString::compare(strElementName, QStringLiteral("single_camera_per_folder")) == 0)
					{

						if (reader.readElementText() == "true")
						{
							options_.single_camera_per_folder = 1;
						}
						else if (reader.readElementText() == "false")
						{
							options_.single_camera_per_folder = 0;
						}
					}
					else if (QString::compare(strElementName, QStringLiteral("camera_model")) == 0)
					{
						options_.camera_model = reader.readElementText().toStdString();
					}
					else if (QString::compare(strElementName, QStringLiteral("sparse")) == 0)
					{

						if (reader.readElementText() == "true")
						{
							options_.sparse = 1;
						}
						else if (reader.readElementText() == "false")
						{
							options_.sparse = 0;
						}
					}
					else if (QString::compare(strElementName, QStringLiteral("dense")) == 0)
					{

						if (reader.readElementText() == "true")
						{
							options_.dense = 1;
						}
						else if (reader.readElementText() == "false")
						{
							options_.dense = 0;
						}
					}
					else if (QString::compare(strElementName, QStringLiteral("num_threads")) == 0)
					{


						options_.num_threads = reader.readElementText().toInt();

					}
					else if (QString::compare(strElementName, QStringLiteral("use_gpu")) == 0)
					{

						if (reader.readElementText() == "true")
						{
							options_.use_gpu = 1;
						}
						else if (reader.readElementText() == "false")
						{
							options_.use_gpu = 0;
						}
					}
					else if (QString::compare(strElementName, QStringLiteral("gpu_index")) == 0)
					{
						options_.gpu_index = reader.readElementText().toStdString();
					}
					else if (QString::compare(strElementName, QStringLiteral("max_feature_point")) == 0)
					{
						options_.max_feature_point = reader.readElementText().toInt();
					}
					else if (QString::compare(strElementName, QStringLiteral("numCurProcess")) == 0)
					{
						options_.numCurProcess = reader.readElementText().toInt();
					}

				}

			}
			file.close();
		}

		initialEngine();
		mJobList.initial(options_.job_path);
		return true;
	}

	bool SlaveProgress::removeEngine() {
		EngineXmlConfig enginexml;
		std::string enginexmlPath = mInputOptions.setting_jobqueue_path + "/Engines";
		//std::string enginexmlPath = options_.jobQueuePath + "/Engines";
		return enginexml.DeleteXmlFile(enginexmlPath);		
	}

	void SlaveProgress::Stop() {
		std::cout << "call stop!" << std::endl;
		if (active_thread_ != nullptr) {
			active_thread_->Stop();
		}
		rollBack();
		//removeEngine();
		Thread::Stop();
	}

	bool SlaveProgress::wirtexml(Options temp)
	{
		QFile file(temp.enginconfPath.c_str());
		if (!file.open(QFile::ReadWrite | QIODevice::Truncate))
		{
			//qDebug() << "Error: cannot open file";
			return false;
		}


		QXmlStreamWriter stream(&file);
		stream.setAutoFormatting(true);
		stream.writeStartDocument();
		stream.writeStartElement("Information");
		stream.writeTextElement("workspacePath", QString::fromStdString(temp.workspace_path));
		stream.writeTextElement("db_path", QString::fromStdString(temp.db_path));
		stream.writeTextElement("imagePath", QString::fromStdString(temp.image_path));
        stream.writeTextElement("gps_file_path", QString::fromStdString(temp.gps_file_path));
        stream.writeTextElement("gcp_file_path", QString::fromStdString(temp.gcp_file_path));
        stream.writeTextElement("at_report_file_path", QString::fromStdString(temp.at_report_file_path));
		stream.writeTextElement("at_file_path", QString::fromStdString(temp.at_file_path));
		stream.writeTextElement("focal_file_path", QString::fromStdString(temp.focal_file_path));
		stream.writeTextElement("vocab_tree_path", QString::fromStdString(temp.vocab_tree_path));
		stream.writeTextElement("jobPath", QString::fromStdString(temp.job_path));
		stream.writeTextElement("feedback_path", QString::fromStdString(temp.feedback_path));
		stream.writeTextElement("projectName", QString::fromStdString(temp.projectName));
		stream.writeTextElement("projectPath", QString::fromStdString(temp.project_path));
		stream.writeTextElement("jobQueuePath", QString::fromStdString(temp.jobQueuePath));
		stream.writeTextElement("outputPath", QString::fromStdString(temp.output_path));

		if (temp.data_type == DataType::INDIVIDUAL)
		{
			stream.writeTextElement("data_type", "0");
		}
		else if (temp.data_type == DataType::VIDEO)
		{
			stream.writeTextElement("data_type", "1");
		}
		else
		{
			stream.writeTextElement("data_type", "2");

		}
		if (temp.quality == Quality::LOW)
		{
			stream.writeTextElement("quality", "0");
		}
		else if (temp.quality == Quality::MEDIUM)
		{
			stream.writeTextElement("quality", "1");
		}
		else if (temp.quality == Quality::HIGH)
		{
			stream.writeTextElement("quality", "2");

		}
		else
		{
			stream.writeTextElement("quality", "3");
		}
		if (temp.single_camera)
		{
			stream.writeTextElement("single_camera", "true");
		}
		else {
			stream.writeTextElement("single_camera", "false");
		}
		if (temp.single_camera_per_folder)
		{
			stream.writeTextElement("single_camera_per_folder", "true");
		}
		else {
			stream.writeTextElement("single_camera_per_folder", "false");
		}
		stream.writeTextElement("camera_model", QString::fromStdString(temp.camera_model));
		if (temp.sparse)
		{
			stream.writeTextElement("sparse", "true");
		}
		else {
			stream.writeTextElement("sparse", "false");
		}
		if (temp.dense)
		{
			stream.writeTextElement("dense", "true");
		}
		else {
			stream.writeTextElement("dense", "false");
		}
		stream.writeTextElement("num_threads", QString("%1").arg(temp.num_threads));
		if (temp.use_gpu)
		{
			stream.writeTextElement("use_gpu", "true");
		}
		else {
			stream.writeTextElement("use_gpu", "false");
		}
		stream.writeTextElement("gpu_index", QString::fromStdString(temp.gpu_index));
		stream.writeTextElement("max_feature_point", QString("%1").arg(temp.max_feature_point));
		stream.writeTextElement("numCurProcess", QString("%1").arg(temp.numCurProcess));


		stream.writeEndElement();
		stream.writeEndDocument();
		file.close();
		return true;
	}

	bool SlaveProgress::getJobConfig(std::string jobFile, JobConfig &jobconfig) {
		std::string jobPath = options_.job_path + "/wait/" + jobFile;
		bool result = jobconfig.readConfig(jobPath);
		return result;
	}

	bool SlaveProgress::handleSingleJob(std::string jobFile, JobConfig jobconfig) {
		std::cout  << "run Job" << jobFile << "......" << std::endl;
		printTime(jobFile + "begin : ");
		//std::this_thread::sleep_for(std::chrono::milliseconds(20000));
		
		//move job to handle fold
		mJobList.handJob(options_.job_path, jobFile);
		
		std::string currentPath = options_.output_path + "/currentPath";
		std::cout << "currentPath:" << currentPath << std::endl;
		CreateDirIfNotExists(currentPath);
		std::string from = options_.job_path + "/handle/" + jobFile;
		std::string to = currentPath + "/" + jobFile;
		std::cout << "from:" << from << std::endl;
		std::cout << "to:" << to << std::endl;
		QFile::copy(QString::fromStdString(from), QString::fromStdString(to));
		

		std::string run_file_path = options_.jobQueuePath + +"/Running/" + options_.projectName + "_AT-file.xml";

		if (!ExistsFile(run_file_path)) {
			std::cout << "stop pool****************" << std::endl;
			clearEngineFile();
			return false;
		}
		//refresh jobStatus 
		EngineXmlConfig enginexml;
		std::string ip = enginexml.getMyIp().toStdString();
		setJobProgress(jobconfig, ip, JOB_BEGIN_STATUS);

		int type = jobconfig.getJobType();
		bool jobHandled = false;
		switch (type) {
		case JOB_FIRST:
			jobHandled = RunFirstJob(jobconfig);
			break;
		case JOB_FEATURE_EXTRACTION:
			jobHandled = RunFeatureExtractionJob(jobconfig);
			break;
		case JOB_FEATURE_MATCHING:
			jobHandled = RunFeatureMatchingJob(jobconfig);
			break;
		case JOB_CLUSTER:
			jobHandled = RunImageClusterJob(jobconfig);
			break;
		case JOB_INCREMENTAL:
			jobHandled = RunIncrementMapperJob(jobconfig);
			break;
		case JOB_CLUSTER_SYNCHRONIZE:
			jobHandled = waitForSynchronize(jobconfig);
			break;
		case JOB_MERGE:
			jobHandled = RunClusterMergeJob(jobconfig);
			break;
		case JOB_DENSE:
			jobHandled = RunDenseCloudJob(jobconfig);
			break;
		case JOB_FINISH:
			jobHandled = RunFinshJob(jobconfig);
			break;
		case JOB_RESPARSE_RECONSTRUTION:
			jobHandled = RunReSparseJob(jobconfig);
			break;
		default:
			std::cout << "ERROR: unsupport job!" << std::endl;
		}
		std::cout <<  "job :" << jobFile << ",type:" << type << ",run result:" << jobHandled << std::endl;
		if (jobHandled) {
			//move job to finish fold
			mJobList.finishJob(options_.job_path, jobFile);
			std::cout << " job :" << type << " time:" << std::endl;
			std::cout << "----------" << std::endl;
			GetTimer().PrintMinutes();
			printTime(jobFile + "end : ");
			std::cout << "----------" << std::endl;
			
			std::string currentPath = options_.output_path + "/currentPath";
			std::string to = currentPath + "/" + jobFile;
			if (ExistsFile(to)) {
				QFile::remove(QString::fromStdString(to));
			}
			
			setJobProgress(jobconfig, ip, JOB_END_STATUS);
			if (type == JOB_FINISH) {
				std::string fromxml = options_.jobQueuePath + "/Running/" + options_.projectName + "_AT-file.xml";
				std::string toxml = options_.jobQueuePath + "/Completed/" + options_.projectName + "_AT-file.xml";
				QDir dir;
				dir.rename(QString::fromStdString(fromxml), QString::fromStdString(toxml));
			}
		}
		return true;

	}

	int  SlaveProgress::isSingleJob(JobConfig jobconfig) {
		int isSingle = JOB_UNDEFIND;

		int type = jobconfig.getJobType();
		if (type == JOB_INCREMENTAL) {
			isSingle = JOB_MULTI;
		}
		else {
			isSingle = JOB_SINGLE;
		}

		return isSingle;
	}

	void SlaveProgress::Run() {
		std::cout << "======================================================" << std::endl;
		std::cout << "initial engine param......" << std::endl;
		std::cout << "begin time:" << std::endl;
		std::cout << "----------" << std::endl;
		GetTimer().PrintMinutes();
		std::cout << "----------" << std::endl;
		//join distribute, get project
		if (!joinDistribute(mWorkSpace) || !setEngineConfig()) {
			Stop();
			return;
		}
		clearEngineFile();
		//mJobList.initial(options_.job_path);
		std::cout << "======================================================" << std::endl;
		std::cout << "begin work:" << std::endl;
		std::cout << "----------" << std::endl;
		GetTimer().PrintMinutes();
		std::cout << "----------" << std::endl;

		//main loop

		while (true) {
			std::cout << "--------------------------------------" << std::endl;
			//check AT-file status
			std::cout << "options_.projectName:" << options_.projectName << std::endl;
			std::string run_file_path = options_.jobQueuePath + +"/Running/" + options_.projectName + "_AT-file.xml";
			if (!ExistsFile(run_file_path)){
				refreshEngine();
				continue;
			}

			//fetch a main job
			std::string jobFile = getJob();
			//no job, sleep
			if (jobFile == "null") {
				clearEngineFile();
				refreshEngine();
				continue;
			}
			//have a job
			else {
				//file_monotor.setMonitorFile(run_file_path);
				//read config
				JobConfig jobconfig;
				int jobMode = JOB_UNDEFIND;
				bool result = getJobConfig(jobFile, jobconfig);
				if (!result) {
					jobMode = JOB_UNDEFIND;
					continue;
				}
				jobMode = isSingleJob(jobconfig);
				//single job, handle it with main thread
				if (jobMode == JOB_SINGLE) {
					std::cout << "main loop job:" << jobFile << std::endl;
					handleSingleJob(jobFile, jobconfig);
				}
				//multi job, creat a thread pool to handle it 
				else {
					//copy db
					FILE* lock = NULL;
					std::string slash = "/";
					std::string lockpath = "db.lock";
					std::string lockFile = JoinPaths(options_.workspace_path, slash + lockpath);
					std::cout << "lock file:--------" << lockFile << std::endl;

					while (fopen_s(&lock, lockFile.c_str(), "r+") != 0) {			//wait to get db lock
						srand((int)time(0));
						int time = rand() % 10 + 1;
						int sleepSecond = time * 1000;
						std::cout << "sleep 0-1s:" << sleepSecond << std::endl;
						std::this_thread::sleep_for(std::chrono::milliseconds(sleepSecond));
						continue;
					}
					printTime("copy db begin : ");
					std::string fromdb = options_.db_path;
					unsigned long long diskremain = getDiskFreeSpace(options_.output_path);
					size_t needspace = GetFileSize(fromdb);
					unsigned long long diskrequire = needspace >> 20;
					std::cout << "Need :" << diskrequire << "MB, remain :" << diskremain << "MB" << std::endl;
					std::string todb = options_.output_path + "/local.db";
					std::cout << "from db ..." << fromdb << std::endl;
					std::cout << "to db ..." << todb << std::endl;
					if (!ExistsFile(todb)) {    //if db is not exist, need copy,
						if (diskremain < diskrequire) {
							fclose(lock);
							handleSingleJob(jobFile, jobconfig);
							std::cerr << "ERROR: Not sufficient local space!" << std::endl;
							Stop();
							break;
						}
						QFile::copy(QString::fromStdString(fromdb), QString::fromStdString(todb));
						printTime("copy db end : ");
					}
					else {           //local db exist
						size_t localDBSize = GetFileSize(todb);
						while (localDBSize < diskrequire) {   //db not copy completed, wait
							srand((int)time(0));
							int time = rand() % 10 + 1;
							int sleepSecond = time * 1000;
							std::cout << "sleep 0-1s:" << sleepSecond << std::endl;
							std::this_thread::sleep_for(std::chrono::milliseconds(sleepSecond));
							localDBSize = GetFileSize(todb);
						}

					}
					
					fclose(lock);
					//options_.db_path = todb;

					std::cout << "multi loop job:" << jobFile << std::endl;
					std::string nextMainJob;
					
					const int kMaxNumThreads = -1;
					const int num_eff_threads = GetEffectiveNumThreads(kMaxNumThreads);
					//const int kDefaultNumWorkers = 8;
					const int kDefaultNumWorkers = options_.numCurProcess;
					mNum_eff_workers = std::min(kDefaultNumWorkers, num_eff_threads);
					ThreadPool threadPool(mNum_eff_workers);
					

					//a thread pool for run job parallel
					auto HandleMultiJob = [&, this](int threadIndex, std::string job) {
						//loop
						while (true) {
							std::cout << "--------------------------------------" << std::endl;
							std::cout << "run multi job " << job << std::endl;
							//no more job, break
							if (job == "null") {
								break;
							}
							else {
								JobConfig subjobconfig;
								int subjobMode = JOB_UNDEFIND;
								bool result = getJobConfig(job, subjobconfig);
								if (!result) {
									subjobMode = JOB_UNDEFIND;
									break;
								}
								subjobMode = isSingleJob(subjobconfig);
								//single job, handle it with main thread
								if (subjobMode == JOB_SINGLE) {
									//get a single job, return to main handle
									std::cout << "back to main job " << job << std::endl;
									nextMainJob = job;
									break;
								}
								else {
									//get a multi job, handle it and stay in loop, and get next
									std::cout << "core " << threadIndex << "run Job" << job << "......" << std::endl;
									printTime(job + " begin : ");
									handleSingleJob(job, subjobconfig);
									job = getJob();
								}
							}
						}
					};
					//add current job to parallel thread pool
					threadPool.AddTask(HandleMultiJob, 1, jobFile);
					//add more multi job to parallel thread pool
					for (int i = 1; i < mNum_eff_workers; i++) {
						std::string job = getJob();
						//no more job, stop
						if (job == "null") {
							break;
						}
						//have more job, add to pool
						else {
							threadPool.AddTask(HandleMultiJob, i + 1, job);
						}
					}
					threadPool.Wait();
					//back to main thread job
					bool result = getJobConfig(nextMainJob, jobconfig);
					if (!result) {
						jobMode = JOB_UNDEFIND;
						continue;
					}
					handleSingleJob(nextMainJob, jobconfig);
					//QString tempDB = QString::fromStdString(todb);
					////delete db
					//QFile::remove(tempDB);
				
				}
			}
		}
		
		
	}

	bool SlaveProgress::RunReSparseJob(JobConfig jobconfig) {
		reconstruction_manager_->Add();
		reconstruction_manager_->Get(0).LoadFromXMLFile(options_.at_file_path, options_.image_path);
		assert(reconstruction_manager_->Size() > 0);
		Reconstruction& reconstruction = reconstruction_manager_->Get(0);

		if (!reconstruction.LoadGCPInfoFromTextFile(options_.gcp_file_path)) {
			std::cerr << "load GCP failed" << std::endl;
			return true;
		}

        export_at_gcp_report(reconstruction,true);

		BundleAdjustmentWithGCPController gcp_controller(option_manager_, &reconstruction);


		gcp_controller.Start();
		gcp_controller.Wait();

        export_at_gcp_report(reconstruction,false);

		std::string lastname = options_.projectName;
		std::string slash = "/";
		std::string subpath = "dense";
		std::string dense_path = JoinPaths(options_.workspace_path, slash + subpath);
		CreateDirIfNotExists(dense_path);
		reconstruction.ExportGPSXMLFile(dense_path + "/" + lastname + "_AT_GPS_GCP.xml", options_.image_path);
		//gen end job
		std::string msg = "Finish job...";
		int nextprogress = PROGRESS_STATE_FINISH;
		int incprogress = PROGRESS_STATE_FINISH - PROGRESS_STATE_BEGIN;
		jobconfig.genJobId(JOB_FINISH, options_.job_path, incprogress, nextprogress, msg);
		return true;
	}

	bool SlaveProgress::RunFinshJob(JobConfig jobconfig) {
		//TODO
		//file_monotor.setMonitorFile("");
		std::cout << "All Jobs Done ..." << std::endl;
		std::cout << "======================================================" << std::endl;
		clearEngineFile();
		printTime("end Project : ");
		return true;
	}

	void SlaveProgress::printTime(std::string intro) {
		time_t now = time(0);
		tm *ltm = localtime(&now);
		std::string str = StringPrintf("%04d/%02d/%02d %02d:%02d:%02d",
			ltm->tm_year + 1900, ltm->tm_mon + 1, ltm->tm_mday,
			ltm->tm_hour, ltm->tm_min, ltm->tm_sec);
		std::cout << intro << " time : " << str << "----------------------" << std::endl;
	}

	//first job, initial job list
	bool SlaveProgress::RunFirstJob(JobConfig jobconfig) {
		printTime("begin Project : ");
		if (options_.at_file_path.empty() || _access(options_.at_file_path.c_str(), 00)) {
			if (!ExistsDir(options_.image_path)) {
				//finish
				//gen end job
				std::string msg = "Empty job...";
				int nextprogress = PROGRESS_STATE_FINISH;
				int incprogress = PROGRESS_STATE_FINISH - PROGRESS_STATE_BEGIN;
				jobconfig.genJobId(JOB_FINISH, options_.job_path, incprogress, nextprogress, msg);
			}
			else {
				//gen featureExtractionJob

				std::string msg = "Feature extraction...";
				int nextprogress = PROGRESS_STATE_FEATURE_MATCH;
				int incprogress = PROGRESS_STATE_FEATURE_MATCH - PROGRESS_STATE_FEATURE_EXTRACT;
				jobconfig.genJobId(JOB_FEATURE_EXTRACTION, options_.job_path, incprogress, nextprogress, msg);
			}
			//mProjectState = PROJECT_STATE_FEATURE_EXTRACT;
		}
		if (!options_.gcp_file_path.empty() && !_access(options_.gcp_file_path.c_str(), 00)) {
			//gen ReSparseJob
			std::string msg = "ReSparse Reconstruction...";
			int nextprogress = PROGRESS_STATE_FINISH;
			int incprogress = PROGRESS_STATE_FINISH - PROGRESS_STATE_BEGIN;
			jobconfig.genJobId(JOB_RESPARSE_RECONSTRUTION, options_.job_path, incprogress, nextprogress, msg);

			//RunReSparseJob();
		}
		return true;
	}

	//feature extraction job
	bool SlaveProgress::RunFeatureExtractionJob(JobConfig jobconfig) {
		std::string path = options_.job_path + "/Job" + jobconfig.getJobId() + ".log";
		//std::ofstream file(path.c_str(), std::ios::trunc);
		std::cout << "RunFeatureExtractionJob ..." << std::endl;
		/******************************************/
		ImageReaderOptions reader_options = *option_manager_.image_reader;
		reader_options.database_path = *option_manager_.database_path;
		reader_options.image_path = *option_manager_.image_path;

		reader_options.single_camera = options_.single_camera;
		reader_options.single_camera_per_folder = options_.single_camera_per_folder;
		reader_options.camera_model = options_.camera_model;
		reader_options.gps_file = options_.gps_file_path;
		reader_options.focal_length_file = options_.focal_file_path;
		//reader_options.m_file = &file;
		std::shared_ptr<Thread> feature_extractor_;
		feature_extractor_.reset(new SiftFeatureExtractor(
			reader_options, *option_manager_.sift_extraction));
		CHECK(feature_extractor_);
		active_thread_ = feature_extractor_.get();
		
    ATReport at_report;
    at_report.settings.key_point_density = option_manager_.sift_extraction->max_num_features;
    ATReport::TimeInfo &time_info = at_report.results.time_infos.feature_extract;
    time_info.start_time = std::time(0);
    
    if (!RunActiveThread(feature_extractor_)) {
      std::cout << "feature extractor stopped" << std::endl;
      //rollBack();
	  clearEngineFile();
      return false;
    }
		active_thread_ = nullptr;

    time_info.end_time = std::time(0);
    time_info.cost_time = std::to_string(double(time_info.end_time - time_info.start_time) / 60) + " minute";
    std::string at_report_path = GetATReportPath();

    std::cout << "at report path: " << at_report_path << std::endl;
    at_report.ExportXml(at_report_path);

		/******************************************/
		//gen featureMatchJob
		std::string msg = "Feature Matching...";
		int nextprogress = PROGRESS_STATE_CLUSTER;
		int incprogress = PROGRESS_STATE_CLUSTER - PROGRESS_STATE_FEATURE_MATCH;
    std::cout << "call here------------------" << std::endl;
		jobconfig.genJobId(JOB_FEATURE_MATCHING, options_.job_path, incprogress, nextprogress, msg);
		return true;
	}

	//feature match job
	bool SlaveProgress::RunFeatureMatchingJob(JobConfig jobconfig) {
		std::string path = options_.job_path + "/Job" + jobconfig.getJobId() + ".log";
		//std::ofstream file(path.c_str(), std::ios::trunc);
		std::cout << "RunFeatureMatchingJob ..." << std::endl;
		/******************************************/
		std::shared_ptr<Thread> exhaustive_matcher_;
		std::shared_ptr<Thread> sequential_matcher_;
		std::shared_ptr<Thread> vocab_tree_matcher_;
		std::shared_ptr<Thread> spatial_matcher_;
		exhaustive_matcher_.reset(new ExhaustiveFeatureMatcher(
			*option_manager_.exhaustive_matching, *option_manager_.sift_matching,
			*option_manager_.database_path));
		if (!options_.vocab_tree_path.empty()) {
			option_manager_.sequential_matching->loop_detection = true;
			option_manager_.sequential_matching->vocab_tree_path =
				options_.vocab_tree_path;
		}

		sequential_matcher_.reset(new SequentialFeatureMatcher(
			*option_manager_.sequential_matching, *option_manager_.sift_matching,
			*option_manager_.database_path));

    option_manager_.spatial_matching->max_num_neighbors = 100;
		spatial_matcher_.reset(new SpatialFeatureMatcher(
			*option_manager_.spatial_matching, *option_manager_.sift_matching,
			*option_manager_.database_path));

		if (!options_.vocab_tree_path.empty()) {
			option_manager_.vocab_tree_matching->vocab_tree_path =
				options_.vocab_tree_path;
			vocab_tree_matcher_.reset(new VocabTreeFeatureMatcher(
				*option_manager_.vocab_tree_matching, *option_manager_.sift_matching,
				*option_manager_.database_path));
		}
		
		std::shared_ptr<Thread> matcher = nullptr;

    std::string at_report_path = GetATReportPath();
    ATReport at_report;
    at_report.LoadFromXml(at_report_path);
    ATReport::TimeInfo &time_info = at_report.results.time_infos.feature_match;
    time_info.start_time = std::time(0);
    
		//video use sequential match
		if (options_.data_type == DataType::VIDEO) {
			matcher = sequential_matcher_;
		}
		else if (options_.data_type == DataType::INDIVIDUAL ||
			options_.data_type == DataType::INTERNET) {
			Database database(*option_manager_.database_path);
			const size_t num_images = database.NumImages();

      if (ExistsFile(options_.gps_file_path)) {
        matcher = spatial_matcher_;
      } else if (options_.vocab_tree_path.empty() || num_images < 50)         //no vocab tree or the number of image is less,use exhausetive match
			{
        at_report.settings.use_pos = false;
        at_report.settings.pair_select_mode = ATReport::PairMode::EXHAUSTIVE;
				matcher = exhaustive_matcher_;
			}
			else {					//default use vocab tree match
        at_report.settings.use_pos = false;
        at_report.settings.pair_select_mode = ATReport::PairMode::VOCABULARY;
				matcher = vocab_tree_matcher_;
			}
		}

		CHECK(matcher);
    active_thread_ = matcher.get();

    if (!RunActiveThread(matcher)) {
      std::cout << "feature matcher stopped" << std::endl;
      //rollBack();
	  clearEngineFile();
      return false;
    }
		
		active_thread_ = nullptr;
    time_info.end_time = std::time(0);
    time_info.cost_time = std::to_string(double(time_info.end_time - time_info.start_time) / 60) + " minute";
    std::cout << "feature match at report: " << at_report_path << std::endl;
    at_report.ExportXml(at_report_path);
		/******************************************/
		//gen  cluster Job
		std::string msg = "Cluster...";
		int nextprogress = PROGRESS_STATE_INCREMENT;
		int incprogress = PROGRESS_STATE_INCREMENT - PROGRESS_STATE_CLUSTER;
		jobconfig.genJobId(JOB_CLUSTER, options_.job_path, incprogress, nextprogress, msg);
		return true;
	}

	//sparse cloud job
	bool SlaveProgress::RunImageClusterJob(JobConfig jobconfig) {
		
		std::string path = options_.job_path + "/Job" + jobconfig.getJobId() + ".log";		
		std::ofstream file(path.c_str(), std::ios::trunc);
       std::cout << "RunImageClusterJob ..." << std::endl;
		/******************************************/
		//cluster 
		ClusterMapperController::Options clustermapper_options;
		clustermapper_options.database_path = *option_manager_.database_path;
		clustermapper_options.image_path = options_.image_path;
		clustermapper_options.workspace_path = options_.workspace_path;
		SceneClustering::Options clustering_options;
		clustering_options.image_overlap = 50;
		clustering_options.leaf_max_num_images = 1000;
		//clustering_options.image_overlap = 90;
		//clustering_options.leaf_max_num_images = 500;

    ATReport at_report;
    std::string at_report_path = GetATReportPath();
    at_report.LoadFromXml(at_report_path);
    at_report.results.time_infos.sparse_reconstruction.start_time = std::time(0);
    at_report.results.time_infos.sparse_reconstruction.cost_time = "-1";
    at_report.ExportXml(at_report_path);

		ClusterMapperController mapper(clustermapper_options, clustering_options);
		mapper.file = &file;
		active_thread_ = &mapper;
		mapper.Start();
		mapper.Wait();
		/******************************************/
		//gen partial Job
		//TODO  get cluster number 
		//TODO-------------------------------
		std::string clusternames = mapper.getClusterNames();
		//std::string clusternames = "1,2,3,4,5,6,7,8,9";
		//int clusterNum = 10;
		std::cout << "clusternames:" << clusternames << std::endl;
		std::string jobList = "";
		const std::vector<std::string> index_elems = StringSplit(clusternames, ",");
		//for (const auto index : index_elems) {
		for (size_t i = 0; i < index_elems.size(); i++)
		{
			//int currentIndex = static_cast<int>(i);
			std::string index = index_elems[i];
			//double subprogress = (currentIndex - 0) / index_elems.size();
			std::unordered_map<std::string, std::string> additionParam;
			additionParam.insert(std::make_pair("currentIndex", index));
			std::string tmpJobId;
			std::string msg = "Sparse  Reconstruction...";
			//int nextprogress = 50 + 30 * subprogress;    //progress is a number between 50 and 80, according to the job index
			int nextprogress = PROGRESS_STATE_SYNC;
			int incprogress = std::ceil((PROGRESS_STATE_SYNC - PROGRESS_STATE_INCREMENT) * 1.0 / index_elems.size());
			std::cout << "incr:" << incprogress << std::endl;
			jobconfig.initJob(JOB_INCREMENTAL, tmpJobId, additionParam, options_.job_path, incprogress, nextprogress, msg);
			if (jobList == "") {
				jobList += tmpJobId;
			}
			else {
				jobList += "," + tmpJobId;
			}
		}

		//gen synchronize job
		std::unordered_map<std::string, std::string> additionParam;
		additionParam.insert(std::make_pair("joblist", jobList));
		std::string tmpJobId;
		std::string msg = "Wait synchronize job...";
		int nextprogress = PROGRESS_STATE_MERGE;
		int incprogress = PROGRESS_STATE_MERGE - PROGRESS_STATE_SYNC;
		jobconfig.initJob(JOB_CLUSTER_SYNCHRONIZE, tmpJobId, additionParam, options_.job_path,  incprogress, nextprogress, msg);

		active_thread_ = nullptr;
		return true;
	}

	//increment mapper job
	bool SlaveProgress::RunIncrementMapperJob(JobConfig jobconfig) {
		/******************************************/
		std::string path = options_.job_path + "/Job" + jobconfig.getJobId() + ".log";
		//std::ofstream file(path.c_str(), std::ios::trunc);
		std::cout << "RunIncrementMapperJob ..." << std::endl;

		//distribute increment 
		PartialMapperController::Options partialmapper_options;
		//partialmapper_options.database_path = *option_manager_.database_path;
		

		partialmapper_options.image_path = options_.image_path;
		partialmapper_options.cluste_path = options_.workspace_path;
		partialmapper_options.job_path = options_.job_path;

		IncrementalMapperOptions custom_options;
		custom_options.max_model_overlap = 3;
		custom_options.init_num_trials = 10;
		custom_options.num_threads = -1;
		if (!_access(options_.focal_file_path.c_str(), 00)) {
			custom_options.ba_refine_focal_length = false;
		}
		//find cluster path
		std::string slash = "/";
		partialmapper_options.current_index = str2int(jobconfig.getValue("currentIndex"));
		std::string subpath = "clusterFile" + slash + "clusterJob_" + int2str(partialmapper_options.current_index);
		std::string leafFilePath = JoinPaths(options_.workspace_path, slash + subpath);
		partialmapper_options.cluste_path = leafFilePath;

		std::string todb = options_.output_path + "/local.db";
		if (!ExistsFile(todb)) {
			return false;
		}
		std::cout << "to db ..." << todb << std::endl;

		partialmapper_options.database_path = todb;


		//PartialMapperController mapper(partialmapper_options, custom_options, reconstruction_manager_, option_manager_);
		////mapper.m_file = &file;
		//mapper.AddCallback(PartialMapperController::PARTIAL_FINISHED_CALLBACK, [this]() {
		//	active_thread_ = nullptr;
		//	std::cout << "back from partial callback..." << std::endl;
		//	return true;
		//});
		//active_thread_ = &mapper;

    std::shared_ptr<PartialMapperController> mapper = std::make_shared<PartialMapperController>
      (partialmapper_options, custom_options, reconstruction_manager_, option_manager_);
    mapper->AddCallback(PartialMapperController::PARTIAL_FINISHED_CALLBACK, [this]() {
      active_thread_ = nullptr;
      std::cout << "back from partial callback..." << std::endl;
      return true;
    });

    if (!RunActiveThread(mapper)) {
      std::cout << "partial mapper stopped" << std::endl;
      //rollBack();
      return false;
    }

		active_thread_ = nullptr;
		std::cout << "back from partial ..." << std::endl;
		/******************************************/
		return true;
		
	}

	//cluster merge job
	bool SlaveProgress::RunClusterMergeJob(JobConfig jobconfig) {
		std::cout << "RunClusterMergeJob ..." << std::endl;
		std::string path = options_.job_path + "/Job" + jobconfig.getJobId() + ".log";
		std::ofstream file(path.c_str(), std::ios::trunc);

		/******************************************/
		MergeMapperController::Options mergemapper_options;
		SceneClustering::Options clustering_options;
		clustering_options.image_overlap = 50;
		clustering_options.leaf_max_num_images = 1000;
		//clustering_options.image_overlap = 90;
		//clustering_options.leaf_max_num_images = 500;

		mergemapper_options.workspace = options_.workspace_path;
		mergemapper_options.clusterNum = str2int(jobconfig.getValue("clusterNum"));
		mergemapper_options.imagePath = options_.image_path;
		mergemapper_options.localoutput = options_.output_path;
		MergeMapperController mapper(mergemapper_options, clustering_options, option_manager_);
		mapper.jobfile = &file;
		active_thread_ = &mapper;
		mapper.Start();
		mapper.Wait();

		active_thread_ = nullptr;

    std::string at_report_path = GetATReportPath();
    ATReport at_report;
    at_report.LoadFromXml(at_report_path);
    ATReport::TimeInfo &time_info = at_report.results.time_infos.sparse_reconstruction;
    time_info.end_time = std::time(0);
    time_info.cost_time = std::to_string(double(time_info.end_time - time_info.start_time) / 60) + " minute";

    std::cout << "at report path: " << at_report_path << std::endl;
    at_report.ExportXml(at_report_path);
		/******************************************/
		//gen dense job
		std::string msg = "Dense Reconstruction...";
		int nextprogress = PROGRESS_STATE_FINISH;
		int incprogress = PROGRESS_STATE_FINISH - PROGRESS_STATE_DENSE;
		jobconfig.genJobId(JOB_DENSE, options_.job_path, incprogress, nextprogress, msg);
		return true;
	}

	//dense cloud job
	bool SlaveProgress::RunDenseCloudJob(JobConfig jobconfig) {

		std::string path = options_.job_path + "/Job" + jobconfig.getJobId() + ".log";
		std::ofstream file(path.c_str(), std::ios::trunc);

		std::cout << "RunDenseCloudJob ..." << std::endl;
		/******************************************/
		PointAdjustController::Options pointAdjust_options;

		std::string slash = "/";
		std::string subpath = "merge";
		std::string mergePath = JoinPaths(options_.workspace_path, slash + subpath);
		pointAdjust_options.manageFile = mergePath;
		pointAdjust_options.workspace = options_.workspace_path;
		pointAdjust_options.imagePath = options_.image_path;
		pointAdjust_options.localoutput = options_.output_path;
		pointAdjust_options.gps_file_path = options_.gps_file_path;
		pointAdjust_options.database = options_.db_path;

    long long start_time = std::time(0);
		PointAdjustController mapper(pointAdjust_options, option_manager_);
		mapper.m_file = &file;
		active_thread_ = &mapper;
		mapper.Start();
		mapper.Wait();

		active_thread_ = nullptr;

    std::string at_report_path = GetATReportPath();
    ATReport at_report;
    at_report.LoadFromXml(at_report_path);
    ATReport::TimeInfo &time_info = at_report.results.time_infos.transform_gps;
    time_info.start_time = start_time;
    time_info.end_time = std::time(0);
    time_info.cost_time = std::to_string(double(time_info.end_time - time_info.start_time) / 60) + " minute";
    at_report.results.UpdateCostPercent();

    std::string at_report_final_path = GetFinalATReportPath();
    at_report.ExportXml(at_report_final_path);
    if (remove(at_report_path.c_str()) == 0) {
      std::cout << "delete at report file successed" << std::endl;
    }
    else {
      std::cout << "delete at report file failed" << std::endl;
    }
		/******************************************/
		//gen end job
		std::string msg = "Finish job...";
		int nextprogress = PROGRESS_STATE_FINISH;
		int incprogress = PROGRESS_STATE_FINISH - PROGRESS_STATE_FINISH;
		jobconfig.genJobId(JOB_FINISH, options_.job_path, incprogress, nextprogress, msg);
		return true;
	}

	//get enginStatus
	int SlaveProgress::getEngineStatus() {

	}

	//get an undistribute job
	std::string SlaveProgress::getJob() {
		return mJobList.getJob();

	}


	//create jobs queue
	bool SlaveProgress::genJobs() {
		return true;

	}

	//send partial result to project
	bool SlaveProgress::sendResult() {
		return true;
	}

	//wait for all part of a job finish
	bool SlaveProgress::waitForSynchronize(JobConfig jobconfig) {
		std::cout << "waitForSynchronize ..." << std::endl;
		int type = jobconfig.getJobType();
		if (type == JOB_CLUSTER_SYNCHRONIZE) {
			//TODO
			std::cout << "wait Synchronize ..." << std::endl;
			std::string joblistStr = jobconfig.getValue("joblist");

      std::string run_file_path = options_.jobQueuePath + +"/Running/" + options_.projectName + "_AT-file.xml";
	  /*
      FileMonotor file_monotor(run_file_path);
      file_monotor.Start();*/

			while (!(mJobList.checkFinishAll(joblistStr))) {
				if (!ExistsFile(run_file_path)) {
					std::cout << "stop waitForSynchronize****************" << std::endl;
					clearEngineFile();
					return false;
				}
       /* if (file_monotor.IsStopped()) {
          std::cout << "stop waitForSynchronize****************" << std::endl;
          return false;
        }*/
				//TODO wait ---------------
				srand((int)time(0));
				int time = rand() % 10 + 1;
				int sleepSecond = time * 1000;
				std::cout << "sleep 0-1s:" << sleepSecond << std::endl;
				std::this_thread::sleep_for(std::chrono::milliseconds(sleepSecond));
			}

     /* file_monotor.Stop();
      file_monotor.Wait();*/
			const std::vector<std::string> job_elems = StringSplit(joblistStr, ",");
			int size = job_elems.size();
			std::unordered_map<std::string, std::string> additionParam;
			additionParam.insert(std::make_pair("clusterNum", int2str(size)));

			//gen merge job
			//TODO  get cluster number 
			std::string msg = "Merge cluster...";
			int nextprogress = PROGRESS_STATE_DENSE;
			int incprogress = PROGRESS_STATE_DENSE - PROGRESS_STATE_MERGE;
			std::string tmpJobId;
			//jobconfig.genJobId(JOB_MERGE, options_.job_path, progress, msg);
			jobconfig.initJob(JOB_MERGE, tmpJobId, additionParam, options_.job_path, incprogress, nextprogress, msg);
			
			
		}

		return true;
	}

	//merge result together
	bool SlaveProgress::mergeResult() {
		return true;
	}

	bool SlaveProgress::setJobProgress(JobConfig jobconfig, std::string engine, int mode) {
		//get status
		int incprogress = jobconfig.getIncProgress();
		int nextprogress = jobconfig.getNextProgress();
		std::string msg = jobconfig.getMessage();
		std::string jobId = jobconfig.getJobId();
		std::cout << "call job :" << jobId << std::endl;

		//TODO lock
		FILE* lock = NULL;
		std::string slash = "/";
		std::string lockpath = "status.lock";
		std::string lockFile = JoinPaths(options_.feedback_path, slash + lockpath);
		while (fopen_s(&lock, lockFile.c_str(), "r+") != 0) {			//wait to get job lock
			srand((int)time(0));
			int time = rand() % 10 + 1;
			int sleepSecond = time * 1000;
			std::cout << "sleep 0-1s:" << sleepSecond << std::endl;
			std::this_thread::sleep_for(std::chrono::milliseconds(sleepSecond));
			continue;
		}
		//refresh progess file
		std::cout << "refresh progess file------" << std::endl;
		std::unordered_map<std::string, std::string> params;
		params.clear();
		std::string progressPath = options_.feedback_path + "/progress.conf";
		std::ifstream progressFile;
		progressFile.open(progressPath);
		CHECK(progressFile.is_open()) << progressPath;
		std::string progressResult = "";

		std::string line;
		while (std::getline(progressFile, line)) {
			StringTrim(&line);
			if (line != "") {
				std::cout << "line:" << line << std::endl;
				const std::vector<std::string> line_elems = StringSplit(line, "#");
				std::string key = line_elems[0];
				std::string value = line_elems[1];
				std::cout << "key:" << key << std::endl;
				std::cout << "value:" << value << std::endl;
				params.insert(std::make_pair(key, value));
			}
		}

		int lastProgress = str2int(params["progress"]);
		std::string lastMsg = params["msg"];
		int progress;
		if(mode == JOB_END_STATUS){
			std::cout << "=======job end======" << std::endl;
			progress  = lastProgress + incprogress;
			if (progress > nextprogress) {
				progress = nextprogress;
				//msg = lastMsg;
			}
		}
		else {
			std::cout << "=======job begin======" << std::endl;
			int type = jobconfig.getJobType();
			int statusProgress = 0;
			switch (type) {
			case JOB_FIRST:
				statusProgress = PROGRESS_STATE_BEGIN;
				break;
			case JOB_FEATURE_EXTRACTION:
				statusProgress = PROGRESS_STATE_FEATURE_EXTRACT;
				break;
			case JOB_FEATURE_MATCHING:
				statusProgress = PROGRESS_STATE_FEATURE_MATCH;
				break;
			case JOB_CLUSTER:
				statusProgress = PROGRESS_STATE_CLUSTER;
				break;
			case JOB_INCREMENTAL:
				statusProgress = PROGRESS_STATE_INCREMENT;
				break;
			case JOB_CLUSTER_SYNCHRONIZE:
				statusProgress = PROGRESS_STATE_SYNC;
				break;
			case JOB_MERGE:
				statusProgress = PROGRESS_STATE_MERGE;
				break;
			case JOB_DENSE:
				statusProgress = PROGRESS_STATE_DENSE;
				break;
			case JOB_FINISH:
				statusProgress = PROGRESS_STATE_FINISH;
				break;
			case JOB_RESPARSE_RECONSTRUTION:
				statusProgress = PROGRESS_STATE_BEGIN;
				break;
			default:
				std::cout << "ERROR: unsupport job!" << std::endl;
			}
			progress = lastProgress;
			if (progress < statusProgress) {
				progress = statusProgress;
			}
		}		

		progressResult += "jobId#" + jobId + "\n";
		progressResult += "progress#" + int2str(progress) + "\n";
		progressResult += "msg#" + msg + "\n";
		std::cout << "==========================" << std::endl;
		std::cout << "progressResult:" << progressResult << std::endl;
		std::cout << "==========================" << std::endl;
		progressFile.close();
		std::ofstream progressnewfile(progressPath, std::ios::trunc);
		CHECK(progressnewfile.is_open()) << progressPath;
		progressnewfile << progressResult << std::endl;
		progressnewfile.close();

		//refresh enginefile
		std::cout << "refresh enginefile------" << std::endl;
		std::unordered_map<std::string, std::string> engins;
		engins.clear();
		std::string enginePath = options_.feedback_path + "/engine.conf";
		std::ifstream engineFile;
		engineFile.open(enginePath);
		CHECK(engineFile.is_open()) << enginePath;
		std::string engineResult = "";

		while (std::getline(engineFile, line)) {
			StringTrim(&line);
			if (line != "") {
				std::cout << "line:" << line << std::endl;
				const std::vector<std::string> line_elems = StringSplit(line, "#");
				std::string key = line_elems[0];
				std::string value = line_elems[1];
				std::cout << "key:" << key << std::endl;
				std::cout << "value:" << value << std::endl;
				engins.insert(std::make_pair(key, value));
			}
		}
		if (engins.find(engine) == engins.end()) {
			engins.insert(std::make_pair(engine, jobId));
		}
		else {
			engins[engine] = jobId;
		}
		
		engineFile.close();
		std::cout << "read--" << std::endl;
		for(std::unordered_map<std::string,std::string>::iterator iter = engins.begin(); iter != engins.end(); iter++){
			engineResult += iter->first + "#" + iter->second + "\n";
			std::cout << "write key:" << iter->first << std::endl;
			std::cout << "write value:" << iter->second << std::endl;
		}
		std::ofstream enginenewfile(enginePath, std::ios::trunc);
		CHECK(enginenewfile.is_open()) << enginePath;
		enginenewfile << engineResult << std::endl;
		enginenewfile.close();

		refreshStatus();
		fclose(lock);
		return true;
	}

	bool SlaveProgress::refreshStatus() {
		std::cout << "refresh status---------------" << std::endl;
		std::unordered_map<std::string, std::string> params;
		std::string progressPath = options_.feedback_path + "/progress.conf";
		std::ifstream progressFile;
		progressFile.open(progressPath);
		CHECK(progressFile.is_open()) << progressPath;
		std::string progressResult = "";

		std::string line;
		while (std::getline(progressFile, line)) {
			StringTrim(&line);
			if (line != "") {
				std::cout << "line:" << line << std::endl;
				const std::vector<std::string> line_elems = StringSplit(line, "#");
				std::string key = line_elems[0];
				std::string value = line_elems[1];
				std::cout << "key:" << key << std::endl;
				std::cout << "value:" << value << std::endl;
				params.insert(std::make_pair(key, value));
			}
		}
		std::unordered_map<std::string, std::string> engins;
		std::string enginePath = options_.feedback_path + "/engine.conf";
		std::ifstream engineFile;
		engineFile.open(enginePath);
		CHECK(engineFile.is_open()) << enginePath;
		std::string engineResult = "";

		while (std::getline(engineFile, line)) {
			StringTrim(&line);
			if (line != "") {
				std::cout << "line:" << line << std::endl;
				const std::vector<std::string> line_elems = StringSplit(line, "#");
				std::string key = line_elems[0];
				std::string value = line_elems[1];
				std::cout << "key:" << key << std::endl;
				std::cout << "value:" << value << std::endl;
				engins.insert(std::make_pair(key, value));
			}
		}
		std::string writePath = options_.jobQueuePath + "/Running/" + options_.projectName + "_AT-file.xml";
		std::cout << "AT file path: " << writePath << std::endl;
		QFile file(writePath.c_str());
		if (!file.open(QFile::ReadWrite | QIODevice::Truncate))
		{
			std::cout << "Error: cannot open file" << std::endl;
			return false;
		}

		QXmlStreamWriter stream(&file);
		stream.setAutoFormatting(true);
		stream.writeStartDocument();

		stream.writeStartElement("Job");
		stream.writeTextElement("Project", QString::fromStdString(options_.project_path));

		stream.writeStartElement("JobFeedBack");
		stream.writeTextElement("Progress", QString::fromStdString(params["progress"]));
		stream.writeTextElement("Msg", QString::fromStdString(params["msg"]));
		stream.writeEndElement();
		stream.writeStartElement("Engines");
		
		for (std::unordered_map<std::string, std::string>::iterator iter = engins.begin(); iter != engins.end(); iter++) {
			stream.writeStartElement("Engine");
			stream.writeTextElement("IP", QString::fromStdString(iter->first));
			stream.writeTextElement("Job", QString::fromStdString(iter->second));
			stream.writeEndElement();
		}		
		stream.writeEndElement();

		stream.writeEndElement();

		stream.writeEndDocument();
		file.close();

		progressFile.close();
		engineFile.close();
		return true;
	}

  bool SlaveProgress::RunActiveThread(std::shared_ptr<Thread> active_thread) {
    std::string run_file_path = options_.jobQueuePath + +"/Running/" + options_.projectName + "_AT-file.xml";
    FileMonotor file_monotor(run_file_path, active_thread);

    active_thread->Start();
    file_monotor.Start();
    active_thread->Wait();
    file_monotor.Stop();
    file_monotor.Wait();

    return (active_thread->IsStopped() ? false : true);
  }

  std::string SlaveProgress::GetATReportPath() {
	  return options_.workspace_path + "/at_report.xml";
  }
  std::string SlaveProgress::GetFinalATReportPath() {
    return options_.workspace_path + "/" + options_.projectName + "_AT_Report.xml";
  }


    void SlaveProgress::export_at_gcp_report_settings(ATGCPReport &at_gcp_report) {
        if (options_.data_type == DataType::VIDEO) {
        }
        else if (options_.data_type == DataType::INDIVIDUAL ||
            options_.data_type == DataType::INTERNET) {
            Database database(*option_manager_.database_path);
            const size_t num_images = database.NumImages();
            if (ExistsFile(options_.gps_file_path)) {
            }
            else if (options_.vocab_tree_path.empty() || num_images < 50){
                at_gcp_report.settings.use_pos = false;
                at_gcp_report.settings.pair_select_mode = ATReport::PairMode::EXHAUSTIVE;
            }
            else {
                at_gcp_report.settings.use_pos = false;
                at_gcp_report.settings.pair_select_mode = ATReport::PairMode::VOCABULARY;
            }
        }
    }
    void SlaveProgress::export_at_gcp_report_global_connections(ATGCPReport &at_gcp_report, Reconstruction& reconstruction, bool before_at)
    {
        std::string at_report_path = options_.at_report_file_path;// options_.workspace_path + "/at_report.xml";
        ATReport at_report;
        at_report.LoadFromXml(at_report_path);

        at_gcp_report.results.global_info.conn_info.number_of_tested_pairs=at_report.results.connections_total.number_of_tested_pairs;
        at_gcp_report.results.global_info.conn_info.median_number_of_tested_pairs_per_photo = at_report.results.connections_total.median_number_of_tested_pairs_per_photo;
        if (before_at)
            at_gcp_report.results.global_info.conn_info.before_at_median_number_of_connected_photos_per_photo = at_report.results.connections_total.median_number_of_connected_photos_per_photo;
        else
            at_gcp_report.results.global_info.conn_info.after_at_median_number_of_connected_photos_per_photo = at_report.results.connections_total.median_number_of_connected_photos_per_photo;
    }
    void SlaveProgress::export_at_gcp_report_automatic_tie_points(ATGCPReport &at_gcp_report, Reconstruction& reconstruction, bool before_at)
    {
        std::map<int, int> image_p3dnum;
        for (auto& point3d : reconstruction.Points3D()) {
            for (auto& elem : point3d.second.Track().Elements()) {
                auto iter = image_p3dnum.find(elem.image_id);
                if (iter != image_p3dnum.end()) {
                    iter->second += 1;
                }
                else {
                    image_p3dnum.insert(std::make_pair(elem.image_id, 1));
                }
            }
        }
        auto CalculateRMS = [&reconstruction](const Image& image, int num_points_3d) {
            float sum_error = 0;
            Eigen::Matrix3x4d proj_matrix = image.ProjectionMatrix();
            Camera camera = reconstruction.Camera(image.CameraId());
            for (int i = 0; i < image.NumPoints2D(); ++i) {
                auto& point2d = image.Point2D(i);
                if (!point2d.HasPoint3D()) {
                    continue;
                }
                Point3D point3d = reconstruction.Point3D(point2d.Point3DId());
                sum_error += CalculateSquaredReprojectionError(point2d.XY(), point3d.XYZ(), proj_matrix, camera);
            }
            return sqrt(sum_error / num_points_3d);
        };
        std::vector<ATReport::PerPhoto> photo_infos;
        photo_infos.clear();
        for (auto& image : reconstruction.Images()) {
            ATReport::PerPhoto photo_info;
            photo_info.image_name = image.second.Name();
            photo_info.num_of_key_points = image.second.NumPoints2D();
            photo_info.num_of_points = image_p3dnum[image.first];
            photo_info.RMS_of_reprojection_errors = CalculateRMS(image.second, photo_info.num_of_points);
            photo_infos.emplace_back(photo_info);
        }
        std::sort(photo_infos.begin(), photo_infos.end(), [](const ATReport::PerPhoto& photo1, const ATReport::PerPhoto& photo2) {
            return photo1.num_of_key_points < photo2.num_of_key_points;
        });
        int median_index = photo_infos.size() / 2;
        at_gcp_report.results.PerPoint_automatic_tie_points.median_number_of_key_points_per_photo = photo_infos[median_index].num_of_key_points;

        std::sort(photo_infos.begin(), photo_infos.end(), [](const ATReport::PerPhoto& photo1, const ATReport::PerPhoto& photo2) {
            return photo1.num_of_points < photo2.num_of_points;
        });
        median_index = photo_infos.size() / 2;
        if (before_at)
            at_gcp_report.results.PerPoint_automatic_tie_points.before_at_median_number_of_points_per_photo = photo_infos[median_index].num_of_points;
        else
            at_gcp_report.results.PerPoint_automatic_tie_points.after_at_median_number_of_points_per_photo = photo_infos[median_index].num_of_points;
        
        std::vector<int> tracks;
        for (auto& point3d : reconstruction.Points3D()) {
            tracks.push_back(point3d.second.Track().Length());
        }
        std::sort(tracks.begin(), tracks.end(), [](int x, int y) {return x < y; });
        median_index = tracks.size() / 2;

        //点云投影误差。（前、后）
        const std::unordered_set<point3D_t>& point3D_ids = reconstruction.Point3DIds();
        std::vector<double> reproj_errs;
        for (const auto point3D_id : point3D_ids) {
            if (!reconstruction.ExistsPoint3D(point3D_id)) {
                continue;
            }
            class Point3D& point3D = reconstruction.Point3D(point3D_id);
            if (point3D.Track().Length() < 2) {
                continue;
            }
            double reproj_error_sum = 0.0;

            for (const auto& track_el : point3D.Track().Elements()) {
                const class Image& image = reconstruction.Image(track_el.image_id);
                const class Camera& camera = reconstruction.Camera(image.CameraId());
                const Point2D& point2D = image.Point2D(track_el.point2D_idx);
                const double squared_reproj_error = CalculateSquaredReprojectionError(
                    point2D.XY(), point3D.XYZ(), image.Qvec(), image.Tvec(), camera);
                    reproj_error_sum += std::sqrt(squared_reproj_error);
            }
            reproj_errs.push_back(reproj_error_sum / point3D.Track().Length());
        }

        double reproj_errors_squared_sum;
        for (auto &err: reproj_errs){
            reproj_errors_squared_sum += err*err;
        }
        float rms_of_reprojection_errors = sqrt(reproj_errors_squared_sum / reproj_errs.size());
        std::sort(reproj_errs.begin(), reproj_errs.end(), [](double x, double y) {return x < y; });
        median_index = reproj_errs.size() / 2;
        float median_reprojection_error = reproj_errs[median_index];

        if (before_at) {
            at_gcp_report.results.PerPoint_automatic_tie_points.before_at_median_number_of_photos_per_point = tracks[median_index];
            at_gcp_report.results.PerPoint_automatic_tie_points.before_at_rms_of_reprojection_errors = rms_of_reprojection_errors;
            at_gcp_report.results.PerPoint_automatic_tie_points.before_at_number_of_points = reconstruction.Points3D().size();
            at_gcp_report.results.global_info.automatic_tie_points.before_at_number_of_points = reconstruction.Points3D().size();
            at_gcp_report.results.global_info.automatic_tie_points.before_at_rms_of_reprojection_errors = rms_of_reprojection_errors;
            at_gcp_report.results.global_info.automatic_tie_points.before_at_median_reprojection_error = median_reprojection_error;
        }
        else {
            at_gcp_report.results.PerPoint_automatic_tie_points.after_at_median_number_of_photos_per_point = tracks[median_index];
            at_gcp_report.results.PerPoint_automatic_tie_points.after_at_rms_of_reprojection_errors = rms_of_reprojection_errors;
            at_gcp_report.results.PerPoint_automatic_tie_points.after_at_number_of_points = reconstruction.Points3D().size();
            at_gcp_report.results.global_info.automatic_tie_points.after_at_number_of_points = reconstruction.Points3D().size();
            at_gcp_report.results.global_info.automatic_tie_points.after_at_rms_of_reprojection_errors = rms_of_reprojection_errors;
            at_gcp_report.results.global_info.automatic_tie_points.after_at_median_reprojection_error = median_reprojection_error;
        }
    }
    void SlaveProgress::export_at_gcp_report(Reconstruction& reconstruction, bool before_at) {
        std::string at_gcp_report_path = options_.workspace_path + "/" + options_.projectName + + "_AT_GCP_Report.xml";
        ATGCPReport at_gcp_report;
        at_gcp_report.LoadFromXml(at_gcp_report_path);

        export_at_gcp_report_settings(at_gcp_report);
        export_at_gcp_report_global_connections(at_gcp_report, reconstruction, before_at);
        export_at_gcp_report_automatic_tie_points(at_gcp_report, reconstruction, before_at);

        //计算RMS of reprojection errors [px]
        const std::vector<gcp::GCPInfo> gcp_infos = reconstruction.GCPInfos();
        std::vector<double> per_point_errors;
        std::vector<double> per_point_horizontal_errors;
        std::vector<double> per_point_vertical_errors;
        std::map<std::string, ATGCPReport::PerPhoto_ControlPoints> &photo_infos_map = at_gcp_report.results.Perphoto_photo_infos_map;
        std::map<std::string, ATGCPReport::PerPoint_ControlPoints> &point_infos_map = at_gcp_report.results.Perpoint_control_points_map;
        if (before_at)
            photo_infos_map.clear();
        if (before_at)
            point_infos_map.clear();

        for (auto& gcp_info : gcp_infos) {
            double per_point_err = 0;
            double per_point_squared_err = 0;
            double observation_error = 0;
            //double observation_num = 0;
            double per_point_horizontal_error = 0;
            double per_point_horizontal_squared_error = 0;
            double per_point_vertical_error = 0;
            double per_point_vertical_squared_error = 0;
            int error_num = 0;
            for (auto& observation : gcp_info.observations) {
                const Image& image = reconstruction.Image(observation.image_id);
                const Camera& camera = reconstruction.Camera(image.CameraId());
                Eigen::Vector2d pixel_coord = ProjectPointToImage(gcp_info.utm_coord, image.ProjectionMatrix(), camera);
                observation_error = (observation.pixel_coord - pixel_coord).norm();
                per_point_err += observation_error;
                per_point_squared_err += observation_error*observation_error;
                per_point_horizontal_error += abs(observation.pixel_coord[0] - pixel_coord[0]);
                per_point_horizontal_squared_error += abs(observation.pixel_coord[0] - pixel_coord[0])*abs(observation.pixel_coord[0] - pixel_coord[0]);
                per_point_vertical_error += abs(observation.pixel_coord[1] - pixel_coord[1]);
                per_point_vertical_squared_error += abs(observation.pixel_coord[1] - pixel_coord[1])*abs(observation.pixel_coord[1] - pixel_coord[1]);
                error_num++;

                auto iter = photo_infos_map.find(image.Name());
                if (iter != photo_infos_map.end()) {
                    if (before_at) {
                        iter->second.before_at_number_of_points += 1;
                    }
                    else{
                        iter->second.after_at_number_of_points += 1;
                    }
                    iter->second.reprojection_squared_errors_sum += observation_error*observation_error;
                }
                else
                {
                    ATGCPReport::PerPhoto_ControlPoints photo_info;
                    photo_info.photogroup = image.CameraId();
                    photo_info.file_name = image.Name();
                    if (before_at) {
                        photo_info.before_at_number_of_points = 1;
                    }
                    else {
                        photo_info.after_at_number_of_points = 1;
                    }
                    photo_info.reprojection_squared_errors_sum = observation_error*observation_error;
                    photo_infos_map.insert(make_pair(image.Name(),photo_info));
                }
            }
            per_point_errors.emplace_back(per_point_err / error_num);
            per_point_horizontal_errors.emplace_back(per_point_horizontal_error / error_num);
            per_point_vertical_errors.emplace_back(per_point_vertical_error / error_num);

            //插入perpoint，
            auto iter = point_infos_map.find(std::to_string(gcp_info.id));
            if(iter != point_infos_map.end())
            {
                if (before_at)
                {
                    std::cout << "err" << std::endl;
                }
                else
                {
                    iter->second.after_at_number_of_photos = gcp_info.observations.size();
                    iter->second.after_at_rms_of_reprojection_errors = sqrt(per_point_squared_err / gcp_info.observations.size());
                    iter->second.after_at_rms_of_horizontal_errors = sqrt(per_point_horizontal_squared_error / gcp_info.observations.size());
                    iter->second.after_at_rms_of_vertical_errors = sqrt(per_point_vertical_squared_error / gcp_info.observations.size());
                }
            }
            else
            {
                if (before_at)
                {
                    ATGCPReport::PerPoint_ControlPoints point_info;
                    point_info.name = std::to_string(gcp_info.id);
                    point_info.before_at_number_of_photos = gcp_info.observations.size();
                    point_info.before_at_rms_of_reprojection_errors = sqrt(per_point_squared_err / gcp_info.observations.size());
                    point_info.before_at_rms_of_horizontal_errors = sqrt(per_point_horizontal_squared_error / gcp_info.observations.size());
                    point_info.before_at_rms_of_vertical_errors = sqrt(per_point_vertical_squared_error / gcp_info.observations.size());
                    point_infos_map.insert(std::make_pair(std::to_string(gcp_info.id), point_info));
                }
                else
                {
                    std::cout << "err" << std::endl;
                }
            }
        }

        for (auto &photo_info : photo_infos_map)
        {
            if (before_at)
            {
                photo_info.second.before_at_rms_of_reprojection_errors = 
                    sqrt(photo_info.second.reprojection_squared_errors_sum / photo_info.second.before_at_number_of_points);
            }
            else
            {
                photo_info.second.after_at_rms_of_reprojection_errors =
                    sqrt(photo_info.second.reprojection_squared_errors_sum / photo_info.second.after_at_number_of_points);
            }
        }

        double rms_errors =0;
        double horizontal_rms_errors=0;
        double vertical_rms_errors=0;
        for (auto err : per_point_errors) {
            rms_errors += err*err;
        }
        for (auto err : per_point_horizontal_errors) {
            horizontal_rms_errors += err*err;
        }
        for (auto err : per_point_vertical_errors) {
            vertical_rms_errors += err*err;
        }

        sort(per_point_errors.begin(), per_point_errors.end());
        double error_median = per_point_errors[per_point_errors.size() / 2];

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////
        at_gcp_report.results.global_info.positioning_mode = "Use control points for adjustment";
        if (before_at)
        {
            at_gcp_report.results.global_info.control_points.before_at_number_of_points = gcp_infos.size();
            at_gcp_report.results.global_info.control_points.before_at_median_reprojection_error = error_median;
            at_gcp_report.results.global_info.control_points.before_at_rms_of_reprojection_errors = sqrt(rms_errors / per_point_errors.size());
            at_gcp_report.results.global_info.control_points.before_at_rms_of_distances_to_rays = 0;//
            at_gcp_report.results.global_info.control_points.before_at_rms_of_3d_errors = 0;//
            at_gcp_report.results.global_info.control_points.before_at_rms_of_horizontal_errors = sqrt(horizontal_rms_errors / per_point_errors.size());
            at_gcp_report.results.global_info.control_points.before_at_rms_of_vertical_errors = sqrt(vertical_rms_errors / per_point_errors.size());
        }
        else
        {
            at_gcp_report.results.global_info.control_points.after_at_number_of_points = gcp_infos.size();
            at_gcp_report.results.global_info.control_points.after_at_median_reprojection_error = error_median;
            at_gcp_report.results.global_info.control_points.after_at_rms_of_reprojection_errors = sqrt(rms_errors / per_point_errors.size());
            at_gcp_report.results.global_info.control_points.after_at_rms_of_distances_to_rays = 0;//
            at_gcp_report.results.global_info.control_points.after_at_rms_of_3d_errors = 0;//
            at_gcp_report.results.global_info.control_points.after_at_rms_of_horizontal_errors = sqrt(horizontal_rms_errors / per_point_errors.size());
            at_gcp_report.results.global_info.control_points.after_at_rms_of_vertical_errors = sqrt(vertical_rms_errors / per_point_errors.size());
        }
        at_gcp_report.ExportXml(at_gcp_report_path);
    }
}