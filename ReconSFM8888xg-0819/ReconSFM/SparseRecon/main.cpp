﻿/*
-fexec - charset = gbk   // 可执行文件中的中文字符资源为 GBK，控制台显示中文
- finput - charset = UTF - 8 // 源代码 main.cpp 为 UTF-8编码
*/
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include "base/similarity_transform.h"
#include "sparse_reconstruction.h"
#include "controllers/bundle_adjustment.h"
#include "controllers/hierarchical_mapper.h"
#include "estimators/coordinate_frame.h"
#include "feature/extraction.h"
#include "feature/matching.h"
#include "feature/utils.h"
#include "retrieval/visual_index.h"
#include "ui/main_window.h"
#include "util/opengl_utils.h"
#include "util/version.h"
#include "cmdLine/cmdLine.h"
#include "MasterProgress.h"
#include "SlaveProgress.h"
#include <iostream>
#include <string>
#include "util/misc.h"
#include <math.h>
#define GOOGLE_GLOG_DLL_DECL    // 由于我们使用的glog的静态库，一定要定义这个宏
#include <boost/filesystem.hpp>
#include "glog/logging.h"
#include <fstream>
#include <tchar.h>

using namespace sfmrecon;
using namespace google;
#ifdef CUDA_ENABLED
const bool kUseOpenGL = false;
#else
const bool kUseOpenGL = true;
#endif


int main(int argc, char** argv) {
	
	const int TYPE_MASTER = 1;
	const int TYPE_ENGINE = 2;
	const int TYPE_SETTING = 3;
	const int TYPE_ROLLBACK = 4;
	const int TYPE_CLEAR = 5;

	CmdLine cmd;
	int exeType = -1;
	std::string confPath;
	cmd.add(make_option('t', exeType, "exe_type"));
	cmd.add(make_option('p', confPath, "conf_path"));

	//	sparse_reconstruction_options.gps_file_path = JoinPaths(sparse_reconstruction_options.image_path,"GPS.txt");
	try {
		if (argc < 2) throw std::string("Invalid command line parameter.");
		cmd.process(argc, argv);
	}
	catch (const std::string& s) {
		std::cerr << "Usage: " << argv[0] << '\n'
			<< "[-t|--exetype] the exe type:1-Master,2-Engine,3-Setting\n"
			<< "[-p|--param conf for exe\n"
			<< std::endl;
		std::cerr << s << std::endl;
		return EXIT_FAILURE;
	}

	std::ifstream confFile;
	confFile.open(confPath);
	CHECK(confFile.is_open()) << confPath;

	if (exeType == TYPE_MASTER) {
		std::cout << "===============================================================" << std::endl;
		std::cout << "Start Master................." << std::endl;
		std::cout << "===============================================================" << std::endl;
		std::cout << "Enter project Path:" << std::endl;
		std::unordered_map<std::string, std::string> params;
		std::string line;
		while (std::getline(confFile, line)) {
			//QString tmpString = QString::fromUtf8(line);
			StringTrim(&line);
			if (line != "") {
				std::cout << "line:" << line << std::endl;
				const std::vector<std::string> line_elems = StringSplit(line, "#");
				std::string key = line_elems[0];
				std::string value = line_elems[1];
				std::cout << "key:" << key << std::endl;
				std::cout << "value:" << value << std::endl;
				params.insert(std::make_pair(key, value));
			}
		}
		confFile.close();
		MasterProgress::Options options;
		if (params.find("projectPath") != params.end()){
			options.projectPath = params["projectPath"];
		}
		else {
			std::cout << "ERROR: project path not defined!" << std::endl;
		}
		if (params.find("projectName") != params.end()) {
			options.projectName = params["projectName"];
		}
		if (params.find("vocabTreePath") != params.end()) {
			options.vocabTreePath = params["vocabTreePath"];
		}
		else {
			std::cout << "ERROR: vocabTreePath not defined!" << std::endl;
		}
		if (params.find("masterJobQueuePath") != params.end()) {
			options.jobQueuePath = params["masterJobQueuePath"];
		}
		else {
			std::cout << "ERROR: enginPath not defined!" << std::endl;
		}
		if (params.find("imagePath") != params.end()) {
			options.imagePath = params["imagePath"];
		}
		if (params.find("gpsPath") != params.end()) {
			options.gpsPath = params["gpsPath"];
		}
		if (params.find("gcpPath") != params.end()) {
			options.gcpPath = params["gcpPath"];
        }
        if (params.find("atReportPath") != params.end()) {
            options.atReportPath = params["atReportPath"];
        }
		if (params.find("atPath") != params.end()) {
			options.atPath = params["atPath"];
		}
		if (params.find("focalPath") != params.end()) {
			options.focalPath = params["focalPath"];
		}
		if (params.find("numCurProcess") != params.end()) {
			options.numCurProcess = str2int(params["numCurProcess"]);
		}
		if (params.find("maxFeaturePoint") != params.end()) {
			options.max_feature_point = str2int(params["maxFeaturePoint"]);
		}

		MasterProgress master;
		master.initialProject(options);

	}
	else if (exeType == TYPE_ENGINE) {
		std::cout << "===============================================================" << std::endl;
		std::cout << "Start Engine................." << std::endl;
		std::cout << "===============================================================" << std::endl;
		std::cout << "Enter engine Path:" << std::endl;
		std::unordered_map<std::string, std::string> params;
		std::string line;
		while (std::getline(confFile, line)) {
			StringTrim(&line);
			if (line != "") {
				std::cout << "line:" << line << std::endl;
				const std::vector<std::string> line_elems = StringSplit(line, "#");
				std::string key = line_elems[0];
				std::string value = line_elems[1];
				std::cout << "key:" << key << std::endl;
				std::cout << "value:" << value << std::endl;
				params.insert(std::make_pair(key, value));
			}
		}
		confFile.close();
		SlaveProgress::inputOptions options;
		if (params.find("settingJobQueuePath") != params.end()) {
			options.setting_jobqueue_path = params["settingJobQueuePath"];
		}
		else {
			std::cout << "ERROR: jobQueue Path not defined!" << std::endl;
		}
		if (params.find("outputPath") != params.end()) {
			options.output_path = params["outputPath"];
		}
		else {
			std::cout << "ERROR: output Path not defined!" << std::endl;
		}
		
		ReconstructionManager reconstruction_manager;
		SlaveProgress controller(&reconstruction_manager, options);
		controller.Start();
		controller.Wait();
		if (controller.IsStopped()) {
			std::cout << "Exit!" << std::endl;
		}
	}
	else if (exeType == TYPE_ROLLBACK) {
		std::cout << "===============================================================" << std::endl;
		std::cout << "Engine RollBack................." << std::endl;
		std::cout << "===============================================================" << std::endl;
		std::cout << "Enter engine Path:" << std::endl;
		std::unordered_map<std::string, std::string> params;
		std::string line;
		while (std::getline(confFile, line)) {
			StringTrim(&line);
			if (line != "") {
				std::cout << "line:" << line << std::endl;
				const std::vector<std::string> line_elems = StringSplit(line, "#");
				std::string key = line_elems[0];
				std::string value = line_elems[1];
				std::cout << "key:" << key << std::endl;
				std::cout << "value:" << value << std::endl;
				params.insert(std::make_pair(key, value));
			}
		}
		confFile.close();
		SlaveProgress::inputOptions options;

		if (params.find("projectPath") != params.end()) {
			options.project_path = params["projectPath"];
		}
		else {
			std::cout << "ERROR: projectPath not defined!" << std::endl;
		}
		if (params.find("outputPath") != params.end()) {
			options.output_path = params["outputPath"];
		}
		else {
			std::cout << "ERROR: output Path not defined!" << std::endl;
		}

		ReconstructionManager reconstruction_manager;
		SlaveProgress controller(&reconstruction_manager, options);
		controller.forceRollBack();
	}
	else if (exeType == TYPE_CLEAR) {
		std::cout << "===============================================================" << std::endl;
		std::cout << "Engine clear................." << std::endl;
		std::cout << "===============================================================" << std::endl;

		std::unordered_map<std::string, std::string> params;
		std::string line;
		while (std::getline(confFile, line)) {
			StringTrim(&line);
			if (line != "") {
				std::cout << "line:" << line << std::endl;
				const std::vector<std::string> line_elems = StringSplit(line, "#");
				std::string key = line_elems[0];
				std::string value = line_elems[1];
				std::cout << "key:" << key << std::endl;
				std::cout << "value:" << value << std::endl;
				params.insert(std::make_pair(key, value));
			}
		}
		confFile.close();
		SlaveProgress::inputOptions options;

		if (params.find("settingJobQueuePath") != params.end()) {
			options.setting_jobqueue_path = params["settingJobQueuePath"];
		}
		else {
			std::cout << "ERROR: setting_jobqueue_path not defined!" << std::endl;
		}
		if (params.find("outputPath") != params.end()) {
			options.output_path = params["outputPath"];
		}
		else {
			std::cout << "ERROR: output Path not defined!" << std::endl;
		}

		ReconstructionManager reconstruction_manager;
		SlaveProgress controller(&reconstruction_manager, options);
		controller.removeEngine();
	}
	else {
		std::cout << "Error: unsupport type" << std::endl;
	}
	

	
	return EXIT_SUCCESS;
}