#include "JobList.h"
#include <QDebug>
#include <QMessageBox>
#include <QFile>
#include <QDir>
#include <QFileInfoList>
#include <QMessageBox>
#include <QSharedMemory>
#include <QSystemSemaphore>
#include <QDateTime>
#include <QDebug>
#include<chrono>
#include <atomic>
#include <thread>

namespace sfmrecon {
	JobList::JobList(){
		mJobPath = "";
		mJobListFile = "";
		mJobWaitPath = "";
		mJobHandlerPath = "";
		mJobFinishPath = "";
	}

	bool JobList::isEmptyList() {
		if (mJobPath == "") {
			return true;
		}
		else {
			return false;
		}
	}

	void JobList::initial(std::string path){
		mJobPath = path;
		std::string slash = "/";
		std::string listpath = "jobList.txt";
		mJobListFile = JoinPaths(mJobPath, slash + listpath);
		std::string waitpath = "wait";
		mJobWaitPath = JoinPaths(mJobPath, slash + waitpath);
		std::string handlepath = "handle";
		mJobHandlerPath = JoinPaths(mJobPath, slash + handlepath);
		std::string finishpath = "finish";
		mJobFinishPath = JoinPaths(mJobPath, slash + finishpath);
	}


	JobList::~JobList(){
	}

	bool JobList::rollBackSingle(std::string workspace) {
		std::string slash = "/";
		std::cout << "workspace:" << workspace << std::endl;
		std::cout << "mJobHandlerPath:" << mJobHandlerPath << std::endl;
		if (ExistsDir(workspace)) {
			std::string unfinishedFiles = "";
			QDir dirHandle(QString::fromStdString(workspace));
			QFileInfoList list = dirHandle.entryInfoList();
			for (int i = 0; i < list.count(); i++) {
				QFileInfo fileInfo = list.at(i);
				if (fileInfo.isDir() | fileInfo.fileName() == "." | fileInfo.fileName() == "..")
				{
					continue;
				}
				std::string Firstfilename = StringReplace(fileInfo.absoluteFilePath().toStdString(), "\\", "/");
				int lastpos = Firstfilename.find_last_of('/');
				std::string lastname(Firstfilename.substr(lastpos + 1));
				std::cout << "unfinished Job: " << lastname << "......" << std::endl;
				std::string currentPath = workspace + "/" + lastname;
				std::string fromPath = mJobHandlerPath + "/" + lastname;
				std::string toPath = mJobWaitPath + "/" + lastname;
				if (QFile::exists(QString::fromStdString(currentPath))) {
					QFile::remove(QString::fromStdString(currentPath));
				}
				if (QFile::exists(QString::fromStdString(toPath))) {
					QFile::remove(QString::fromStdString(toPath));
				}
				QDir dir;
				dir.rename(QString::fromStdString(fromPath), QString::fromStdString(toPath));
				unfinishedFiles += lastname + "\n";
			}

			//TODO
			//mJobListFile

			std::cout << "begin rollback joblist ------------" << std::endl;
			FILE* lock = NULL;
			std::string slash = "/";
			std::string lockpath = "job.lock";
			std::string lockFile = JoinPaths(mJobPath, slash + lockpath);
			while (fopen_s(&lock, lockFile.c_str(), "r+") != 0) {			//wait to get job lock
				srand((int)time(0));
				int time = rand() % 10 + 1;
				int sleepSecond = time * 10000;
				std::cout << "sleep 0-1s:" << sleepSecond << std::endl;
				std::this_thread::sleep_for(std::chrono::milliseconds(sleepSecond));
				continue;
			}
			FILE* readfile = NULL;
			fopen_s(&readfile, mJobListFile.c_str(), "r+");
			char  loadFilePath[1000];
			//get first line
			bool isfirstLine = false;
			while (EOF != fscanf(readfile, "%s", loadFilePath))
			{
				std::string firstline = loadFilePath;
				StringTrim(&firstline);
				if (firstline == "") {           //empty
					continue;
				}
				else if (firstline.find("Job_") != firstline.npos) {      //is a job name
					unfinishedFiles += firstline + "\n";
				}
				else if (!isfirstLine && isNums(firstline)) {
					unfinishedFiles = firstline + "\n" + unfinishedFiles;
					isfirstLine = true;
				}
			}
			fclose(readfile);
			FILE* writefile = NULL;
			fopen_s(&writefile, mJobListFile.c_str(), "w+");
			fprintf(writefile, "%s\n", unfinishedFiles.c_str());
			fclose(writefile);
			std::cout << "end rollback joblist ------------" << std::endl;

			fclose(lock);
		}


		return true;
	}

	bool JobList::rollBack() {
		std::string slash = "/";
		std::cout << "mJobHandlerPath:" << mJobHandlerPath << std::endl;
		if (ExistsDir(mJobHandlerPath)) {
			std::string unfinishedFiles = "";
			QDir dirHandle(QString::fromStdString(mJobHandlerPath));
			QFileInfoList list = dirHandle.entryInfoList();
			for (int i = 0; i < list.count(); i++) {
				QFileInfo fileInfo = list.at(i);
				if (fileInfo.isDir() | fileInfo.fileName() == "." | fileInfo.fileName() == "..")
				{
					continue;
				}
				std::string Firstfilename = StringReplace(fileInfo.absoluteFilePath().toStdString(), "\\", "/");
				int lastpos = Firstfilename.find_last_of('/');
				std::string lastname(Firstfilename.substr(lastpos + 1));
				std::cout << "unfinished Job: " << lastname << "......" << std::endl;
				std::string fromPath = mJobHandlerPath + "/" + lastname;
				std::string toPath = mJobWaitPath + "/" + lastname;
				if (QFile::exists(QString::fromStdString(toPath))) {
					QFile::remove(QString::fromStdString(toPath));
				}
				QDir dir;
				dir.rename(QString::fromStdString(fromPath), QString::fromStdString(toPath));
				unfinishedFiles += lastname + "\n";
			}

			//TODO
			//mJobListFile

			std::cout << "begin rollback joblist ------------" << std::endl;
			FILE* lock = NULL;
			std::string slash = "/";
			std::string lockpath = "job.lock";
			std::string lockFile = JoinPaths(mJobPath, slash + lockpath);
			while (fopen_s(&lock, lockFile.c_str(), "r+") != 0) {			//wait to get job lock
				srand((int)time(0));
				int time = rand() % 10 + 1;
				int sleepSecond = time * 10000;
				std::cout << "sleep 0-1s:" << sleepSecond << std::endl;
				std::this_thread::sleep_for(std::chrono::milliseconds(sleepSecond));
				continue;
			}
			FILE* readfile = NULL;
			fopen_s(&readfile, mJobListFile.c_str(), "r+");
			char  loadFilePath[1000];
			//get first line
			bool isfirstLine = false;
			while (EOF != fscanf(readfile, "%s", loadFilePath))
			{
				std::string firstline = loadFilePath;
				StringTrim(&firstline);
				if (firstline == "") {           //empty
					continue;
				}else if (firstline.find("Job_") != firstline.npos) {      //is a job name
					unfinishedFiles += firstline + "\n";
				}else if (!isfirstLine && isNums(firstline)) {
					unfinishedFiles = firstline + "\n" + unfinishedFiles;
					isfirstLine = true;
				}
			}
			fclose(readfile);
			FILE* writefile = NULL;
			fopen_s(&writefile, mJobListFile.c_str(), "w+");
			fprintf(writefile, "%s\n", unfinishedFiles.c_str());
			fclose(writefile);
			std::cout << "end rollback joblist ------------" << std::endl;

      		fclose(lock);
		}


		return true;
	}

	bool JobList::isNums(std::string str) {
		bool result = true;
		for (int i = 0; i < str.size(); i++)
		{
			int tmp = (int)str[i];
			if (tmp >= 48 && tmp <= 57)
			{
				continue;
			}
			else
			{
				result =  false;
				break;
			}
		}
		return result;
	}

	int JobList::del_assigned_row(FILE *fp_in, const char *path, int assign_line, std::string &str)
	{
		int lines = 1;
		char path_tmp[256] = { 0 };
		char line[ONE_ROW_MAX_CHR] = { 0 };
		FILE *fp_out = NULL;

		sprintf(path_tmp, "%s.tmp", path);
		//fp_in = fopen(path, "r");
		if (fp_in == NULL) {
			printf("open %s failed!\n", path);
			return -1;
		}
		fp_out = fopen(path_tmp, "a+");
		if (fp_out == NULL) {
			printf("create tmp file %s failed!\n", path_tmp);
			return -1;
		}

		while (fgets(line, ONE_ROW_MAX_CHR, fp_in) != NULL)
		{
			if (lines == 1)
			{
				str = line;
			}

			if (lines != assign_line) {
				fwrite(line, 1, strlen(line), fp_out);
			}


			++lines;
			memset(line, 0, ONE_ROW_MAX_CHR);
		}


		if (fclose(fp_out) != 0) {
			printf("close file %s failed!\n", path_tmp);
			return -2;
		}

		if (fclose(fp_in) != 0) {
			printf("close file %s failed!\n", path);
			return -2;
		}
		QDateTime current_date_time = QDateTime::currentDateTime();
		QString current_time = current_date_time.toString("hh:mm:ss.zzz ");
		qDebug() << current_time;
		remove(path);
		rename(path_tmp, path);
		current_date_time = QDateTime::currentDateTime();
		current_time = current_date_time.toString("hh:mm:ss.zzz ");
		qDebug() << current_time;
		return 0;
	}
	std::string JobList::getJob() {
		std::cout << "get a job -------------" << std::endl;
		FILE* lock = NULL;
		std::string slash = "/";
		std::string lockpath = "job.lock";
		std::string lockFile = JoinPaths(mJobPath, slash + lockpath);
		while (fopen_s(&lock, lockFile.c_str(), "r+") != 0) {			//wait to get job lock
			srand((int)time(0));
			int time = rand() % 10 + 1;
			int sleepSecond = time * 10000;
			std::cout << "sleep 0-1s:" << sleepSecond << std::endl;
			std::this_thread::sleep_for(std::chrono::milliseconds(sleepSecond));
			continue;
		}
		int maxJobId;
		getMaxJobId(maxJobId);
		std::cout << "getMaxId :" << maxJobId << std::endl;
		FILE* readfile = NULL;
		fopen_s(&readfile, mJobListFile.c_str(), "r+");
		char  loadImgPath[1000];
		//get first line
		std::string resultJob = "null";
		bool isFirstJob = false;
		std::string jobs = "";
		jobs += int2str(maxJobId) + "\n";
		while(EOF != fscanf(readfile, "%s", loadImgPath))
		{
			std::string firstline = loadImgPath;
			StringTrim(&firstline);
			if (firstline.find("Job_") != firstline.npos) {      //is a job name
				if (!isFirstJob) {
					resultJob = firstline;                             // get first job
					std::cout << "resultJob :" << resultJob << std::endl;
					isFirstJob = true;
				}
				else {
					jobs += firstline + "\n";
				}
			}
		}
		std::cout << "jobs :" << jobs << std::endl;
		fclose(readfile);
		FILE* writefile = NULL;
		fopen_s(&writefile, mJobListFile.c_str(), "w+");
		fprintf(writefile, "%s\n", jobs.c_str());
		fclose(writefile);

		fclose(lock);
		return resultJob;

	}

	bool JobList::getMaxJobId(int &maxNum){
	//	std::cout << fp << std::endl;
		FILE* file = NULL;
		fopen_s(&file, mJobListFile.c_str(), "r+");
		char  loadImgPath[1000];
		//get first line
		if (EOF != fscanf(file, "%s", loadImgPath))
		{
			std::string firstline = loadImgPath;
			std::cout << "firstline:" << firstline << std::endl;
			StringTrim(&firstline);
			maxNum = str2int(firstline);			
		}
		else {
			std::cout << "initial an empty file..." << std::endl;
			fwrite("0\n", 1, 3, file);
			maxNum = 0;
		}
		fclose(file);
		return true;
	}

#define ONE_ROW_MAX_CHR 256
	static int add_assigned_row(FILE *fp_in, const char *path, int assign_line, const char* data, int datalength)
	{
		int lines = 1;
		char path_tmp[256] = { 0 };
		char line[ONE_ROW_MAX_CHR] = { 0 };
		FILE *fp_out = NULL;

		sprintf(path_tmp, "%s.tmp", path);
		//fp_in = fopen(path, "r");
		if (fp_in == NULL) {
			printf("open %s failed!\n", path);
			return -1;
		}
		fp_out = fopen(path_tmp, "a+");
		if (fp_out == NULL) {
			printf("create tmp file %s failed!\n", path_tmp);
			return -1;
		}

		while (fgets(line, ONE_ROW_MAX_CHR, fp_in) != NULL)
		{
			std::cout << line << std::endl;
			if (lines == 1)
			{
				int id = str2int(line);
				std::string num = int2str(id + 1) + "\n";
				//std::cout << id << std::endl;
				fwrite(num.c_str(), 1, strlen(num.c_str()), fp_out);
			}
			else {
				fwrite(line, 1, strlen(line), fp_out);
			}

			++lines;
			memset(line, 0, ONE_ROW_MAX_CHR);
		}

		fwrite(data, 1, datalength, fp_out);

		if (fclose(fp_out) != 0) {
			printf("close file %s failed!\n", path_tmp);
			return -2;
		}

		if (fclose(fp_in) != 0) {
			printf("close file %s failed!\n", path);
			return -2;
		}
		QDateTime current_date_time = QDateTime::currentDateTime();
		QString current_time = current_date_time.toString("hh:mm:ss.zzz ");
		qDebug() << current_time;
		remove(path);
		rename(path_tmp, path);
		current_date_time = QDateTime::currentDateTime();
		current_time = current_date_time.toString("hh:mm:ss.zzz ");
		qDebug() << current_time;
		return 0;
	}
	bool JobList::refreshList(int newMaxId){
		

		//TODO
		//mJobListFile
		
		std::cout << "begin refresh ------------"  << std::endl;
		//std::cout << "wiatPath:" << mJobWaitPath << std::endl;

		FILE* readfile = NULL;
		fopen_s(&readfile, mJobListFile.c_str(), "r+");
		char  loadImgPath[1000];
		//get first line
		bool firstLine = true;
		std::string jobs = "";
		jobs += int2str(newMaxId) + "\n";
		while (EOF != fscanf(readfile, "%s", loadImgPath))
		{
			std::string firstline = loadImgPath;
			StringTrim(&firstline);
			if (firstline.find("Job_") != firstline.npos) {      //is a job name
				jobs += firstline + "\n";
			}
		}
		jobs += "Job_" + int2str(newMaxId) + ".xml\n";
		std::cout << "jobs :" << jobs << std::endl;
		fclose(readfile);
		FILE* writefile = NULL;
		fopen_s(&writefile, mJobListFile.c_str(), "w+");
		fprintf(writefile, "%s\n", jobs.c_str());
		fclose(writefile);

		std::cout << "end refresh ------------" << std::endl;
		return true;
		
	}

	bool JobList::insertJob(std::string jobPath, std::string jobfile, int newMaxId){
		std::string from = mJobPath + "/" + jobfile;
		std::string to = mJobWaitPath + "/" + jobfile;
		if (moveFile(from, to)) {
			return refreshList(newMaxId);
		}
		else {
			return false;
		}
		
	}

	bool JobList::handJob(std::string jobPath, std::string jobfile){
		std::cout << "handle Job: " << jobfile << "......" << std::endl;
		std::string from = mJobWaitPath + "/" + jobfile;
		std::string to = mJobHandlerPath + "/" + jobfile;
		return moveFile(from, to);
	}

	bool JobList::finishJob(std::string jobPath, std::string jobfile){
		std::cout << "finish Job: " << jobfile << "......" << std::endl;
		std::string from = mJobHandlerPath + "/" + jobfile;
		std::string to = mJobFinishPath + "/" + jobfile;
		return moveFile(from, to);
	}

	bool JobList::moveFile(std::string fromPath, std::string toPath){

		QDir dir;
		int temp = dir.rename(QString::fromStdString(fromPath), QString::fromStdString(toPath));
		return temp;
	}

	bool JobList::checkFinishAll(std::string list){
		const std::vector<std::string> job_elems = StringSplit(list, ",");
		bool exist = false;
		//check each job is in finish list
		for(int i = 0; i < job_elems.size(); i++){
			std::string jobElem = mJobFinishPath + "/" + job_elems[i] + ".xml";
			std::cout << "job elem: " << jobElem << std::endl;
			exist = ExistsFile(jobElem);
			if(!exist){
				std::cout << "not exist: " << jobElem << std::endl;
				break;
			}
		}
		return exist;
	}
}