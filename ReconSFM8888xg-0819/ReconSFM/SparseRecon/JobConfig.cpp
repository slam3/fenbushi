#include "JobConfig.h"
#include <QCoreApplication>
//#include <Qxml>
#include <QFile>
#include <QDir>
#include <QXmlStreamWriter>
#include <QXmlStreamReader>
#include<chrono>
#include <atomic>
#include <thread>

namespace sfmrecon {

	JobConfig::JobConfig()
	{
		//read and get value
	}


	JobConfig::~JobConfig()
	{
	}

	std::string JobConfig::getJobId() {
		return mJobId;
	}

	int JobConfig::getJobType() {
		return mJobType;
	}
	/*
	int JobConfig::getProgress() {
		return mProgress;
	}
	*/
	int JobConfig::getIncProgress() {
		return mIncProgress;
	}

	int JobConfig::getNextProgress() {
		return mNextProgress;
	}

	std::string JobConfig::getMessage() {
		return mMessage;
	}


	std::string JobConfig::getValue(std::string key) {
		if (mAdditionParam.find(key) == mAdditionParam.end()) {
			std::cout << "ERROR: no such key !" << std::endl;
			return "";
		}
		else {
			return mAdditionParam.at(key);
		}
	}

	//write job to job xml
	bool JobConfig::writeConfig(std::string writePath, int incprogress, int nextprogress, std::string msg) {
		//TODO
		//Options temp;
		std::cout << "gen job path: " << writePath << std::endl;
		QFile file(writePath.c_str());
		if (!file.open(QFile::ReadWrite | QIODevice::Truncate))
		{
			std::cout << "Error: cannot open file" << std::endl;
			return false;
		}

		QXmlStreamWriter stream(&file);
		stream.setAutoFormatting(true);
		stream.writeStartDocument();

		stream.writeStartElement("Job");
		stream.writeTextElement("JobId", QString::fromStdString(mJobId));
		stream.writeTextElement("JobType", QString::fromStdString(int2str(mJobType)));
		stream.writeTextElement("incprogress", QString::fromStdString(int2str(incprogress)));
		stream.writeTextElement("nextprogress", QString::fromStdString(int2str(nextprogress)));
		stream.writeTextElement("Msg", QString::fromStdString(msg));
		if (mAdditionParam.size() > 0) {
			for (std::unordered_map<std::string, std::string>::iterator iter = mAdditionParam.begin(); iter != mAdditionParam.end(); iter++)
			{
				//std::cout << "key value is" << iter->first;
				std::string key = iter->first;
				std::string value = iter->second;
				std::cout << "key:" << key << std::endl;
				std::cout << " value" << value << std::endl;
				stream.writeTextElement(QString::fromStdString(key), QString::fromStdString(value));
			}
		}

		stream.writeEndElement();

		stream.writeEndDocument();
		file.close();
		return true;
	}

	//read xml and get job
	bool JobConfig::readConfig(std::string fileName) {
		//TODO
		std::cout << "read jobconfig: " << fileName << " ...... " << std::endl;
		QFile file(fileName.c_str());
	    if (!file.open(QFile::ReadOnly | QFile::Text)) 
	    {
	        std::cerr << "Error: Cannot read file " << fileName << std::endl;
	        return false;
	    }
	    //如果文件打开成功，将它设置为QXmlStreamReader的输入设备
	    QXmlStreamReader reader(&file);

	    mAdditionParam.clear();
	    //当读取到XML文档结尾，或者发生错误，atEnd()函数返回true
	    while (!reader.atEnd()) 
	    {
			reader.readNext();//读取下一个开始元素
	        //如果当前记号为StartElement，返回true
	        if (reader.isStartElement())
	        {
				if (reader.name() == "Job") {
					continue;
				}
	            else if (reader.name() == "JobId") 
	            {
	                mJobId = reader.readElementText().toStdString();
					std::cout << "mJobId :" << mJobId << std::endl;
				}
	            else if (reader.name() == "JobType")
	            {
	                mJobType = str2int(reader.readElementText().toStdString());
					std::cout << "mJobType :" << mJobType << std::endl;
				}
				/*
				else if (reader.name() == "progress")
				{
					mProgress = str2int(reader.readElementText().toStdString());
					std::cout << "mJobType :" << mJobType << std::endl;
				}
				*/
				else if (reader.name() == "incprogress")
				{
					mIncProgress = str2int(reader.readElementText().toStdString());
					std::cout << "mIncProgress :" << mIncProgress << std::endl;
				}
				else if (reader.name() == "nextprogress")
				{
					mNextProgress = str2int(reader.readElementText().toStdString());
					std::cout << "mNextProgress :" << mNextProgress << std::endl;
				}
				else if (reader.name() == "Msg")
				{
					mMessage = reader.readElementText().toStdString();
					std::cout << "mJobType :" << mJobType << std::endl;
				}
				else {
	            	std::string key = reader.name().toString().toStdString();
	            	std::string value = reader.readElementText().toStdString();
					std::cout << "key :" << key << ", value :" << value << std::endl;
	            	mAdditionParam.insert(std::make_pair(key, value));
	            }
			}
			else {
				continue;
			}
	       
	    }

	    file.close();
	    if (reader.hasError())
	    {
	        std::cerr << "Error: Failed to parse file "<< fileName << ": "<< reader.errorString().toStdString() << std::endl;
	        return false;
	    }
	    else if (file.error() != QFile::NoError) 
	    {
	        std::cerr << "Error: Cannot read file " << fileName << ": " << file.errorString().toStdString() << std::endl;
	        return false;
	    }

		return true;
	}

	//gen job by id
	void JobConfig::genJobId(int jobType, std::string writepath, int incprogress, int nextprogress, std::string msg) {
		std::string jobId;
		std::unordered_map<std::string, std::string> additionParam;
		switch (jobType) {
		case JOB_FIRST:			
			initJob(JOB_FIRST, jobId, additionParam, writepath, incprogress, nextprogress, msg);
			break;
		case JOB_FEATURE_EXTRACTION:
			initJob(JOB_FEATURE_EXTRACTION, jobId, additionParam, writepath, incprogress, nextprogress, msg);
			break;
		case JOB_FEATURE_MATCHING:
			initJob(JOB_FEATURE_MATCHING, jobId, additionParam, writepath, incprogress, nextprogress, msg);
			break;
		case JOB_CLUSTER:
			initJob(JOB_CLUSTER, jobId, additionParam, writepath, incprogress, nextprogress, msg);
			break;
		case JOB_MERGE:
			initJob(JOB_MERGE, jobId, additionParam, writepath, incprogress, nextprogress, msg);
			break;
		case JOB_DENSE:
			initJob(JOB_DENSE, jobId, additionParam, writepath, incprogress, nextprogress, msg);
			break;
		case JOB_FINISH:
			initJob(JOB_FINISH, jobId, additionParam, writepath, incprogress, nextprogress, msg);
			break;
		case JOB_RESPARSE_RECONSTRUTION:
			initJob(JOB_RESPARSE_RECONSTRUTION, jobId, additionParam, writepath, incprogress, nextprogress, msg);
			break;
		default:
			std::cout << "ERROR: unsupport job!" << std::endl;
		}
	}

	//gen a job by param, and write to a xml
	void JobConfig::initJob(int jobType, std::string &jobId, std::unordered_map<std::string, std::string> additionParam, std::string writepath, int incprogress, int nextprogress, std::string msg) {
		std::cout << "create job ...." << std::endl;
		JobList jobList;
		jobList.initial(writepath);
		int maxJobId;
		FILE* lock = NULL;
		std::string slash = "/";
		std::string lockpath = "job.lock";
		std::string lockFile = JoinPaths(writepath, slash + lockpath);
		std::cout << "lock file:--------" << lockFile << std::endl;
		while (fopen_s(&lock, lockFile.c_str(), "r+") != 0) {			//wait to get job lock
			srand((int)time(0));
			int time = rand() % 10 + 1;
			int sleepSecond = time * 1000;
			std::cout << "sleep 0-1s:" << sleepSecond << std::endl;
			std::this_thread::sleep_for(std::chrono::milliseconds(sleepSecond));
			continue;
		}
		
		jobList.getMaxJobId(maxJobId);
		std::cout << " get job id:--------" << maxJobId << std::endl;
		int newJobId = maxJobId + 1;
		jobId = "Job_" + int2str(newJobId);
		mJobId = jobId;
		mJobType = jobType;
		mAdditionParam.clear();
		
		
		if (additionParam.size() > 0) {
			for (std::unordered_map<std::string, std::string>::iterator iter = additionParam.begin(); iter != additionParam.end(); iter++)
			{
				//std::cout << "key value is" << iter->first;
				std::string key = iter->first;
				std::string value = iter->second;
				std::cout << "key:" << key << std::endl;
				std::cout << " value" << value << std::endl;
				mAdditionParam.insert(std::make_pair(key, value));
			}
		}
		std::string jobFileName = jobId + ".xml";
		std::string jobpath = JoinPaths(writepath, slash + jobFileName);
		writeConfig(jobpath, incprogress, nextprogress, msg);
		jobList.insertJob(writepath, jobFileName, newJobId);
		fclose(lock);
	}
}
