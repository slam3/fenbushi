#ifndef  FILE_MONOTOR_H_
#define FILE_MONOTOR_H_
#include "util/threading.h"

namespace sfmrecon {
  class FileMonotor : public Thread {
  public:
	  
    FileMonotor(const std::string& file_path,
      std::shared_ptr<Thread> task = nullptr);

	FileMonotor(
		Thread* task,
		ThreadPool *threadPool);
		
	FileMonotor();
	void setMonitor(Thread * task, ThreadPool* pool);
    void Run();
	void setMonitorFile(std::string file);

    static bool Sleep(int t);
  private:
    std::string file_path_;
    std::shared_ptr<Thread> task_;
	Thread* _mainThread;
	ThreadPool* _mainPool;
  };
}
#endif // ! FILE_MONOTOR_H_
