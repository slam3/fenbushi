#include <string>
#include "base/reconstruction_manager.h"
#include "util/option_manager.h"
#include "util/threading.h"
#include "controllers/hierarchical_mapper.h"
#include <QFile>
#include <QXmlStreamWriter>
namespace sfmrecon {
	class SparseReconstructionController : public Thread {
	public:
		enum class DataType { INDIVIDUAL, VIDEO, INTERNET };
		enum class Quality { LOW, MEDIUM, HIGH, EXTREME };

		struct Options {
			std::string workspace_path;
			std::string image_path;
			std::string vocab_tree_path;
			std::string gps_file_path;
			std::string gcp_file_path;
			std::string at_file_path;
			std::string focal_file_path;
			DataType data_type = DataType::INDIVIDUAL;
			Quality quality = Quality::HIGH;
			bool single_camera =false;
			bool single_camera_per_folder= true;
			std::string camera_model = "SIMPLE_RADIAL";//FULL_OPENCV
			bool sparse = true;
#ifdef CUDA_ENABLED
			bool dense = true;
#else
			bool dense = false;
#endif

			// The number of threads to use in all stages.
			int num_threads = -1;
			// Whether to use the GPU in feature extraction and matching.
			bool use_gpu = true;
			// Index of the GPU used for GPU stages. For multi-GPU computation,
			// you should separate multiple GPU indices by comma, e.g., "0,1,2,3".
			// By default, all GPUs will be used in all stages.
			std::string gpu_index = "-1";
			int max_feature_point =4096;
		};

		SparseReconstructionController(
			const Options& options, ReconstructionManager* reconstruction_manager);

		void Stop() override;

	private:
		void Run() override;
		void RunFeatureExtraction();
		void RunFeatureMatching();
		void RunSparseMapper();
		void RunDenseMapper();
		void RunReSparseMapper();
		bool RunGeoPos(Reconstruction& reconstruction);
		bool RunReGen(Reconstruction& reconstruction);
		bool RunInvPos(Reconstruction &reconstruction);
        HierarchicalMapperController::Options hierarchical_options;
		Options options_;
		OptionManager option_manager_;
		ReconstructionManager* reconstruction_manager_;
		Thread* active_thread_;
		std::unique_ptr<Thread> feature_extractor_;
		std::unique_ptr<Thread> exhaustive_matcher_;
		std::unique_ptr<Thread> sequential_matcher_;
		std::unique_ptr<Thread> vocab_tree_matcher_;
		std::unique_ptr<Thread> spatial_matcher_;

		void writeXml(Options temp);
		void readXml(QString path, Options temp);
		//void CopyDir(QString src, QString dst);//demo:    copyPath("D://Qt//4.8.6", "//192.168.31.148//zhiyong");
		bool Copyfile(QString filepath, QString despath);//demo:    temp->CopyFile("D:/yinlu/libgdal.a", "//192.168.31.148//zhiyong");
		char* getTask(char* filename);//demo:    char* ss = temp->getTask("//192.168.31.148/zhiyong/test2/test.txt");
		std::string getHostName();//get hostname
		std::string getHostIp();
		
	};

} //


