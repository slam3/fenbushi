#pragma once
#include "base/reconstruction_manager.h"
#include "util/option_manager.h"
#include "util/threading.h"
#include "JobConfig.h"
#include "controllers/incremental_mapper.h"
#include "controllers/bundle_adjustment.h"
#include "controllers/partial_mapper.h"
#include "controllers/cluster_mapper.h"
#include "controllers/merge_mapper.h"
#include "controllers/pointadjust_mapper.h"
#include "EngineXmlConfig.h"
#include "FileMonotor.h"
#include "basemacro.h"
#include "base/at_gcp_report.h"

namespace sfmrecon {
	/*
	const int PROJECT_STATE_BEGIN = 0;
	const int PROJECT_STATE_FEATURE_EXTRACT = 1;
	const int PROJECT_STATE_FEATURE_MATCH = 2;
	const int PROJECT_STATE_FEATURE_CLUSTER = 3;
	const int PROJECT_STATE_FEATURE_MERGE = 4;
	const int PROJECT_STATE_FEATURE_END = 5;

	const int JOB_SINGLE = 0;
	const int JOB_MULTI = 1;
	const int JOB_UNDEFIND = 2;

	const int JOB_MAIN = 0;
	const int JOB_SUB = 1;

	const int JOB_BEGIN_STATUS = 1;
	const int JOB_END_STATUS = 2;
	*/
	class SlaveProgress :
		public Thread
	{
	public:
		enum class DataType { INDIVIDUAL, VIDEO, INTERNET };
		enum class Quality { LOW, MEDIUM, HIGH, EXTREME };
		struct inputOptions {
			std::string project_path;
			std::string setting_jobqueue_path;
			std::string output_path;
		};

		struct Options {
			std::string workspace_path;
			std::string image_path;
			std::string vocab_tree_path;
            std::string gps_file_path;
            std::string gcp_file_path;
			std::string at_report_file_path;
			std::string at_file_path;
			std::string focal_file_path;
			std::string db_path;
			std::string job_path;
			std::string project_path;
			std::string jobQueuePath;
			std::string output_path;
			std::string feedback_path;
			std::string enginconfPath;
			std::string projectName;
			int numCurProcess;

			DataType data_type = DataType::INDIVIDUAL;
			Quality quality = Quality::HIGH;
			bool single_camera = false;
			bool single_camera_per_folder = true;
			std::string camera_model = "SIMPLE_RADIAL";//FULL_OPENCV
			bool sparse = true;
#ifdef CUDA_ENABLED
			bool dense = true;
#else
			bool dense = false;
#endif
			// The number of threads to use in all stages.
			int num_threads = -1;
			// Whether to use the GPU in feature extraction and matching.
			bool use_gpu = true;
			// Index of the GPU used for GPU stages. For multi-GPU computation,
			// you should separate multiple GPU indices by comma, e.g., "0,1,2,3".
			// By default, all GPUs will be used in all stages.
			std::string gpu_index = "-1";
			int max_feature_point = 4096;
			//int max_feature_point = 2048;
		};

		SlaveProgress(ReconstructionManager* reconstruction_manager, inputOptions options);
		//SlaveProgress(std::string jobQueuePath, std::string outputPath);
		~SlaveProgress();

		bool setting(std::string jobQueuePath, std::string outputPath);
		bool joinDistribute(std::string outputPath);
		bool initialEngine();
		bool setEngineConfig();							//set distribute system config from xml to memory
		bool genEngineConfig(std::string output_path, std::string jobQueuePath);                         //read jobqueue path and find project xml and genarate engine xml
		bool refreshEngine();
		//bool refreshEngine(std::string output_path, std::string jobQueuePath);                              //update engine xml and memorys

		void Stop() override;
		bool rollBack();                 //roll back engine to a right status
		bool forceRollBack();
		bool wirtexml(Options temp);
		void printTime(std::string intro);
		bool clearEngineFile();
        bool removeEngine();

    private:
		void Run() override;
		
		bool getJobConfig(std::string jobFile, JobConfig &jobconfig);
		int isSingleJob(JobConfig jobconfig);
		bool handleSingleJob(std::string jobFile, JobConfig jobconfig);
		bool RunFirstJob(JobConfig jobconfig);
		//bool RunImageDivideJob(JobConfig jobconfig);
		bool RunFeatureExtractionJob(JobConfig jobconfig);			//feature extraction job
		bool RunFeatureMatchingJob(JobConfig jobconfig);				//feature match job
		bool RunImageClusterJob(JobConfig jobconfig);					//image cluster job
		bool RunIncrementMapperJob(JobConfig jobconfig);			//increment mapper job
		bool RunClusterMergeJob(JobConfig jobconfig);					//cluster merge job
		bool RunDenseCloudJob(JobConfig jobconfig);						//dense cloud job
		bool RunFinshJob(JobConfig jobconfig);
		bool RunReSparseJob(JobConfig jobconfig);

		int getEngineStatus();							//get enginStatus
		std::string getJob();										//get an undistribute job
		bool genJobs();										//create jobs queue
		bool sendResult();									//send partial result to project
		bool waitForSynchronize(JobConfig jobconfig);					//wait for all part of a job finish
		bool mergeResult();								//merge result together
		bool setJobProgress(JobConfig jobconfig, std::string engine, int mode);
		bool refreshStatus();
        void export_at_gcp_report(Reconstruction& reconstruction, bool before_at = true);
        void export_at_gcp_report_settings(ATGCPReport &at_gcp_report);
        void export_at_gcp_report_global_connections(ATGCPReport &at_gcp_report, Reconstruction& reconstruction, bool before_at = true);
        void export_at_gcp_report_automatic_tie_points(ATGCPReport &at_gcp_report, Reconstruction& reconstruction, bool before_at = true);
    bool RunActiveThread(std::shared_ptr<Thread> active_thread);

	std::string GetATReportPath();
	std::string GetATGCPReportPath();
    std::string GetFinalATReportPath();

		Options options_;
		inputOptions mInputOptions;

		OptionManager option_manager_;
		ReconstructionManager* reconstruction_manager_;
		Thread* active_thread_;
		//FileMonotor file_monotor;
		//std::shared_ptr<Thread> active_thread_;

		std::string mParamfile;
		std::string mWorkSpace;
		std::string mJobPath;
		int mProjectState;
		int mNum_eff_workers;
		JobList mJobList;
	};

}

