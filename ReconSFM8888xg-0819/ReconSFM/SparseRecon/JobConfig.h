#pragma once
#include <string>
#include <unordered_map>
#include <iostream>
#include "JobList.h"

namespace sfmrecon {
	const int JOB_FIRST = 0;
	const int JOB_FEATURE_EXTRACTION = 1;
	const int JOB_FEATURE_MATCHING = 2;
	const int JOB_CLUSTER = 3;
	const int JOB_INCREMENTAL = 4;
	const int JOB_CLUSTER_SYNCHRONIZE = 5;
	const int JOB_MERGE = 6;
	const int JOB_DENSE = 7;
	const int JOB_FINISH = 8;
	const int JOB_RESPARSE_RECONSTRUTION = 9;

	class JobConfig
	{
	public:
		JobConfig();
		~JobConfig();

		std::string getJobId();
		int getJobType();
		std::string getValue(std::string key);
		//int getProgress();
		int getIncProgress();
		int getNextProgress();
		std::string getMessage();
		bool writeConfig(std::string writePath, int incprogress, int nextprogress, std::string msg);
		bool readConfig(std::string configfile);
		void genJobId(int jobType, std::string writepath, int incprogress, int nextprogress, std::string msg);
		void initJob(int jobType, std::string &jobId, std::unordered_map<std::string, std::string> additionParam, std::string writepath, int incprogress, int nextprogress, std::string msg);
	
	private:
		std::string mJobId;
		int mJobType;
		std::string mMessage;
		//int mProgress;
		int mIncProgress;
		int mNextProgress;
		std::unordered_map<std::string, std::string> mAdditionParam;
	};


}

