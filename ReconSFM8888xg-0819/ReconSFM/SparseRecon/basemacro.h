#pragma once
namespace sfmrecon {
	const int PROJECT_STATE_BEGIN = 0;
	const int PROJECT_STATE_FEATURE_EXTRACT = 1;
	const int PROJECT_STATE_FEATURE_MATCH = 2;
	const int PROJECT_STATE_FEATURE_CLUSTER = 3;
	const int PROJECT_STATE_FEATURE_MERGE = 4;
	const int PROJECT_STATE_FEATURE_END = 5;

	const int JOB_SINGLE = 0;
	const int JOB_MULTI = 1;
	const int JOB_UNDEFIND = 2;

	const int JOB_MAIN = 0;
	const int JOB_SUB = 1;

	const int JOB_BEGIN_STATUS = 1;
	const int JOB_END_STATUS = 2;

	const int PROGRESS_STATE_BEGIN = 0;
	const int PROGRESS_STATE_FEATURE_EXTRACT = 0;
	const int PROGRESS_STATE_FEATURE_MATCH = 10;
	const int PROGRESS_STATE_CLUSTER = 40;
	const int PROGRESS_STATE_INCREMENT = 50;
	const int PROGRESS_STATE_SYNC = 80;
	const int PROGRESS_STATE_MERGE = 85;
	const int PROGRESS_STATE_DENSE = 90;
	const int PROGRESS_STATE_FINISH = 100;
}