#include "MasterProgress.h"
#include "util/misc.h"
#include "JobConfig.h"
#include <QDebug>
#include <QMessageBox>
#include <QFile>
#include <QDir>
#include <QFileInfoList>
#include <QMessageBox>
#include <QSharedMemory>
#include <QSystemSemaphore>
#include <QDateTime>
#include <QDebug>
#include <QXmlStreamWriter>
#include "util/fileFun.h"
#include "basemacro.h"

namespace sfmrecon {

	MasterProgress::MasterProgress()
		: active_thread_(nullptr) {
	}


	MasterProgress::~MasterProgress()
	{
	}

	void MasterProgress::Stop() {
		if (active_thread_ != nullptr) {
			active_thread_->Stop();
		}
		Thread::Stop();
	}

	//create jobs queue
	bool MasterProgress::genJobs() {
	}

	bool MasterProgress::writeConfig() {

		std::string path = mOptions.confPath + "/masteroption.xml";
		QFile file(path.c_str());
		if (!file.open(QFile::ReadWrite | QIODevice::Truncate))
		{
			//qDebug() << "Error: cannot open file";
			return false;
		}


		QXmlStreamWriter stream(&file);
		stream.setAutoFormatting(true);
		stream.writeStartDocument();
		stream.writeStartElement("Information");
		stream.writeTextElement("workspacePath", QString::fromStdString(mOptions.workspacePath));
		stream.writeTextElement("db_path", QString::fromStdString(mOptions.db_path));
		stream.writeTextElement("imagePath", QString::fromStdString(mOptions.imagePath));
		stream.writeTextElement("gpsPath", QString::fromStdString(mOptions.gpsPath));
		stream.writeTextElement("vocab_tree_path", QString::fromStdString(mOptions.vocabTreePath));
		stream.writeTextElement("jobPath", QString::fromStdString(mOptions.jobPath));
        stream.writeTextElement("feedbackPath", QString::fromStdString(mOptions.feedBackPath));
        stream.writeTextElement("gcpPath", QString::fromStdString(mOptions.gcpPath));
        stream.writeTextElement("atReportPath", QString::fromStdString(mOptions.atReportPath));
		stream.writeTextElement("atPath", QString::fromStdString(mOptions.atPath));
		stream.writeTextElement("focalPath", QString::fromStdString(mOptions.focalPath));

		stream.writeTextElement("projectName", QString::fromStdString(mOptions.projectName));
		stream.writeTextElement("projectPath", QString::fromStdString(mOptions.projectPath));
		stream.writeTextElement("jobQueuePath", QString::fromStdString(mOptions.jobQueuePath));

		if (mOptions.data_type == DataType::INDIVIDUAL)
		{
			stream.writeTextElement("data_type", "0");
		}
		else if (mOptions.data_type == DataType::VIDEO)
		{
			stream.writeTextElement("data_type", "1");
		}
		else
		{
			stream.writeTextElement("data_type", "2");

		}
		if (mOptions.quality == Quality::LOW)
		{
			stream.writeTextElement("quality", "0");
		}
		else if (mOptions.quality == Quality::MEDIUM)
		{
			stream.writeTextElement("quality", "1");
		}
		else if (mOptions.quality == Quality::HIGH)
		{
			stream.writeTextElement("quality", "2");

		}
		else
		{
			stream.writeTextElement("quality", "3");
		}
		if (mOptions.single_camera)
		{
			stream.writeTextElement("single_camera", "true");
		}
		else {
			stream.writeTextElement("single_camera", "false");
		}
		if (mOptions.single_camera_per_folder)
		{
			stream.writeTextElement("single_camera_per_folder", "true");
		}
		else {
			stream.writeTextElement("single_camera_per_folder", "false");
		}
		stream.writeTextElement("camera_model", QString::fromStdString(mOptions.camera_model));
		if (mOptions.sparse)
		{
			stream.writeTextElement("sparse", "true");
		}
		else {
			stream.writeTextElement("sparse", "false");
		}
		if (mOptions.dense)
		{
			stream.writeTextElement("dense", "true");
		}
		else {
			stream.writeTextElement("dense", "false");
		}
		stream.writeTextElement("num_threads", QString("%1").arg(mOptions.num_threads));
		if (mOptions.use_gpu)
		{
			stream.writeTextElement("use_gpu", "true");
		}
		else {
			stream.writeTextElement("use_gpu", "false");
		}
		stream.writeTextElement("gpu_index", QString::fromStdString(mOptions.gpu_index));
		stream.writeTextElement("max_feature_point", QString("%1").arg(mOptions.max_feature_point));
		stream.writeTextElement("numCurProcess", QString("%1").arg(mOptions.numCurProcess));


		stream.writeEndElement();
		stream.writeEndDocument();
		file.close();
		return true;
	}

	void MasterProgress::loadOption(std::string confFile) {
		//TODO
		confFile = confFile + "/masteroption.xml";
		QFile file(confFile.c_str());
		if (file.open(QIODevice::ReadOnly | QIODevice::Text))
		{

			//构建QXmlStreamReader对象
			QXmlStreamReader reader(&file);

			while (!reader.atEnd())
			{
				//reader.readNext();//读取下一个开始元素
				reader.readNextStartElement();//读取下一个开始元素
											  //判断是否是节点的开始
				if (reader.isStartElement())
				{
					QString strElementName = reader.name().toString();
					if (QString::compare(strElementName, "Information") == 0)
					{
						//qDebug() << QString::fromLocal8Bit("**********<Information>********** ");
					}
					else if (QString::compare(strElementName, QStringLiteral("projectPath")) == 0)
					{
						mOptions.projectPath = reader.readElementText().toStdString();
					}
					else if (QString::compare(strElementName, QStringLiteral("jobQueuePath")) == 0)
					{
						mOptions.jobQueuePath = reader.readElementText().toStdString();
					}
					else if (QString::compare(strElementName, QStringLiteral("projectName")) == 0)
					{
						mOptions.projectName = reader.readElementText().toStdString();
					}
					else if (QString::compare(strElementName, QStringLiteral("vocabTreePath")) == 0)
					{
						mOptions.vocabTreePath = reader.readElementText().toStdString();
					}

				}

			}
			file.close();
		}



	}

	void MasterProgress::setOption(const Options option) {
		mOptions.projectName = option.projectName;
		mOptions.projectPath = option.projectPath;
		mOptions.vocabTreePath = option.vocabTreePath;
		mOptions.jobQueuePath = option.jobQueuePath;
		mOptions.imagePath = option.imagePath;
        mOptions.gpsPath = option.gpsPath;
        mOptions.gcpPath = option.gcpPath;
        mOptions.atReportPath = option.atReportPath;
		mOptions.atPath = option.atPath;
		mOptions.focalPath = option.focalPath;
		mOptions.data_type = option.data_type;
		mOptions.quality = option.quality;
		mOptions.single_camera = option.single_camera;
		mOptions.single_camera_per_folder = option.single_camera_per_folder;
		mOptions.camera_model = option.camera_model;
		mOptions.sparse = option.sparse;
		mOptions.dense = option.dense;
		mOptions.num_threads = option.num_threads;
		mOptions.use_gpu = option.use_gpu;
		mOptions.gpu_index = option.gpu_index;
		mOptions.max_feature_point = option.max_feature_point;
		mOptions.numCurProcess = option.numCurProcess;
	}

	bool MasterProgress::writeJobQueueConfig(std::string path, std::string projectPath) {
		path = path + "/project.xml";
		QFile file(path.c_str());
		if (!file.open(QFile::ReadWrite | QIODevice::Truncate))
		{
			//qDebug() << "Error: cannot open file";
			return false;
		}

		//old model
		/* 
		QXmlStreamWriter stream(&file);
		stream.setAutoFormatting(true);
		stream.writeStartDocument();
		stream.writeStartElement("project");
		stream.writeTextElement("currentProject", QString::fromStdString(projectPath));

		stream.writeEndElement();
		stream.writeEndDocument();
		file.close();
		*/

		QXmlStreamWriter stream(&file);
		stream.setAutoFormatting(true);
		stream.writeStartDocument();

		stream.writeStartElement("Job");
		stream.writeTextElement("Project", QString::fromStdString(projectPath));

		stream.writeStartElement("JobFeedBack");
		stream.writeTextElement("Progress", QString::fromStdString("0"));
		stream.writeTextElement("Msg", QString::fromStdString("Initialing..."));
		stream.writeEndElement();

		stream.writeEndElement();

		stream.writeEndDocument();
		file.close();


		return true;
	}

	//create a project
	bool MasterProgress::initialProject(const Options Options) {
		//std::cout << "call initial:" << std::endl;
		setOption(Options);
	/*	std::string testProject = "测试project";
		std::string testjobQueue = "测试jobQueue";
		std::string testusrWorkspace = "测试usrworkspace";
		std::cout << "projectPath:" << mOptions.projectPath  << std::endl;

		QDir projectDir;
		projectDir.mkdir(QString::fromStdString("F:/codenew/blocked-processing/ReconSFM8888xg-0819/ReconSFM/output/master/" + testProject));
		QDir jobQueueDir;
		jobQueueDir.mkdir(QString::fromStdString("F:/codenew/blocked-processing/ReconSFM8888xg-0819/ReconSFM/output/master/" + testjobQueue));
		QDir usrworkspaceDir;
		usrworkspaceDir.mkdir(QString::fromStdString("F:/codenew/blocked-processing/ReconSFM8888xg-0819/ReconSFM/output/master/" + testusrWorkspace));
*/

		std::string slash = "/";
		CreateDirIfNotExists(mOptions.projectPath);

		mOptions.confPath = JoinPaths(mOptions.projectPath, slash + "conf");
		std::cout << "confPath:" << mOptions.confPath << std::endl;
		CreateDirIfNotExists(mOptions.confPath);

		mOptions.feedBackPath = JoinPaths(mOptions.projectPath, slash + "feedback");
		std::cout << "feedBackPath:" << mOptions.feedBackPath << std::endl;
		CreateDirIfNotExists(mOptions.feedBackPath);

		mOptions.workspacePath = JoinPaths(mOptions.projectPath, slash + "workspace");
		std::cout << "workspacePath:" << mOptions.workspacePath << std::endl;
		CreateDirIfNotExists(mOptions.workspacePath);

		mOptions.db_path = JoinPaths(mOptions.workspacePath, slash + mOptions.projectName + ".db");
		std::cout << "db_path:" << mOptions.db_path << std::endl;

		//create a db lock
		std::string DBLockName = JoinPaths(mOptions.workspacePath, "/db.lock");
		std::cout << "dbLockName:" << DBLockName << std::endl;
		std::ofstream lockfile(DBLockName, std::ios::trunc);
		CHECK(lockfile.is_open()) << DBLockName;
		lockfile << "This is DB Lock" << std::endl;
		lockfile.close();

		/*
		std::string imagePath = JoinPaths(mOptions.projectPath, slash + "image");
		std::cout << "imagePath:" << imagePath << std::endl;
		CreateDirIfNotExists(imagePath);

		std::string gpsPath = JoinPaths(mOptions.projectPath, slash + "gps");
		std::cout << "gpsPath:" << gpsPath << std::endl;
		CreateDirIfNotExists(gpsPath);
		*/

		mOptions.jobPath = JoinPaths(mOptions.projectPath, slash + "jobs");
		std::cout << "jobPath:" << mOptions.jobPath << std::endl;
		CreateDirIfNotExists(mOptions.jobPath);

		std::string waitJobPath = JoinPaths(mOptions.jobPath, slash + "wait");
		std::cout << "waitJobPath:" << waitJobPath << std::endl;
		CreateDirIfNotExists(waitJobPath);

		std::string handleJobPath = JoinPaths(mOptions.jobPath, slash + "handle");
		std::cout << "handleJobPath:" << handleJobPath << std::endl;
		CreateDirIfNotExists(handleJobPath);

		std::string finishJobPath = JoinPaths(mOptions.jobPath, slash + "finish");
		std::cout << "finishJobPath:" << finishJobPath << std::endl;
		CreateDirIfNotExists(finishJobPath);

		//CreateDirIfNotExists(mOptions.enginPath);

		//TODO
		writeConfig();
	
		createInitialJob(mOptions.jobPath);

		CreateDirIfNotExists(mOptions.jobQueuePath);
		//create jobQueue
		/*		
		std::string toPath = JoinPaths(mOptions.jobQueuePath, slash + "conf");
		CreateDirIfNotExists(toPath);
		*/
		std::string ArchivePath = JoinPaths(mOptions.jobQueuePath, slash + "Archive");
		CreateDirIfNotExists(ArchivePath);
		std::string CancelledPath = JoinPaths(mOptions.jobQueuePath, slash + "Cancelled");
		CreateDirIfNotExists(CancelledPath);
		std::string CompletedPath = JoinPaths(mOptions.jobQueuePath, slash + "Completed");
		CreateDirIfNotExists(CompletedPath);
		std::string EnginesPath = JoinPaths(mOptions.jobQueuePath, slash + "Engines");
		CreateDirIfNotExists(EnginesPath);
		std::string FailedPath = JoinPaths(mOptions.jobQueuePath, slash + "Failed");
		CreateDirIfNotExists(FailedPath);
		std::string PendingPath = JoinPaths(mOptions.jobQueuePath, slash + "Pending");
		CreateDirIfNotExists(PendingPath);
		std::string PendingHigh = JoinPaths(PendingPath, slash + "High");
		CreateDirIfNotExists(PendingHigh);
		std::string PendingLow = JoinPaths(PendingPath, slash + "Low");
		CreateDirIfNotExists(PendingLow);
		std::string PendingNormal = JoinPaths(PendingPath, slash + "Normal");
		CreateDirIfNotExists(PendingNormal);
		std::string RunningPath = JoinPaths(mOptions.jobQueuePath, slash + "Running");
		CreateDirIfNotExists(RunningPath);
		//copy conf to jobQueuePath
		/*
		std::string fromconf = JoinPaths(mOptions.workspacePath, slash + "project.xml");
		std::string toconf = JoinPaths(toPath, slash + "project.xml");
		std::cout << "from conf:" << fromconf << std::endl;
		std::cout << "to conf:" << toconf << std::endl;
		writeJobQueueConfig(mOptions.workspacePath, mOptions.projectPath);
		if (QFile::exists(QString::fromStdString(toconf))) {
			QFile::remove(QString::fromStdString(toconf));
		}
		QFile::copy(QString::fromStdString(fromconf), QString::fromStdString(toconf));
		*/
		std::string toPath;	
		std::string toconf;

		toPath = PendingHigh;
		toconf = JoinPaths(toPath, slash + Options.projectName + "_AT-file.xml");
		std::string fromconf = JoinPaths(mOptions.workspacePath, slash + "project.xml");
		//std::string toconf = JoinPaths(toPath, slash + Options.projectName + "_AT-file.xml");
		std::cout << "from conf:" << fromconf << std::endl;
		std::cout << "to conf:" << toconf << std::endl;
		writeJobQueueConfig(mOptions.workspacePath, mOptions.projectPath);
		if (QFile::exists(QString::fromStdString(toconf))) {
			QFile::remove(QString::fromStdString(toconf));
		}
		QFile::copy(QString::fromStdString(fromconf), QString::fromStdString(toconf));

		std::string statusLockName = JoinPaths(mOptions.feedBackPath, "/status.lock");
		std::cout << "statusLockName:" << statusLockName << std::endl;
		std::ofstream statuslockfile(statusLockName, std::ios::trunc);
		CHECK(statuslockfile.is_open()) << statusLockName;
		statuslockfile << "This is status Lock" << std::endl;
		statuslockfile.close();
		std::string progressResult = "";
		progressResult += "jobId#0\n";
		progressResult += "progress#0\n";
		progressResult += "msg#Ready\n";
		std::string progressPath = JoinPaths(mOptions.feedBackPath, "/progress.conf");
		std::ofstream progressnewfile(progressPath, std::ios::trunc);
		CHECK(progressnewfile.is_open()) << progressPath;
		progressnewfile << progressResult << std::endl;
		progressnewfile.close();
		std::string engineResult = "";
		std::string enginePath = JoinPaths(mOptions.feedBackPath, "/engine.conf");
		std::ofstream enginenewfile(enginePath, std::ios::trunc);
		CHECK(enginenewfile.is_open()) << enginePath;
		enginenewfile << engineResult << std::endl;
		enginenewfile.close();

		return true;
	}

	//get progress status
	bool MasterProgress::getProgressStatus() {
	}

	void MasterProgress::Run() {
	}

	//job for feature extraction
	void MasterProgress::createInitialJob(std::string jobpath) {
		//initial joblist file
		//std::locale loc = std::locale::global(std::locale(""));
		std::string jobListFileName = JoinPaths(jobpath, "/jobList.txt");
		std::cout << "jobListFileName:" << jobListFileName << std::endl;
		std::ofstream jobListfile(jobListFileName, std::ios::trunc);
		CHECK(jobListfile.is_open()) << jobListFileName;
		jobListfile << "0" << std::endl;
		jobListfile.close();
		std::string jobLockName = JoinPaths(jobpath, "/job.lock");
		std::cout << "jobLockName:" << jobLockName << std::endl;
		std::ofstream lockfile(jobLockName, std::ios::trunc);
		CHECK(lockfile.is_open()) << jobLockName;
		lockfile << "This is Job Lock" << std::endl;
		lockfile.close();
		
		//initial first job
		JobConfig jobconfig;
		std::string msg = "Initial job...";
		int nextprogress = PROGRESS_STATE_FEATURE_EXTRACT;
		int incprogress = PROGRESS_STATE_FEATURE_EXTRACT - PROGRESS_STATE_BEGIN;
		jobconfig.genJobId(JOB_FIRST, jobpath, incprogress, nextprogress, msg);
		//std::locale::global(loc);
	}


}
