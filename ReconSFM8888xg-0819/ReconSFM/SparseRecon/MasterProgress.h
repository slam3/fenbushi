/**
Master Progess, center control of the distribute system.
main fuction include:
make a project,
set initial configuration and data,
communicate with slaves,
distribute jobs,
control the 3D reconstruction progress,
interface with view.
author: fangbojun 2019-10-31
*/
#pragma once
#include "base/reconstruction_manager.h"
#include "util/option_manager.h"
#include "util/threading.h"
namespace sfmrecon {
	class MasterProgress :
		public Thread
	{
	public:
		enum class DataType { INDIVIDUAL, VIDEO, INTERNET };
		enum class Quality { LOW, MEDIUM, HIGH, EXTREME };

		struct Options {
			std::string projectName;
			std::string projectPath;
			std::string vocabTreePath;
			std::string jobQueuePath;
			std::string imagePath;
            std::string gpsPath;
            std::string gcpPath;
            std::string atReportPath;
			std::string atPath;
			std::string focalPath;
			std::string confPath;
			std::string workspacePath;
			std::string db_path;
			std::string jobPath;
			std::string feedBackPath;
			int numCurProcess;
			DataType data_type = DataType::INDIVIDUAL;
			Quality quality = Quality::HIGH;
			bool single_camera = false;
			bool single_camera_per_folder = true;
			std::string camera_model = "SIMPLE_RADIAL";//FULL_OPENCV
			bool sparse = true;
#ifdef CUDA_ENABLED
			bool dense = true;
#else
			bool dense = false;
#endif
			// The number of threads to use in all stages.
			int num_threads = -1;
			// Whether to use the GPU in feature extraction and matching.
			bool use_gpu = true;
			// Index of the GPU used for GPU stages. For multi-GPU computation,
			// you should separate multiple GPU indices by comma, e.g., "0,1,2,3".
			// By default, all GPUs will be used in all stages.
			std::string gpu_index = "-1";
			int max_feature_point = 4096;
			//int max_feature_point = 2048;
		};

		MasterProgress();
		~MasterProgress();

		void Stop() override;
		bool genJobs();										//create jobs queue
		bool initialProject(const Options option);								//create a project
		bool getProgressStatus();						//get progress status
		void setOption(const Options option);       //set option
		void loadOption(std::string confFile);
		bool writeConfig();
		bool writeJobQueueConfig(std::string path, std::string projectPath);
	private:
		void Run() override;

		void createInitialJob(std::string jobpath);        			//job for initial

		Thread* active_thread_;
		Options mOptions;
	};
}

