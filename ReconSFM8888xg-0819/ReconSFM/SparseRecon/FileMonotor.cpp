#include "FileMonotor.h"
#include "util/misc.h"

namespace sfmrecon {
	
FileMonotor::FileMonotor(const std::string& file_path,
  std::shared_ptr<Thread> task) {
  file_path_ = file_path;
  task_ = task;
}

FileMonotor::FileMonotor(
	Thread* task,
	ThreadPool *threadPool) {

	_mainThread = task;
	_mainPool = threadPool;
}


	FileMonotor::FileMonotor(){
	}
	void FileMonotor::setMonitorFile(std::string file) {
		file_path_ = file;
	}

	void FileMonotor::setMonitor(Thread* task, ThreadPool* pool) {
		_mainThread = task;
		_mainPool = pool;
	}

	void FileMonotor::Run() {
	  while (1) {
      if (IsStopped() || (task_ != nullptr && task_->IsFinished())) {
        return;
      }
		  
		  if (!ExistsFile(file_path_)) {
			  task_->Stop();
			  return;
		  }

		  Sleep(1000);
	  }
	  /*
		if (IsStopped()) {
		  return;
		}
		if (!ExistsFile(file_path_)) {
			std::cout << "call monitor end" << std::endl;
		
			if (_mainThread != nullptr)
				_mainThread->Stop();
			if (_mainPool != nullptr)
				_mainPool->Stop();
		
		  return;
		}

		Sleep(1000);
	  }
	  */
	 }

	bool FileMonotor::Sleep(int t) {
	  srand((int)time(0));
	  int time = rand() % t + 1;
	  std::this_thread::sleep_for(std::chrono::milliseconds(time));
	  return true;
	}
}