#pragma once
#include <string>
#include <unordered_map>
#include <iostream>
#include "util/misc.h"

namespace sfmrecon {
#define ONE_ROW_MAX_CHR 256
	class JobList
	{
	public:
		JobList();
		~JobList();

		void initial(std::string path);
		std::string getJob();
		bool JobList::getMaxJobId(int &maxNum);

		bool insertJob(std::string jobPath, std::string jobfile, int newMaxId);
		bool handJob(std::string jobPath, std::string jobfile);
		bool finishJob(std::string jobPath, std::string jobfile);
		bool refreshList(int insertId);
		bool checkFinishAll(std::string list);
		bool rollBack();
		bool rollBackSingle(std::string workspace);
		bool isNums(std::string str);
		bool isEmptyList();

	private:
		std::string mJobPath;
		std::string mJobListFile;
		std::string mJobWaitPath;
		std::string mJobHandlerPath;
		std::string mJobFinishPath;

		bool moveFile(std::string fromPath, std::string toPath);
		int del_assigned_row(FILE *fp_in, const char *path, int assign_line, std::string &str);

	};


}

