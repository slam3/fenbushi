#pragma once
#include <string>
#include <QString>
class EngineXmlConfig
{
public:
	EngineXmlConfig();
	~EngineXmlConfig();
	QString getMyIp();
	bool NewXmlFile(std::string enginconfPath);
	bool DeleteXmlFile(std::string enginconfPath);
	QString getData(std::string enginconfPath,std::string name);
};

