#include "ui/undistortion_widget.h"

namespace sfmrecon {

UndistortionWidget::UndistortionWidget(QWidget* parent,
                                       const OptionManager* options)
    : OptionsWidget(parent),
      options_(options),
      reconstruction_(nullptr),
      thread_control_widget_(new ThreadControlWidget(this)) {
  setWindowFlags(Qt::Dialog);
  setWindowModality(Qt::ApplicationModal);
  setWindowTitle("影像校正");
  UndistortCameraOptions default_options;
  AddOptionInt(&undistortion_options_.max_image_size, "level", -1);
  AddOptionDouble(&undistortion_options_.blank_pixels, "block_size", 0);
  AddOptionDirPath(&output_path_, "output_path");

  AddSpacer();
  QPushButton* undistort_button = new QPushButton(tr("影像校正"), this);
  connect(undistort_button, &QPushButton::released, this,
          &UndistortionWidget::Undistort);
  grid_layout_->addWidget(undistort_button, grid_layout_->rowCount(), 1);
}

void UndistortionWidget::Show(const Reconstruction& reconstruction) {
  reconstruction_ = &reconstruction;
  show();
  raise();
}

bool UndistortionWidget::IsValid() const { return ExistsDir(output_path_); }

void UndistortionWidget::Undistort() {
  CHECK_NOTNULL(reconstruction_);

  WriteOptions();

  if (IsValid()) {
    Thread* undistorter = nullptr;

      undistorter = new PMVSUndistorter(undistortion_options_, *reconstruction_,
                                        *options_->image_path, output_path_);

    thread_control_widget_->StartThread("Undistorting...", true, undistorter);
  } else {
    QMessageBox::critical(this, "", tr("Invalid output path"));
  }
}

}
