#include "ui/license_widget.h"

namespace sfmrecon {

LicenseWidget::LicenseWidget(QWidget* parent) : QTextEdit(parent) {
  setReadOnly(true);
  setWindowFlags(Qt::Dialog);
  resize(parent->width() - 20, parent->height() - 20);
  setWindowTitle("License");

  QString licenses;
  licenses += "<h2>MW3D</h2>";
  licenses += GetCOLMAPLicense();
  licenses += "<h2>External</h2>";
  licenses += "<h3>FLANN</h3>";
  licenses += GetFLANNLicense();
  licenses += "<h3>Graclus</h3>";
  licenses += GetGraclusLicense();
  licenses += "<h3>LSD</h3>";
  licenses += GetLSDLicense();
  licenses += "<h3>PBA</h3>";
  licenses += GetPBALicense();
  licenses += "<h3>PoissonRecon</h3>";
  licenses += GetPoissonReconLicense();
  licenses += "<h3>SiftGPU</h3>";
  licenses += GetSiftGPULicense();
  licenses += "<h3>SQLite</h3>";
  licenses += GetSQLiteLicense();
  licenses += "<h3>VLFeat</h3>";
  licenses += GetVLFeatLicense();
  setHtml(licenses);
}

QString LicenseWidget::GetCOLMAPLicense() const {
  const QString license =
      "Please Contact QQ 476242782";
  return license;
}

QString LicenseWidget::GetFLANNLicense() const {
  const QString license =
      "The BSD License<br>";
  return license;
}

QString LicenseWidget::GetGraclusLicense() const {
  const QString license =
      "Copyright(c)";
  return license;
}

QString LicenseWidget::GetLSDLicense() const {
  const QString license =
      "LSD - Line Segment Detector on digital images<br>";
  return license;
}

QString LicenseWidget::GetPBALicense() const {
  const QString license =
      "Copyright (c) 2011  Changchang Wu (ccwu@cs.washington.edu)<br>";
  return license;
}

QString LicenseWidget::GetPoissonReconLicense() const {
  const QString license =
      "Copyright (c) 2015 mkazhdan<br>";
  return license;
}

QString LicenseWidget::GetSiftGPULicense() const {
  const QString license =
      "Copyright (c) 2007 University of North Carolina at Chapel Hill<br>";
  return license;
}

QString LicenseWidget::GetSQLiteLicense() const {
  const QString license =
      "The author disclaims copyright to this source code. In place of<br>";
  return license;
}

QString LicenseWidget::GetVLFeatLicense() const {
  const QString license =
      "Copyright (C) 2007-11, Andrea Vedaldi and Brian Fulkerson<br>";
  return license;
}

} //
