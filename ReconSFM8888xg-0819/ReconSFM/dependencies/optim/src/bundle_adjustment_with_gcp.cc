#include "optim/bundle_adjustment_with_gcp.h"
#include "util/misc.h"
#include "base/camera_models.h"
#include "base/cost_functions.h"
#include "base/projection.h"
namespace sfmrecon {
bool BundleAdjustmentWithGCP::Solve(Reconstruction* reconstruction) {
	CHECK_NOTNULL(reconstruction);
	CHECK(!problem_) << "Cannot use the same BundleAdjuster multiple times";

	problem_.reset(new ceres::Problem());

	ceres::LossFunction* loss_function = options_.CreateLossFunction();
	SetUp(reconstruction, loss_function);

	if (problem_->NumResiduals() == 0) {
		return false;
	}

	ceres::Solver::Options solver_options = options_.solver_options;
	//solver_options.max_num_iterations = 500;
	solver_options.max_num_iterations = 100;
	solver_options.max_linear_solver_iterations = 1000;
	/************************/
	solver_options.parameter_tolerance = 0;
	solver_options.gradient_tolerance = 0;
	/************************/

	float progessBar_gcp = solver_options.max_num_iterations;
	std::cout << progessBar_gcp << std::endl;
	// Empirical choice.
	const size_t kMaxNumImagesDirectDenseSolver = 50;
	const size_t kMaxNumImagesDirectSparseSolver = 1000;
	const size_t num_images = config_.NumImages();
	if (num_images <= kMaxNumImagesDirectDenseSolver) {
		solver_options.linear_solver_type = ceres::DENSE_SCHUR;
	}
	else if (num_images <= kMaxNumImagesDirectSparseSolver) {
		solver_options.linear_solver_type = ceres::SPARSE_SCHUR;
	}
	else {  // Indirect sparse (preconditioned CG) solver.
		solver_options.linear_solver_type = ceres::ITERATIVE_SCHUR;
		solver_options.preconditioner_type = ceres::SCHUR_JACOBI;
	}

#ifdef OPENMP_ENABLED
	if (solver_options.num_threads <= 0) {
		solver_options.num_threads = omp_get_max_threads();
	}
#if CERES_VERSION_MAJOR < 2
	if (solver_options.num_linear_solver_threads <= 0) {
		solver_options.num_linear_solver_threads = omp_get_max_threads();
	}
#endif  // CERES_VERSION_MAJOR
#else
	solver_options.num_threads = 1;
#if CERES_VERSION_MAJOR < 2
	solver_options.num_linear_solver_threads = 1;
#endif  // CERES_VERSION_MAJOR
#endif

	std::string solver_error;
	CHECK(solver_options.IsValid(&solver_error)) << solver_error;

	ceres::Solve(solver_options, problem_.get(), &summary_);

	if (solver_options.minimizer_progress_to_stdout) {
		std::cout << std::endl;
	}

	if (options_.print_summary) {
		PrintHeading2("Bundle adjustment report");
		PrintSolverSummary(summary_);
	}
	return true;
}

void BundleAdjustmentWithGCP::SetUp(Reconstruction* reconstruction,
	ceres::LossFunction* loss_function) {
	for (const image_t image_id : config_.Images()) {
		AddImageToProblem(image_id, reconstruction, loss_function);
	}

  ParameterizeCameras(reconstruction);

	AddGCPPointsToProblem(reconstruction, loss_function);
}

void BundleAdjustmentWithGCP::AddImageToProblem(const image_t image_id, Reconstruction* reconstruction,
	ceres::LossFunction* loss_function) {
	Image& image = reconstruction->Image(image_id);
	Camera& camera = reconstruction->Camera(image.CameraId());

	// CostFunction assumes unit quaternions.
	image.NormalizeQvec();

	double* qvec_data = image.Qvec().data();
	double* tvec_data = image.Tvec().data();
	double* camera_params_data = camera.ParamsData();

	// Add residuals to bundle adjustment problem.
	size_t num_observations = 0;
	for (const Point2D& point2D : image.Points2D()) {
		if (!point2D.HasPoint3D()) {
			continue;
		}

		num_observations += 1;
		point3D_num_observations_[point2D.Point3DId()] += 1;

		Point3D& point3D = reconstruction->Point3D(point2D.Point3DId());
		assert(point3D.Track().Length() > 1);

		ceres::CostFunction* cost_function = nullptr;

		switch (camera.ModelId()) {
#define CAMERA_MODEL_CASE(CameraModel)                                   \
			case CameraModel::kModelId:                                            \
			cost_function =                                                      \
				BundleAdjustmentCostFunction<CameraModel>::Create(point2D.XY()); \
			break;

			CAMERA_MODEL_SWITCH_CASES
#undef CAMERA_MODEL_CASE
		}

		problem_->AddResidualBlock(cost_function, loss_function, qvec_data,
			tvec_data, point3D.XYZ().data(),
			camera_params_data);
	}
	

	if (num_observations > 0) {
		camera_ids_.insert(image.CameraId());
		ceres::LocalParameterization* quaternion_parameterization =
			new ceres::QuaternionParameterization;
		problem_->SetParameterization(qvec_data, quaternion_parameterization);
	}
}

void BundleAdjustmentWithGCP::AddPointToProblem(const point3D_t point3D_id,
	Reconstruction* reconstruction,
	ceres::LossFunction* loss_function) {
}

void BundleAdjustmentWithGCP::AddGCPPointsToProblem(Reconstruction* reconstruction,
	ceres::LossFunction* loss_function) {
	std::vector<gcp::GCPInfo> &gcp_infos = reconstruction->GCPInfos();

	for (auto& gcp_info : gcp_infos) {
		for (auto& observation : gcp_info.observations) {
			Image& image = reconstruction->Image(observation.image_id);
			Camera& camera = reconstruction->Camera(image.CameraId());

			image.NormalizeQvec();

			double* qvec_data = image.Qvec().data();
			double* tvec_data = image.Tvec().data();
			double* camera_params_data = camera.ParamsData();
			ceres::CostFunction* cost_function = nullptr;

      switch (camera.ModelId()) {
#define CAMERA_MODEL_CASE(CameraModel)                                   \
			case CameraModel::kModelId:                                            \
			cost_function = WeightedCostFunction<CameraModel>::Create(observation.pixel_coord, 500.0); \
			break;

				CAMERA_MODEL_SWITCH_CASES
#undef CAMERA_MODEL_CASE

//#define CAMERA_MODEL_CASE(CameraModel)                                   \
//			case CameraModel::kModelId:                                            \
//			cost_function =                                                      \
//				BundleAdjustmentCostFunction<CameraModel>::Create(observation.pixel_coord); \
//			break;
//
//        CAMERA_MODEL_SWITCH_CASES
//#undef CAMERA_MODEL_CASE
			}

			problem_->AddResidualBlock(cost_function, loss_function, qvec_data,
				tvec_data, gcp_info.utm_coord.data(),
				camera_params_data);
		}
    problem_->SetParameterBlockConstant(gcp_info.utm_coord.data());
	}
}
}