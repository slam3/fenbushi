#include "base/gcp.h"
#include <io.h>
#include<iostream>
#include<fstream>
#include<sstream>
#include<base/gps.h>

namespace gcp {
	using namespace std;

	bool ParseGCPInfosFromTxt(const std::string& txt_path, std::vector<GCPInfo>& gcp_infos) {
		if (_access(txt_path.c_str(), 00)){
			std::cerr << "gcp_path is not exist: " << txt_path << std::endl;
			return false;
		}
		std::ifstream fs(txt_path);
		gcp_infos.clear();
		
		string line;
		sfmrecon::GPSTransform gps_transform(sfmrecon::GPSTransform::WGS84);
		while (std::getline(fs, line)) {
			istringstream ss(line);
			if (line[0] != ' ') {
				struct GCPInfo gcp_info;
				ss >> gcp_info.id >> gcp_info.coord(1) >> gcp_info.coord(0) >> gcp_info.coord(2);
				gcp_info.utm_coord = gps_transform.EllToXYZ(gcp_info.coord);
				gcp_infos.push_back(gcp_info);
			} else{
				assert(gcp_infos.size() > 0);
				struct Observation observation;
				int id;
				ss >> id >> observation.image_path >> observation.pixel_coord(0) >> observation.pixel_coord(1);
				gcp_infos[gcp_infos.size() - 1].observations.push_back(observation);
			}
			

		}

		

			
		return true;
	}
}