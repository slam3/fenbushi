#include <base/at_gcp_report.h>
#include <iostream>

namespace sfmrecon {
    void ATGCPReport::Results::Load(const pugi::xml_node& node) {
        if (!node) {
            return;
        }
	    //A. Global:
        if (node.child("Global")) {
            global_info.Load(node.child("Global"));
        }
        //B.Per photo:
        if (node.child("Per_photo")) {
            pugi::xml_node photo = node.child("Per_photo").child("Control_points").first_child();
            while (photo) {
                PerPhoto_ControlPoints photo_info;
                photo_info.Load(photo);
                //Perphoto_photo_infos.emplace_back(photo_info);
                Perphoto_photo_infos_map.insert(std::make_pair(photo_info.file_name,photo_info));
                photo = photo.next_sibling();
            }
        }
        //C. Per point:
        if (node.child("Per_point")) {
            pugi::xml_node point = node.child("Per_point").child("Control_points").first_child();
            while (point) {
                PerPoint_ControlPoints point_info;
                point_info.Load(point);
                Perpoint_control_points_map.insert(make_pair(point_info.name,point_info));
                point = point.next_sibling();
            }

            PerPoint_automatic_tie_points.Load(node.child("Per_point").child("Automatic_tie_points"));
        }
    }

    void ATGCPReport::Results::Save(pugi::xml_node& node) const {
        //A. Global:
        global_info.Save(node.append_child("Global"));
        //B.Per photo:
        pugi::xml_node photo_infos_node = node.append_child("Per_photo").append_child("Control_points");
        for (auto& photo_info : Perphoto_photo_infos_map) {
            photo_info.second.Save(photo_infos_node.append_child("Control_point"));
        }
        //C. Per point:
        pugi::xml_node per_point_root = node.append_child("Per_point");
        pugi::xml_node control_points_node= per_point_root.append_child("Control_points");
        for (auto& point_info : Perpoint_control_points_map) {
            point_info.second.Save(control_points_node.append_child("Control_point"));
        }
        //- Automatic tie points:
        PerPoint_automatic_tie_points.Save(per_point_root.append_child("Automatic_tie_points"));
    }

    void ATGCPReport::LoadFromXml(const std::string& xml_path) {
        pugi::xml_document doc;
        pugi::xml_parse_result result = doc.load_file(xml_path.c_str());

        settings.Load(doc.child("settings"));

        if (doc.child("results")) {
            results.Load(doc.child("results"));
        }
    }

    void ATGCPReport::ExportXml(const std::string& xml_path) const {
        pugi::xml_document doc;
        pugi::xml_node decl = doc.prepend_child(pugi::node_declaration);
        decl.append_attribute("version") = "1.0";
        decl.append_attribute("encoding") = "utf-8";
        settings.Save(doc.append_child("settings"));
        results.Save(doc.append_child("results"));

        doc.save_file(xml_path.c_str());
    }

    void ATGCPReport::PerPhoto_ControlPoints::Load(const pugi::xml_node & node) {
        if (!node) {
            return;
        }
        photogroup = node.child("Photogroup").text().as_int();
        file_name = node.child("File_name").text().as_string();
        before_at_number_of_points = node.child("before_AT:Number_of_points").text().as_int();
        before_at_rms_of_reprojection_errors = node.child("before_AT:RMS_of_reprojection_errors_px").text().as_float();
        before_at_rms_of_distances_to_rays = node.child("before_AT:RMS_of_distances_to_rays_m").text().as_float();
        after_at_number_of_points = node.child("after_AT:Number_of_points").text().as_int();
        after_at_rms_of_reprojection_errors = node.child("after_AT:RMS_of_reprojectsion_errors_px").text().as_float();
        after_at_rms_of_distances_to_rays = node.child("after_AT:RMS_of_distances_to_rays_m").text().as_float();
    }

    void ATGCPReport::PerPhoto_ControlPoints::Save(pugi::xml_node & node) const {
        node.append_child("Photogroup").append_child(pugi::node_pcdata).set_value(photogroup);
        node.append_child("File_name").append_child(pugi::node_pcdata).set_value(file_name);
        node.append_child("before_AT:Number_of_points").append_child(pugi::node_pcdata).set_value(before_at_number_of_points);
        node.append_child("before_AT:RMS_of_reprojection_errors_px").append_child(pugi::node_pcdata).set_value(before_at_rms_of_reprojection_errors);
        //node.append_child("before_AT:RMS_of_distances_to_rays_m").append_child(pugi::node_pcdata).set_value(before_at_rms_of_distances_to_rays);
        node.append_child("after_AT:Number_of_points").append_child(pugi::node_pcdata).set_value(after_at_number_of_points);
        node.append_child("after_AT:RMS_of_reprojection_errors_px").append_child(pugi::node_pcdata).set_value(after_at_rms_of_reprojection_errors);
        //node.append_child("after_AT:RMS_of_distances_to_rays_m").append_child(pugi::node_pcdata).set_value(after_at_rms_of_distances_to_rays);
    }


    void ATGCPReport::PerPoint_ControlPoints::Load(const pugi::xml_node & node) {
        if (!node) {
            return;
        }
        name = node.child("Name").text().as_string();
        category = node.child("Category").text().as_string();
        checkpoint = node.child("Check_point").text().as_string();
        horizontal_accuracy = node.child("Horizontal_accuracy_m").text().as_float();
        vertical_accuracy = node.child("Vertical_accuracy_m").text().as_float();

        before_at_number_of_photos = node.child("before_AT:Number_of_points").text().as_int();
        before_at_rms_of_reprojection_errors = node.child("before_AT:RMS_of_reprojection_errors_px").text().as_float();
        before_at_rms_of_distances_to_rays = node.child("before_AT:RMS_of_distances_to_rays_m").text().as_float();
        before_at_rms_of_3d_errors = node.child("before_AT:RMS_of_3D_errors_m").text().as_float();
        before_at_rms_of_horizontal_errors = node.child("before_AT:RMS_of_horizontal_errors_m").text().as_float();
        before_at_rms_of_vertical_errors = node.child("before_AT:RMS_of_vertical_errors_m").text().as_float();

        after_at_number_of_photos = node.child("after_AT:Number_of_photos").text().as_int();
        after_at_rms_of_reprojection_errors = node.child("after_AT:RMS_of_reprojection_errors_px").text().as_float();
        after_at_rms_of_distances_to_rays = node.child("after_AT:RMS_of_distances_to_rays_m").text().as_float();
        after_at_rms_of_3d_errors = node.child("after_AT:RMS_of_3D_errors_m").text().as_float();
        after_at_rms_of_horizontal_errors = node.child("after_AT:RMS_of_horizontal_errors_m").text().as_float();
        after_at_rms_of_vertical_errors = node.child("after_AT:RMS_of_vertical_errors_m").text().as_float();

    }

    void ATGCPReport::PerPoint_ControlPoints::Save(pugi::xml_node & node) const {
        node.append_child("Name").append_child(pugi::node_pcdata).set_value(name);
        node.append_child("Category").append_child(pugi::node_pcdata).set_value(category);
        node.append_child("Check_point").append_child(pugi::node_pcdata).set_value(checkpoint);
//         node.append_child("Horizontal_accuracy_m").append_child(pugi::node_pcdata).set_value(horizontal_accuracy);
//         node.append_child("Vertical_accuracy_m").append_child(pugi::node_pcdata).set_value(vertical_accuracy);

        node.append_child("before_AT:Number_of_photos").append_child(pugi::node_pcdata).set_value(before_at_number_of_photos);
        node.append_child("before_AT:RMS_of_reprojection_errors_px").append_child(pugi::node_pcdata).set_value(before_at_rms_of_reprojection_errors);
//         node.append_child("before_AT:RMS_of_distances_to_rays_m").append_child(pugi::node_pcdata).set_value(before_at_rms_of_distances_to_rays);
//         node.append_child("before_AT:RMS_of_3D_errors_m").append_child(pugi::node_pcdata).set_value(before_at_rms_of_3d_errors);
//         node.append_child("before_AT:RMS_of_horizontal_errors_m").append_child(pugi::node_pcdata).set_value(before_at_rms_of_horizontal_errors);
//         node.append_child("before_AT:RMS_of_vertical_errors_m").append_child(pugi::node_pcdata).set_value(before_at_rms_of_vertical_errors);

        node.append_child("after_AT:Number_of_photos").append_child(pugi::node_pcdata).set_value(after_at_number_of_photos);
        node.append_child("after_AT:RMS_of_reprojection_errors_px").append_child(pugi::node_pcdata).set_value(after_at_rms_of_reprojection_errors);
//         node.append_child("after_AT:RMS_of_distances_to_rays_m").append_child(pugi::node_pcdata).set_value(after_at_rms_of_distances_to_rays);
//         node.append_child("after_AT:RMS_of_3D_errors_m").append_child(pugi::node_pcdata).set_value(after_at_rms_of_3d_errors);
//         node.append_child("after_AT:RMS_of_horizontal_errors_m").append_child(pugi::node_pcdata).set_value(after_at_rms_of_horizontal_errors);
//         node.append_child("after_AT:RMS_of_vertical_errors_m").append_child(pugi::node_pcdata).set_value(after_at_rms_of_vertical_errors);
    }


    void ATGCPReport::GlobalInfo::Load(const pugi::xml_node & node) {
        if (!node) {
            return;
        }
	    positioning_mode = node.child("Positioning_mode").text().as_string();

        if (node.child("Errors").child("Control_points")) {
            control_points.Load(node.child("Errors").child("Control_points"));
        }
        if (node.child("Errors").child("Automatic_tie_points")) {
            automatic_tie_points.Load(node.child("Errors").child("Automatic_tie_points"));
        }
        //connections
        pugi::xml_node  conn_node=node.child("Connections");
        conn_info.number_of_tested_pairs = conn_node.child("Number_of_tested_pairs").text().as_int();
        conn_info.median_number_of_tested_pairs_per_photo = conn_node.child("Median_number_of_tested_pairs_per_photo").text().as_int();
        conn_info.before_at_median_number_of_connected_photos_per_photo = conn_node.child("before_AT:Median_number_of_connected_photos_per_photo").text().as_int();
        conn_info.after_at_median_number_of_connected_photos_per_photo = conn_node.child("after_AT:Median_number_of_connected_photos_per_photo").text().as_int();
    }
    void ATGCPReport::GlobalInfo::Save(pugi::xml_node & node) const {
        //positioning mode
        node.append_child("Positioning_mode").append_child(pugi::node_pcdata).set_value(positioning_mode);
        //errors
        pugi::xml_node & errors_node = node.append_child("Errors");
        control_points.Save(errors_node.append_child("Control_points"));
        automatic_tie_points.Save(errors_node.append_child("Automatic_tie_points"));
        //connections
        pugi::xml_node & conn_node = node.append_child("Connections");
        conn_node.append_child("Number_of_tested_pairs").append_child(pugi::node_pcdata).set_value(conn_info.number_of_tested_pairs);
        conn_node.append_child("Median_number_of_tested_pairs_per_photo").append_child(pugi::node_pcdata).set_value(conn_info.median_number_of_tested_pairs_per_photo);
        conn_node.append_child("before_AT:Median_number_of_connected_photos_per_photo").append_child(pugi::node_pcdata).set_value(conn_info.before_at_median_number_of_connected_photos_per_photo);
        conn_node.append_child("after_AT:Median_number_of_connected_photos_per_photo").append_child(pugi::node_pcdata).set_value(conn_info.after_at_median_number_of_connected_photos_per_photo);

    }

    void ATGCPReport::global_errors_info::Save(pugi::xml_node & node) const {
        node.append_child("before_AT:Number_of_points").append_child(pugi::node_pcdata).set_value(before_at_number_of_points);
        node.append_child("before_AT:Median_reprojection_error_px").append_child(pugi::node_pcdata).set_value(before_at_median_reprojection_error);
        node.append_child("before_AT:RMS_of_reprojection_errors_px").append_child(pugi::node_pcdata).set_value(before_at_rms_of_reprojection_errors);
//         node.append_child("before_AT:RMS_of_distances_to_rays_m").append_child(pugi::node_pcdata).set_value(before_at_rms_of_distances_to_rays);
//         node.append_child("before_AT:RMS_of_3D_errors_m").append_child(pugi::node_pcdata).set_value(before_at_rms_of_3d_errors);
//         node.append_child("before_AT:RMS_of_horizontal_errors_m").append_child(pugi::node_pcdata).set_value(before_at_rms_of_horizontal_errors);
//         node.append_child("before_AT:RMS_of_vertical_errors_m").append_child(pugi::node_pcdata).set_value(before_at_rms_of_vertical_errors);

        node.append_child("after_AT:Number_of_points").append_child(pugi::node_pcdata).set_value(after_at_number_of_points);
        node.append_child("after_AT:Median_reprojection_error_px").append_child(pugi::node_pcdata).set_value(after_at_median_reprojection_error);
        node.append_child("after_AT:RMS_of_reprojection_errors_px").append_child(pugi::node_pcdata).set_value(after_at_rms_of_reprojection_errors);
//         node.append_child("after_AT:RMS_of_distances_to_rays_m").append_child(pugi::node_pcdata).set_value(after_at_rms_of_distances_to_rays);
//         node.append_child("after_AT:RMS_of_3D_errors_m").append_child(pugi::node_pcdata).set_value(after_at_rms_of_3d_errors);
//         node.append_child("after_AT:RMS_of_horizontal_errors_m").append_child(pugi::node_pcdata).set_value(after_at_rms_of_horizontal_errors);
//         node.append_child("after_AT:RMS_of_vertical_errors_m").append_child(pugi::node_pcdata).set_value(after_at_rms_of_vertical_errors);
    }

    void ATGCPReport::global_errors_info::Load(const pugi::xml_node & node) {
        if (!node) {
            return;
        }
        before_at_number_of_points = node.child("before_AT:Number_of_points").text().as_int();
        before_at_median_reprojection_error = node.child("before_AT:Median_reprojection_error_px").text().as_float();
        before_at_rms_of_reprojection_errors = node.child("before_AT:RMS_of_reprojection_errors_px").text().as_float();
        before_at_rms_of_distances_to_rays = node.child("before_AT:RMS_of_distances_to_rays_m").text().as_float();
        before_at_rms_of_3d_errors = node.child("before_AT:RMS_of_3D_errors_m").text().as_float();
        before_at_rms_of_horizontal_errors = node.child("before_AT:RMS_of_horizontal_errors_m").text().as_float();
        before_at_rms_of_vertical_errors = node.child("before_AT:RMS_of_vertical_errors_m").text().as_float();

        after_at_number_of_points = node.child("after_AT:Number_of_points").text().as_int();
        after_at_median_reprojection_error = node.child("after_AT:Median_reprojection_error_px").text().as_float();
        after_at_rms_of_reprojection_errors = node.child("after_AT:RMS_of_reprojection_errors_px").text().as_float();
        after_at_rms_of_distances_to_rays = node.child("after_AT:RMS_of_distances_to_rays_m").text().as_float();
        after_at_rms_of_3d_errors = node.child("after_AT:RMS_of_3D_errors_m").text().as_float();
        after_at_rms_of_horizontal_errors = node.child("after_AT:RMS_of_horizontal_errors_m").text().as_float();
        after_at_rms_of_vertical_errors = node.child("after_AT:RMS_of_vertical_errors_m").text().as_float();
    }


    void ATGCPReport::PerPoint_AutomaticTiePoints::Save(pugi::xml_node & node) const {
        node.append_child("Median_number_of_key_points_per_photo").append_child(pugi::node_pcdata).set_value(median_number_of_key_points_per_photo);

        node.append_child("before_AT:Number_of_points").append_child(pugi::node_pcdata).set_value(before_at_number_of_points);
        node.append_child("before_AT:Median_number_of_photos_per_point").append_child(pugi::node_pcdata).set_value(before_at_median_number_of_photos_per_point);
        node.append_child("before_AT:Median_number_of_points_per_photo").append_child(pugi::node_pcdata).set_value(before_at_median_number_of_points_per_photo);
        node.append_child("before_AT:RMS_of_reprojection_errors_px").append_child(pugi::node_pcdata).set_value(before_at_rms_of_reprojection_errors);
/*        node.append_child("before_AT:RMS_of_distances_to_rays_m").append_child(pugi::node_pcdata).set_value(before_at_rms_of_distances_to_rays);*/

        node.append_child("after_AT:Number_of_points").append_child(pugi::node_pcdata).set_value(after_at_number_of_points);
        node.append_child("after_AT:Median_number_of_photos_per_point").append_child(pugi::node_pcdata).set_value(after_at_median_number_of_photos_per_point);
        node.append_child("after_AT:Median_number_of_points_per_photo").append_child(pugi::node_pcdata).set_value(after_at_median_number_of_points_per_photo);
        node.append_child("after_AT:RMS_of_reprojection_errors_px").append_child(pugi::node_pcdata).set_value(after_at_rms_of_reprojection_errors);
/*        node.append_child("after_AT:RMS_of_distances_to_rays_m").append_child(pugi::node_pcdata).set_value(after_at_rms_of_distances_to_rays);*/
    }

    void ATGCPReport::PerPoint_AutomaticTiePoints::Load(const pugi::xml_node & node) {
        if (!node) {
            return;
        }
        median_number_of_key_points_per_photo = node.child("Median_number_of_key_points_per_photo").text().as_int();

        before_at_number_of_points = node.child("before_AT:Number_of_points").text().as_int();
        before_at_median_number_of_photos_per_point = node.child("before_AT:Median_number_of_photos_per_point").text().as_float();
        before_at_median_number_of_points_per_photo = node.child("before_AT:Median_number_of_points_per_photo").text().as_float();
        before_at_rms_of_reprojection_errors = node.child("before_AT:RMS_of_reprojection_errors_px").text().as_float();
        before_at_rms_of_distances_to_rays = node.child("before_AT:RMS_of_distances_to_rays_m").text().as_float();

        after_at_number_of_points = node.child("after_AT:Number_of_points").text().as_int();
        after_at_median_number_of_photos_per_point = node.child("after_AT:Median_number_of_photos_per_point").text().as_float();
        after_at_median_number_of_points_per_photo = node.child("after_AT:Median_number_of_points_per_photo").text().as_float();
        after_at_rms_of_reprojection_errors = node.child("after_AT:RMS_of_reprojection_errors_px").text().as_float();
        after_at_rms_of_distances_to_rays = node.child("after_AT:RMS_of_distances_to_rays_m").text().as_float();
    }
}


