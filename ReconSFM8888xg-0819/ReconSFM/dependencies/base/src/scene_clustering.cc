// Copyright (c) 2018, ETH Zurich and UNC Chapel Hill.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//
//     * Neither the name of ETH Zurich and UNC Chapel Hill nor the names of
//       its contributors may be used to endorse or promote products derived
//       from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Author: Johannes L. Schoenberger (jsch-at-demuc-dot-de)

#include "base/scene_clustering.h"

#include <set>

#include "base/database.h"
#include "base/graph_cut.h"
#include "util/random.h"
#include <glog/logging.h>

namespace sfmrecon {

bool SceneClustering::Options::Check() const {
  CHECK_OPTION_GT(branching, 0);
  CHECK_OPTION_GE(image_overlap, 0);
  return true;
}
SceneClustering::SceneClustering(const Options& options) : options_(options) {
  CHECK(options_.Check());
}

void SceneClustering::Partition(
    const std::vector<std::pair<image_t, image_t>>& image_pairs,
    const std::vector<int>& num_inliers) {
  CHECK(!root_cluster_);
  CHECK_EQ(image_pairs.size(), num_inliers.size());

  std::set<image_t> image_ids;
  std::vector<std::pair<int, int>> edges;
  edges.reserve(image_pairs.size());
  for (const auto& image_pair : image_pairs) {
    image_ids.insert(image_pair.first);
    image_ids.insert(image_pair.second);
    edges.emplace_back(image_pair.first, image_pair.second);
  }

  root_cluster_.reset(new Cluster());
  root_cluster_->image_ids.insert(image_ids.begin(), image_ids.end());
  PartitionCluster(edges, num_inliers, root_cluster_.get());
}

void SceneClustering::PartitionCluster(
    const std::vector<std::pair<int, int>>& edges,
    const std::vector<int>& weights, Cluster* cluster) {
  CHECK_EQ(edges.size(), weights.size());
  // If the cluster is small enough, we return from the recursive clustering.
  if (edges.size() == 0 || cluster->image_ids.size() <=
          static_cast<size_t>(options_.leaf_max_num_images)) {
    return;
  }

  // Partition the cluster using a normalized cut on the scene graph.
  const auto labels = ComputeNormalizedMinGraphCutModified(edges, weights, options_.branching,cluster,options_.image_overlap);

  // Assign the images to the clustered child clusters.
  cluster->child_clusters.resize(options_.branching);
  for (const auto image_id : cluster->image_ids) {
    if (labels.count(image_id)) {
      auto& child_cluster = cluster->child_clusters.at(labels.at(image_id));
      child_cluster.image_ids.insert(image_id);
    }
  }

  // Collect the edges based on whether they are inter or intra child clusters.
  std::vector<std::vector<std::pair<int, int>>> child_edges(options_.branching);
  std::vector<std::vector<int>> child_weights(options_.branching);
  std::vector<std::vector<std::pair<std::pair<int, int>, int>>>
      overlapping_edges(options_.branching);
  for (size_t i = 0; i < edges.size(); ++i) {
    const int label1 = labels.at(edges[i].first);
    const int label2 = labels.at(edges[i].second);
    if (label1 == label2) {
      child_edges.at(label1).push_back(edges[i]);
      child_weights.at(label1).push_back(weights[i]);
    } else {
      overlapping_edges.at(label1).emplace_back(edges[i], weights[i]);
      overlapping_edges.at(label2).emplace_back(edges[i], weights[i]);
    }
  }

  for (int i = 0; i < options_.branching; ++i) {
    if (!ClusterCheck(child_edges[i], &cluster->child_clusters[i])) {
      LOG(ERROR) << "ComputeNormalizedMinGraphCut error";
      cluster->child_clusters.clear();
      return;
    }
  }
  if (options_.image_overlap > 0) {
    for (int i = 0; i < options_.branching; ++i) {
      // Sort the overlapping edges by the number of inlier matches, such
      // that we add overlapping images with many common observations.
      std::sort(overlapping_edges[i].begin(), overlapping_edges[i].end(),
                [](const std::pair<std::pair<int, int>, int>& edge1,
                   const std::pair<std::pair<int, int>, int>& edge2) {
                  return edge1.second > edge2.second;
                });

      // Select overlapping edges at random and add image to cluster.
      std::set<int> overlapping_image_ids;
      for (const auto& edge : overlapping_edges[i]) {
        if (labels.at(edge.first.first) == i) {
          overlapping_image_ids.insert(edge.first.second);
        } else {
          overlapping_image_ids.insert(edge.first.first);
        }

        child_edges[i].emplace_back(edge.first);
        child_weights[i].emplace_back(edge.second);
        if (overlapping_image_ids.size() >=
            static_cast<size_t>(options_.image_overlap)) {
          break;
        }
      }

      if (overlapping_image_ids.size() < static_cast<size_t>(options_.image_overlap)) {
          LOG(ERROR) << "overlapping_edges less " << options_.image_overlap;
          LOG(ERROR) << "overlapping_edges is " << overlapping_image_ids.size();
          LOG(ERROR) << "image_ids num is  " << cluster->image_ids.size();
        cluster->child_clusters.clear();
        return;
      }

      cluster->child_clusters[i].image_ids.insert(overlapping_image_ids.begin(),
        overlapping_image_ids.end());
    }
  }

  // Recursively partition all the child clusters.
  for (int i = 0; i < options_.branching; ++i) {
      if (!ClusterCheck(child_edges[i], &cluster->child_clusters[i])) {
          LOG(ERROR) << "after insert overlap error";
          cluster->child_clusters.clear();
          return;
      }

    int domain_size = CheckConnectedDomain(child_edges[i]);
    if (domain_size > 1) {
      LOG(ERROR) << "domain_size error: " << domain_size;
      cluster->child_clusters.clear();
      return;
    }

    PartitionCluster(child_edges[i], child_weights[i],
      &cluster->child_clusters[i]);
  }
}

const SceneClustering::Cluster* SceneClustering::GetRootCluster() const {
  return root_cluster_.get();
}

std::vector<const SceneClustering::Cluster*> SceneClustering::GetLeafClusters()
    const {
  CHECK(root_cluster_);

  std::vector<const Cluster*> leaf_clusters;

  if (!root_cluster_) {
    return leaf_clusters;
  } else if (root_cluster_->child_clusters.empty()) {
    leaf_clusters.push_back(root_cluster_.get());
    return leaf_clusters;
  }

  std::vector<const Cluster*> non_leaf_clusters;
  non_leaf_clusters.push_back(root_cluster_.get());

  while (!non_leaf_clusters.empty()) {
    const auto cluster = non_leaf_clusters.back();
    non_leaf_clusters.pop_back();

    for (const auto& child_cluster : cluster->child_clusters) {
      if (child_cluster.child_clusters.empty()) {
        leaf_clusters.push_back(&child_cluster);
      } else {
        non_leaf_clusters.push_back(&child_cluster);
      }
    }
  }

  return leaf_clusters;
}

template<class T>
int SceneClustering::CheckConnectedDomain(const std::vector<std::pair<T, T>>&edges, bool PrintDomain) {
  std::vector<std::set<T>> connected_domains;
  ConnectedDomains(edges, connected_domains);

  if (PrintDomain) {
    std::cout << "********************************connected_domains*************************" << std::endl;
    for (int i = 0; i < connected_domains.size(); ++i) {
      std::cout << "domains " << i << " has image: " << connected_domains[i].size() << std::endl;
    }
    std::cout << "********************************connected_domains*************************" << std::endl;
  }
  
  return connected_domains.size();
}
void SceneClustering::MergeConnectedDomainsPhase1(
    const std::vector<std::pair<int, int>>& edges,
    const std::vector<int>& weights, const int num_parts, Cluster *cluster,
    std::unordered_map<int, int> &labels)
{
    std::vector<Cluster> child_clusters;
    child_clusters.resize(num_parts);

    for (const auto image_id : cluster->image_ids) {
        if (labels.count(image_id)) {
            auto& child_cluster = child_clusters.at(labels.at(image_id));
            child_cluster.image_ids.insert(image_id);
        }
    }
    std::vector<std::vector<std::pair<int, int>>> child_edges(num_parts);
    std::vector<std::vector<int>> child_weights(num_parts);
    std::vector<std::pair<std::pair<int, int>, int>>  overlapping_edges;
    for (size_t i = 0; i < edges.size(); ++i) {
        const int label1 = labels.at(edges[i].first);
        const int label2 = labels.at(edges[i].second);
        if (label1 == label2) {
            child_edges.at(label1).push_back(edges[i]);
            child_weights.at(label1).push_back(weights[i]);
        }
        else {
            overlapping_edges.emplace_back(edges[i], weights[i]);
        }
    }
    for (auto &edge : overlapping_edges)
    {
        bool in_child_edges1 = false;
        bool in_child_edges2 = false;

        for (auto &child_edge : child_edges[0])
        {
            if (edge.first.first == child_edge.first || edge.first.first == child_edge.second ||
                edge.first.second == child_edge.first || edge.first.second == child_edge.second)
            {
                in_child_edges1 = true;
                break;
            }
        }
        for (auto child_edge : child_edges[1])
        {
            if (edge.first.first == child_edge.first || edge.first.first == child_edge.second ||
                edge.first.second == child_edge.first || edge.first.second == child_edge.second)
            {
                in_child_edges2 = true;
                break;
            }
        }

        if (!in_child_edges1 && !in_child_edges2)
        {
            LOG(ERROR) << "clustering error #1" << std::endl;
            break;
        }
        else if (in_child_edges1 && in_child_edges2)
        {

        }
        else if (in_child_edges1 || in_child_edges2)
        {
            std::unordered_map<int, int>::iterator a = labels.find(edge.first.first);
            if (labels.end() != a) {
                a->second = in_child_edges1 ? 0 : 1;
            }
            a = labels.find(edge.first.second);
            if (labels.end() != a) {
                a->second = in_child_edges1 ? 0 : 1;
            }
        }
        else
        {
            LOG(ERROR) << "clustering error #2" << std::endl;
        }
    }
}
void SceneClustering::MergeConnectedDomainsPhase2(
    const std::vector<std::pair<int, int>>& edges,
    const std::vector<int>& weights, const int num_parts, Cluster *cluster,
    std::unordered_map<int, int> &labels,
    int image_overlap_threshold)
{
    std::vector<std::vector<std::pair<int, int>>> child_edges(num_parts);
    std::vector<std::vector<int>> child_weights(num_parts);
    std::vector<std::vector<std::pair<std::pair<int, int>, int>>>
        overlapping_edges(num_parts);

    for (size_t i = 0; i < edges.size(); ++i) {
        const int label1 = labels.at(edges[i].first);
        const int label2 = labels.at(edges[i].second);
        if (label1 == label2) {
            child_edges.at(label1).push_back(edges[i]);
            child_weights.at(label1).push_back(weights[i]);
        }
        else {
            overlapping_edges.at(label1).emplace_back(edges[i], weights[i]);
            overlapping_edges.at(label2).emplace_back(edges[i], weights[i]);
        }
    }

    std::vector<std::vector<std::set<int>>> connected_domains(num_parts);
    for (int i = 0; i < num_parts; ++i) {
        ConnectedDomains(child_edges[i], connected_domains[i]);
    }

    std::vector<std::vector<std::set<int>>> to_insert;
    to_insert.resize(2);
    for (const auto&edge : overlapping_edges[0]) {
        for (int i = 0; i < num_parts; i++)
        {
            to_insert[i].resize(connected_domains[i].size());
            for (int j = 0; j < connected_domains[i].size(); j++)
            {
                if (connected_domains[i][j].count(edge.first.first) || connected_domains[i][j].count(edge.first.second))
                {
                    to_insert[i][j].insert(edge.first.first);
                    to_insert[i][j].insert(edge.first.second);
                }
            }
        }
    }
    for (int i = 0; i < num_parts; i++)
    {
        for (int j = 0; j < to_insert[i].size(); j++)
        {
            connected_domains[i][j].insert(to_insert[i][j].begin(), to_insert[i][j].end());
        }
    }

    bool do_merge = false;
    for (int i = 0; i < num_parts; i++) {
        if (connected_domains[i].size() > 1)
            do_merge = true;
    }
    while (do_merge) {
        for (int i = 0; i < num_parts; i++)
        {
            std::sort(connected_domains[i].begin(), connected_domains[i].end(),
                [](const std::set<int>& domain1,
                    const std::set<int>& domain2) {
                return domain1.size() > domain2.size();
            });
        }

        for (int i = 0; i < num_parts; i++)
        {
            if (connected_domains[i].size() > 1)
            {
                for (int j = connected_domains[i].size() - 1; j > 0; j--)
                {
                    for (int k = 0; k <= connected_domains[1 - i].size() - 1; k++)
                    {
                        std::vector<image_t> intersection;
                        std::set_intersection(connected_domains[i][j].begin(), connected_domains[i][j].end(),
                            connected_domains[1 - i][k].begin(), connected_domains[1 - i][k].end(), back_inserter(intersection));
#if 0
                        if (intersection.size()>=image_overlap_threshold) {
#else
                        if (intersection.size()) {
#endif
                            for (auto &img_id : connected_domains[i][j])
                            {
                                labels.at(img_id) = 1 - i;
                            }
                            connected_domains[1 - i][k].insert(connected_domains[i][j].begin(), connected_domains[i][j].end());
                            connected_domains[i].erase(connected_domains[i].begin() + j);
                        }
                    }
                }
            }
        }

        do_merge = false;
        for (int i = 0; i < num_parts; i++) {
            if (connected_domains[i].size() > 1)
                do_merge = true;
        }
    }
}
std::unordered_map<int, int> SceneClustering::ComputeNormalizedMinGraphCutModified(
    const std::vector<std::pair<int, int>>& edges,
    const std::vector<int>& weights, const int num_parts, Cluster *cluster,
    int image_overlap_threshold)
{
    auto labels = ComputeNormalizedMinGraphCut(edges, weights, num_parts);
    MergeConnectedDomainsPhase1(edges, weights, num_parts, cluster, labels);
    MergeConnectedDomainsPhase2(edges, weights, num_parts, cluster, labels, image_overlap_threshold);
    return labels;
}

bool SceneClustering::ClusterCheck(const std::vector<std::pair<int, int>> &edges,
  const SceneClustering::Cluster* cluster) {
  std::set<image_t> image_ids;
  for (auto& edge : edges) {
    if (!cluster->image_ids.count(edge.first) || !cluster->image_ids.count(edge.second)) {
      LOG(ERROR) << "edges peek is not in image_ids";
      return false;
    }
    image_ids.insert(edge.first);
    image_ids.insert(edge.second);
  }

  if (image_ids.size() != cluster->image_ids.size()) {
    LOG(ERROR) << "image_id num not equal edges peek num";
    return false;
  }
  return true;
}

int SceneClustering::DeleteOtherDomains(std::vector<std::pair<image_t, image_t>>& image_pairs,
  std::vector<int>& num_inliers) {
  std::vector<std::set<image_t>> connected_domains;
  ConnectedDomains(image_pairs, connected_domains);
  std::sort(connected_domains.begin(), connected_domains.end(), [](const std::set<image_t>&domain1, const std::set<image_t>&domain2) {
    return domain1.size() < domain2.size(); });

  std::set<image_t> image_ids;
  while (connected_domains.size() > 1) {
    image_ids.insert(connected_domains[0].begin(), connected_domains[0].end());
    connected_domains.erase(connected_domains.begin());
  }
  
  for (int i = 0; i < image_pairs.size(); ++i) {
    if (image_ids.count(image_pairs[i].first) || image_ids.count(image_pairs[i].second)) {
      image_pairs.erase(image_pairs.begin() + i);
      num_inliers.erase(num_inliers.begin() + i);
      --i;
    }
  }
  return 0;
}

template<class T>
void SceneClustering::ConnectedDomains(const std::vector<std::pair<T, T>>& edges, std::vector<std::set<T>>& connected_domains) {
  connected_domains.clear();
  for (const auto&edge : edges) {
    bool flag = false;
    for (auto&connected_domain : connected_domains) {
      if (connected_domain.count(edge.first) || connected_domain.count(edge.second)) {
        connected_domain.insert(edge.first);
        connected_domain.insert(edge.second);
        flag = true;
      }
    }

    if (flag) continue;
    connected_domains.push_back(std::set<T>());
    connected_domains.back().insert(edge.first);
    connected_domains.back().insert(edge.second);
  }

  int index = 0;
  while (index < connected_domains.size()) {
    bool flag = false;
    auto& connected_domain = connected_domains[index];
    for (int i = index + 1; i < connected_domains.size(); ++i) {
      std::vector<image_t> intersection;
      std::set_intersection(connected_domain.begin(), connected_domain.end(),
        connected_domains[i].begin(), connected_domains[i].end(), back_inserter(intersection));
      if (intersection.size()) {
        connected_domain.insert(connected_domains[i].begin(), connected_domains[i].end());
        connected_domains.erase(connected_domains.begin() + i);
        --i;
        flag = true;
      }
    }

    if (!flag) {
      ++index;
    }
  }
}

template int SceneClustering::CheckConnectedDomain<image_t>(const std::vector<std::pair<image_t, image_t>>&edges, bool PrintDomain);
template int SceneClustering::CheckConnectedDomain<int>(const std::vector<std::pair<int, int>>&edges, bool PrintDomain);

template void SceneClustering::ConnectedDomains(const std::vector<std::pair<image_t, image_t>>& edges, std::vector<std::set<image_t>>& connected_domains);
template void SceneClustering::ConnectedDomains(const std::vector<std::pair<int, int>>& edges, std::vector<std::set<int>>& connected_domains);
}  // namespace sfmrecon
