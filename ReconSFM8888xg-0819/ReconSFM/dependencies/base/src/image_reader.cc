#include "base/image_reader.h"

#include "base/camera_models.h"
#include "util/misc.h"

namespace sfmrecon {

bool ImageReader::LoadFocalLengthInfoFromTextFile(const std::string& focal_path) {
	FILE *mfile = fopen(focal_path.c_str(), "r");
	if (mfile == NULL)
		return false;
	focal_length_infos_.clear();
	FocalLengthInfo focal_length_init;
	while (!feof(mfile))
	{
		char group_name[512];
		fscanf(mfile, "%s\t%lf\t%d\n", group_name, &focal_length_init.focal_length, &focal_length_init.status);
		focal_length_init.group_name = group_name;
		if (focal_length_init.group_name.empty())
		{
			std::cout << "group name not correct!" << std::endl;
			return false;
		}
		focal_length_infos_[focal_length_init.group_name] = focal_length_init;
	}
	fclose(mfile);
	return !(focal_length_infos_.empty());
}

bool ImageReaderOptions::Check() const {
  CHECK_OPTION_GT(default_focal_length_factor, 0.0);
  CHECK_OPTION(ExistsCameraModelWithName(camera_model));
  const int model_id = CameraModelNameToId(camera_model);
  if (!camera_params.empty()) {
    CHECK_OPTION(
        CameraModelVerifyParams(model_id, CSVToVector<double>(camera_params)));
  }
  return true;
}

ImageReader::ImageReader(const ImageReaderOptions& options, Database* database)
    : options_(options), database_(database), image_index_(0) {
  CHECK(options_.Check());

  // Ensure trailing slash, so that we can build the correct image name.
  options_.image_path =
      EnsureTrailingSlash(StringReplace(options_.image_path, "\\", "/"));

  // Get a list of all files in the image path, sorted by image name.
  if (options_.image_list.empty()) {
    options_.image_list = GetRecursiveFileList(options_.image_path);
    std::sort(options_.image_list.begin(), options_.image_list.end());
  } else {
    for (auto& image_name : options_.image_list) {
      image_name = JoinPaths(options_.image_path, image_name);
    }
  }

  if (static_cast<camera_t>(options_.existing_camera_id) != kInvalidCameraId) {
    CHECK(database->ExistsCamera(options_.existing_camera_id));
    prev_camera_ = database->ReadCamera(options_.existing_camera_id);
  } else {
    // Set the manually specified camera parameters.
    prev_camera_.SetCameraId(kInvalidCameraId);
    prev_camera_.SetModelIdFromName(options_.camera_model);
    if (!options_.camera_params.empty()) {
      prev_camera_.SetParamsFromString(options_.camera_params);
      prev_camera_.SetPriorFocalLength(true);
    }
  }
  if (ExistsFile(options_.focal_length_file)) {
	  if (LoadFocalLengthInfoFromTextFile(options_.focal_length_file)) {
		  std::cout << "Load Focal File Succesed! " << std::endl;
	  }
	  else
		  std::cout << "Load Focal File Failed! " << std::endl;
  }
}

ImageReader::Status ImageReader::Next(Camera* camera, Image* image,
                                      Bitmap* bitmap, Bitmap* mask) {
  CHECK_NOTNULL(camera);
  CHECK_NOTNULL(image);
  CHECK_NOTNULL(bitmap);

  image_index_ += 1;
  CHECK_LE(image_index_, options_.image_list.size());

  std::string image_path = StringReplace(options_.image_list.at(image_index_ - 1), "\\", "/");
  image_path = image_path.substr(options_.image_path.size());
  DatabaseTransaction database_transaction(database_);

  //////////////////////////////////////////////////////////////////////////////
  // Set the image name.
  //////////////////////////////////////////////////////////////////////////////

  image->SetName(image_path);
  const std::string image_folder = GetParentDir(image->Name());

  //////////////////////////////////////////////////////////////////////////////
  // Check if image already read.
  //////////////////////////////////////////////////////////////////////////////

  const bool exists_image = database_->ExistsImageWithName(image->Name());

  if (exists_image) {
    *image = database_->ReadImageWithName(image->Name());
    const bool exists_keypoints = database_->ExistsKeypoints(image->ImageId());
    const bool exists_descriptors =
        database_->ExistsDescriptors(image->ImageId());

    if (exists_keypoints && exists_descriptors) {
      return Status::IMAGE_EXISTS;
    }
  }

  //////////////////////////////////////////////////////////////////////////////
  // Read image.
  //////////////////////////////////////////////////////////////////////////////

  if (!bitmap->Read(image_path, false)) {
    return Status::BITMAP_ERROR;
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Read mask.
  //////////////////////////////////////////////////////////////////////////////

  if (mask && !options_.mask_path.empty()) {
    const std::string mask_path =
        JoinPaths(options_.mask_path,
                  GetRelativePath(options_.image_path, image_path) + ".png");
    if (ExistsFile(mask_path) && !mask->Read(mask_path, false)) {
      // NOTE: Maybe introduce a separate error type MASK_ERROR?
      return Status::BITMAP_ERROR;
    }
  }

  //////////////////////////////////////////////////////////////////////////////
  // Check for well-formed data.
  //////////////////////////////////////////////////////////////////////////////

  if (exists_image) {
    const Camera camera = database_->ReadCamera(image->CameraId());

    if (options_.single_camera && prev_camera_.CameraId() != kInvalidCameraId &&
        (camera.Width() != prev_camera_.Width() ||
         camera.Height() != prev_camera_.Height())) {
      return Status::CAMERA_SINGLE_DIM_ERROR;
    }

    if (static_cast<size_t>(bitmap->Width()) != camera.Width() ||
        static_cast<size_t>(bitmap->Height()) != camera.Height()) {
      return Status::CAMERA_EXIST_DIM_ERROR;
    }
  }

  //////////////////////////////////////////////////////////////////////////////
  // Check image dimensions.
  //////////////////////////////////////////////////////////////////////////////

 /* if (prev_camera_.CameraId() != kInvalidCameraId &&
      ((options_.single_camera && !options_.single_camera_per_folder) ||
       (options_.single_camera_per_folder &&
        image_folder == prev_image_folder_)) &&
      (prev_camera_.Width() != static_cast<size_t>(bitmap->Width()) ||
       prev_camera_.Height() != static_cast<size_t>(bitmap->Height()))) {
    return Status::CAMERA_SINGLE_DIM_ERROR;
  }*/

  //////////////////////////////////////////////////////////////////////////////
  // Extract camera model and focal length
  //////////////////////////////////////////////////////////////////////////////

  if (prev_camera_.CameraId() == kInvalidCameraId ||
      (!options_.single_camera && !options_.single_camera_per_folder &&
       static_cast<camera_t>(options_.existing_camera_id) ==
           kInvalidCameraId) ||
      (options_.single_camera_per_folder )) {
    if (options_.camera_params.empty()) {
      // Extract focal length.
      double focal_length = 0.0;
      if (bitmap->ExifFocalLength(&focal_length)) {
        prev_camera_.SetPriorFocalLength(true);
      } else {
        focal_length = options_.default_focal_length_factor *
                       std::max(bitmap->Width(), bitmap->Height());
        prev_camera_.SetPriorFocalLength(false);
      }

      prev_camera_.InitializeWithId(prev_camera_.ModelId(), focal_length,
                                    bitmap->Width(), bitmap->Height());
    }

    prev_camera_.SetWidth(static_cast<size_t>(bitmap->Width()));
    prev_camera_.SetHeight(static_cast<size_t>(bitmap->Height()));

    if (!prev_camera_.VerifyParams()) {
      return Status::CAMERA_PARAM_ERROR;
    }

    prev_camera_.SetCameraId(database_->WriteCamera(prev_camera_));
  }

  image->SetCameraId(prev_camera_.CameraId());

  //////////////////////////////////////////////////////////////////////////////
  // Extract GPS data.
  //////////////////////////////////////////////////////////////////////////////

  if (!bitmap->ExifLatitude(&image->TvecPrior(0)) ||
      !bitmap->ExifLongitude(&image->TvecPrior(1)) ||
      !bitmap->ExifAltitude(&image->TvecPrior(2))) {
    image->TvecPrior().setConstant(std::numeric_limits<double>::quiet_NaN());
  }

  *camera = prev_camera_;

  /*image_folders_.insert(image_folder);
  prev_image_folder_ = image_folder;*/

  return Status::SUCCESS;
}

ImageReader::Status ImageReader::Next(Camera* camera, Image* image,
	Bitmap* bitmap, const Eigen::Vector3d &xyz) {
	CHECK_NOTNULL(camera);
	CHECK_NOTNULL(image);
	CHECK_NOTNULL(bitmap);

	CHECK_LT(image_index_, options_.image_list.size());
	const std::string image_path = options_.image_list.at(image_index_);
	image_index_ += 1;
	DatabaseTransaction database_transaction(database_);

	//////////////////////////////////////////////////////////////////////////////
	// Set the image name.
	//////////////////////////////////////////////////////////////////////////////

	image->SetName(image_path);
	image->SetName(StringReplace(image->Name(), "\\", "/"));
	image->SetName(
		image->Name().substr(options_.image_path.size(),
			image->Name().size() - options_.image_path.size()));

	std::string folder_name = GetParentDir(image->Name());

	//////////////////////////////////////////////////////////////////////////////
	// Check if image already read.
	//////////////////////////////////////////////////////////////////////////////

	const bool exists_image = database_->ExistsImageWithName(image->Name());

	if (exists_image) {
		*image = database_->ReadImageWithName(image->Name());
		if (options_.single_camera_per_folder) {
			if (prev_cameras_.find(folder_name) == prev_cameras_.end()) {
				const Camera camera = database_->ReadCamera(image->CameraId());
				prev_cameras_[folder_name] = camera;
			}	
		} else {
			const Camera camera = database_->ReadCamera(image->CameraId());
			prev_camera_ = camera;
		}
		
		const bool exists_keypoints = database_->ExistsKeypoints(image->ImageId());
		const bool exists_descriptors =
			database_->ExistsDescriptors(image->ImageId());

		if (exists_keypoints && exists_descriptors) {
			return Status::IMAGE_EXISTS;
		}
	}

	//////////////////////////////////////////////////////////////////////////////
	// Read image.
	//////////////////////////////////////////////////////////////////////////////

	if (!bitmap->Read(image_path, false)) {
		return Status::BITMAP_ERROR;
	}

	//////////////////////////////////////////////////////////////////////////////
	// Check for well-formed data.
	//////////////////////////////////////////////////////////////////////////////

	if (exists_image) {
		const Camera camera = database_->ReadCamera(image->CameraId());

		if (options_.single_camera && prev_camera_.CameraId() != kInvalidCameraId &&
			(camera.Width() != prev_camera_.Width() ||
				camera.Height() != prev_camera_.Height())) {
			return Status::CAMERA_SINGLE_DIM_ERROR;
		}

		if (static_cast<size_t>(bitmap->Width()) != camera.Width() ||
			static_cast<size_t>(bitmap->Height()) != camera.Height()) {
			return Status::CAMERA_EXIST_DIM_ERROR;
		}
	}

	//////////////////////////////////////////////////////////////////////////////
	// Check image dimensions.
	//////////////////////////////////////////////////////////////////////////////

	/*if (prev_camera_.CameraId() != kInvalidCameraId &&
		((options_.single_camera && !options_.single_camera_per_folder) ||
		(options_.single_camera_per_folder &&
			image_folder == prev_image_folder_)) &&
			(prev_camera_.Width() != static_cast<size_t>(bitmap->Width()) ||
				prev_camera_.Height() != static_cast<size_t>(bitmap->Height()))) {
		return Status::CAMERA_SINGLE_DIM_ERROR;
	}*/

	auto GetFocalLength = [bitmap, this](Camera & camera) {
		double focal_length = 0.0;
		if (bitmap->ExifFocalLength(&focal_length)) {
			camera.SetPriorFocalLength(true);
		}
		else {
			focal_length = this->options_.default_focal_length_factor *
				std::max(bitmap->Width(), bitmap->Height());
			camera.SetPriorFocalLength(false);
		}
		return focal_length;
	};
	//////////////////////////////////////////////////////////////////////////////
	// Extract camera model and focal length
	//////////////////////////////////////////////////////////////////////////////

	if (options_.single_camera_per_folder) {
		if (prev_cameras_.find(folder_name) == prev_cameras_.end()) {
			double focal_length = GetFocalLength(prev_cameras_[folder_name]);
			// share focal length
			if (focal_length_infos_.find(folder_name) != focal_length_infos_.end() && focal_length_infos_[folder_name].status) {
				focal_length = focal_length_infos_[folder_name].focal_length;
			}

			prev_cameras_[folder_name].InitializeWithId(prev_camera_.ModelId(), focal_length,
				bitmap->Width(), bitmap->Height());

			if (!prev_cameras_[folder_name].VerifyParams()) {
				return Status::CAMERA_PARAM_ERROR;
			}

			prev_cameras_[folder_name].SetCameraId(database_->WriteCamera(prev_cameras_[folder_name]));
		}
		
	} else if(prev_camera_.CameraId()== kInvalidCameraId || prev_camera_.CameraId()!=image->CameraId()){
		double focal_length = GetFocalLength(prev_camera_);
		prev_camera_.InitializeWithId(prev_camera_.ModelId(), focal_length,
			bitmap->Width(), bitmap->Height());

		if (!prev_camera_.VerifyParams()) {
			return Status::CAMERA_PARAM_ERROR;
		}

		prev_camera_.SetCameraId(database_->WriteCamera(prev_camera_));
	}

	if (options_.single_camera_per_folder) {
		image->SetCameraId(prev_cameras_[folder_name].CameraId());
		*camera = prev_cameras_[folder_name];
	} else {
		image->SetCameraId(prev_camera_.CameraId());
		*camera = prev_camera_;
	}

	//////////////////////////////////////////////////////////////////////////////
	// Extract GPS data.
	//////////////////////////////////////////////////////////////////////////////
	if (xyz != Eigen::Vector3d(0, 0, 0)) {
		image->SetTvecPrior(xyz);
	}
	return Status::SUCCESS;
}

size_t ImageReader::NextIndex() const { return image_index_; }

size_t ImageReader::NumImages() const { return options_.image_list.size(); }

std::string ImageReader::NextImageName() {
	return GetPathBaseName(options_.image_list.at(image_index_));
}

void ImageReader::SkipNextIndex() { ++image_index_; }

}  // namespace sfmrecon
