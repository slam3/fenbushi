#include <base/point_cloud_filter.h>
#include <util/timer.h>

namespace sfmrecon {
  void PointCloudFilter::Filter(Reconstruction * reconstruction) {
    auto& PointConvert = [](PointT& point, const Point3D& point3d) {
      point.x = point3d.X();
      point.y = point3d.Y();
      point.z = point3d.Z();
      point.r = point3d.Color()(0);
      point.g = point3d.Color()(1);
      point.b = point3d.Color()(2); 
    };

    std::cout << "==========begin filter point cloud=========" << std::endl;
    std::cout << "before filter point3d num: " << reconstruction->NumPoints3D() << std::endl;
    PointCloud::Ptr cloud(new PointCloud);

    const EIGEN_STL_UMAP(point3D_t, class Point3D) &points3d = reconstruction->Points3D();
    size_t num_locations = 0;
    Eigen::Matrix<float, Eigen::Dynamic, 3, Eigen::RowMajor> location_matrix(
      points3d.size(), 3);
    std::vector<size_t> location_idxs;
    location_idxs.reserve(points3d.size());
    for (auto& point3d : points3d) {
      PointT point;
      PointConvert(point, point3d.second);
      cloud->points.emplace_back(point);

      location_idxs.push_back(point3d.first);
      location_matrix.row(num_locations++) = Eigen::Vector3f(point.x, point.y, point.z);
    }

    cloud->height = 1;
    cloud->width = cloud->points.size();
    cloud->is_dense = false;

    PointCloud::Ptr filter_cloud = SORFilter(cloud);
    std::cout << "after sor filter point3d num: " << filter_cloud->points.size() << std::endl;
#if 0
    pcl::PCDWriter writer;
    writer.write<PointT>(std::string("./src.pcd"), *cloud);
    writer.write<PointT>(std::string("./sor_filter.pcd"), *filter_cloud);
#endif
    filter_cloud = VoxelGridFilter(filter_cloud);
    std::cout << "after vg filter point3d num: " << filter_cloud->points.size() << std::endl;
    //build trained matrix
    flann::Matrix<float> locations(location_matrix.data(), num_locations,
      location_matrix.cols());

    //build query matrix
    Eigen::Matrix<float, Eigen::Dynamic, 3, Eigen::RowMajor> query_matrix(
      filter_cloud->points.size(), 3);
    num_locations = 0;
    for (auto& point : filter_cloud->points) {
      query_matrix.row(num_locations++) = Eigen::Vector3f(point.x, point.y, point.z);
      /*if (num_locations > 10000) {
        break;
      }*/
    }
    flann::Matrix<float> queries(query_matrix.data(), num_locations,
      query_matrix.cols());

    //build search index
    flann::KDTreeIndexParams index_params(16);
    //index_params["algorithm"] = flann::FLANN_INDEX_KDTREE;
    //flann::LinearIndex<flann::L2<float>> search_index(index_params);
    index_params.erase("trees");
    flann::KDTreeIndex<flann::L2<float>> search_index(index_params);
    search_index.buildIndex(locations);

    flann::SearchParams search_params;
    search_params.cores = std::thread::hardware_concurrency();

    int knn = 1;
    Eigen::Matrix<size_t, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>
      index_matrix(query_matrix.rows(), knn);
    flann::Matrix<size_t> indices(index_matrix.data(), query_matrix.rows(), knn);

    Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>
      distance_matrix(query_matrix.rows(), knn);
    flann::Matrix<float> distances(distance_matrix.data(), query_matrix.rows(), knn);

    std::cout << "==========begin knn search===============" << std::endl;
    Timer timer;
    timer.Start();
    search_index.knnSearch(queries, indices, distances, knn, search_params);
    std::cout << "search query num: " << query_matrix.rows() << " \tcost time: " << timer.ElapsedMinutes() << std::endl;
    std::cout << "==========end knn search===============" << std::endl;
    std::set<point3D_t> point3d_ids;
    for (int i = 0; i < index_matrix.rows(); ++i) {
      point3d_ids.insert(location_idxs[index_matrix(i, 0)]);
      float distance = distance_matrix(i, 0);
    }

    //filter point3d
    std::set<point3D_t> filter_point3d_ids;
    for (auto& point3d : points3d) {
      if (!point3d_ids.count(point3d.first)) {
        filter_point3d_ids.insert(point3d.first);
      }
    }

    std::cout << "filter point3d num: " << filter_point3d_ids.size() << std::endl;
    for (auto id : filter_point3d_ids) {
      reconstruction->DeletePoint3D(id);
    }
  }

  PointCloud::Ptr PointCloudFilter::SORFilter(const PointCloud::Ptr & cloud) {
    pcl::StatisticalOutlierRemoval<PointT> sor;
    sor.setInputCloud(cloud);
    sor.setMeanK(10);               
    sor.setStddevMulThresh(3.0);    

    PointCloud::Ptr cloud_filter(new PointCloud);
    sor.filter(*cloud_filter);

    return cloud_filter;
  }

  PointCloud::Ptr PointCloudFilter::VoxelGridFilter(const PointCloud::Ptr & cloud) {
    pcl::VoxelGrid<PointT> vg;
    vg.setInputCloud(cloud);
    vg.setLeafSize(3.f, 3.f, 1.f);

    PointCloud::Ptr cloud_filter(new PointCloud);
    vg.filter(*cloud_filter);

    return cloud_filter;
  }
}
