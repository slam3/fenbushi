#include <base/at_report.h>
#include <iostream>
namespace sfmrecon {
  void ATReport::Settings::Load(const pugi::xml_node& node) {
    if (!node) {
      return;
    }
    use_pos = node.child("use_pos").text().as_bool();
    key_point_density = node.child("key_point_density").text().as_int(); 
    std::string pair_mode = node.child("pair_select_mode").text().as_string();
    
    if (pair_mode == "exhaustive") {
      pair_select_mode = EXHAUSTIVE;
    }
    else if (pair_mode == "vocabulary") {
      pair_select_mode = VOCABULARY;
    }
    else if (pair_mode == "spatial") {
      pair_select_mode = SPATIAL;
    }
    
    estimate_position = node.child("estimate_position").text().as_bool();
    estimate_rotation = node.child("estimate_rotation").text().as_bool();
    estimate_tie_point = node.child("estimate_tie_point").text().as_bool();
    estimate_focal_length = node.child("estimate_focal_length").text().as_bool();
    estimate_principal_point = node.child("estimate_principal_point").text().as_bool();
    estimate_radial_distortion = node.child("estimate_radial_distortion").text().as_bool();
    
    std::string camera_model_name = node.child("camera_model").text().as_string();

    if (pair_mode == "SimplePinhole") {
      camera_model = SimplePinhole;
    }
    else if (pair_mode == "PinholeCamera") {
      camera_model = PinholeCamera;
    }
    else if (pair_mode == "SimpleRadial") {
      camera_model = SimpleRadial;
    }
    else if (pair_mode == "SimpleRadialFisheye") {
      camera_model = SimpleRadialFisheye;
    }
  }

  void ATReport::Settings::Save(pugi::xml_node& node) const {
    node.append_child("use_pos").append_child(pugi::node_pcdata).set_value(use_pos);
    node.append_child("key_point_density").append_child(pugi::node_pcdata).set_value(key_point_density);

    std::string pair_mode = "";
    switch (pair_select_mode) {
    case EXHAUSTIVE:
      pair_mode = "exhaustive";
      break;
    case VOCABULARY:
      pair_mode = "vocabulary";
      break;
    case SPATIAL:
      pair_mode = "spatial";
      break;
    default:
      pair_mode = "unknown";
    }
    node.append_child("pair_select_mode").append_child(pugi::node_pcdata).set_value(pair_mode);;
    
    node.append_child("estimate_position").append_child(pugi::node_pcdata).set_value(estimate_position);
    node.append_child("estimate_rotation").append_child(pugi::node_pcdata).set_value(estimate_rotation);
    node.append_child("estimate_tie_point").append_child(pugi::node_pcdata).set_value(estimate_tie_point);
    node.append_child("estimate_focal_length").append_child(pugi::node_pcdata).set_value(estimate_focal_length);
    node.append_child("estimate_principal_point").append_child(pugi::node_pcdata).set_value(estimate_principal_point);
    node.append_child("estimate_radial_distortion").append_child(pugi::node_pcdata).set_value(estimate_radial_distortion);
    
    std::string camera_model_name = "";
    switch (camera_model) {
    case SimplePinhole:
      camera_model_name = "SimplePinhole";
      break;
    case PinholeCamera:
      camera_model_name = "PinholeCamera";
      break;
    case SimpleRadial:
      camera_model_name = "SimpleRadial";
      break;
    case SimpleRadialFisheye:
      camera_model_name = "SimpleRadialFisheye";
      break;
    default:
      camera_model_name = "unknown";
      break;
    }
    
    node.append_child("camera_model").append_child(pugi::node_pcdata).set_value(camera_model_name);
  }

  void ATReport::TimeInfo::Load(const pugi::xml_node& node) {
    if (!node) {
      return;
    }
    start_time = node.child("start_time").text().as_llong();
    end_time = node.child("end_time").text().as_llong();
    cost_time = node.child("cost_time").text().as_string();
    if (node.child("total_cost_time_percent")) {
      total_cost_time_percent = node.child("total_cost_time_percent").text().as_float();
    }
  }

  void ATReport::TimeInfo::Save(pugi::xml_node& node) const {
    node.append_child("start_time").append_child(pugi::node_pcdata).set_value(start_time);
    node.append_child("end_time").append_child(pugi::node_pcdata).set_value(end_time);
    node.append_child("cost_time").append_child(pugi::node_pcdata).set_value(cost_time);
    if (total_cost_time_percent > 0) {
      node.append_child("total_cost_time_percent").append_child(pugi::node_pcdata).set_value(total_cost_time_percent);
    }
  }

  void ATReport::Results::Load(const pugi::xml_node& node) {
    if (!node) {
      return;
    }
    
    time_infos.Load(node.child("time_infos"));

    pugi::xml_node cluster = node.child("clusters").first_child();
    while (cluster) {
      ClusterInfo cluster_info;
      cluster_info.Load(cluster);
      clusters.emplace_back(cluster_info);
      cluster = cluster.next_sibling();
    }

    connections_total.Load(node.child("connections_total"));

    pugi::xml_node photo = node.child("Photos").first_child();
    while (photo) {
      PerPhoto photo_info;
      photo_info.Load(photo);
      photo_infos.emplace_back(photo_info);
      photo = photo.next_sibling();
    } 

    pugi::xml_node connection = node.child("Connections").first_child();
    while (connection) {
      Connection connect;
      connect.Load(connection);
      connections.emplace_back(connect);
      connection = connection.next_sibling();
    }

    if (node.child("PerPoint")) {
      per_point.Load(node.child("PerPoint"));
    }
  }

  void ATReport::Results::Save(pugi::xml_node& node) const {
    time_infos.Save(node.append_child("time_infos"));

    if (!total_cost_time.empty()) {
      node.append_child("total_cost_time").append_child(pugi::node_pcdata).set_value(total_cost_time);
    }

    if (!clusters.empty()) {
      pugi::xml_node clusters_node = node.append_child("clusters");
      for (auto& cluster : clusters) {
        cluster.Save(clusters_node.append_child("cluster"));
      }
    }

    if (connections_total.number_of_tested_pairs) {
      connections_total.Save(node.append_child("connections_total"));
    }
    
    if (photo_infos.size()) {
      pugi::xml_node photo_infos_node = node.append_child("Photos");
      for (auto& photo_info : photo_infos) {
        photo_info.Save(photo_infos_node.append_child("Photo"));
      }
    }
    
    if (connections.size()) {
      pugi::xml_node connections_node = node.append_child("Connections");
      for (auto& connection : connections) {
        connection.Save(connections_node.append_child("Connection"));
      }
    }
    
    if (per_point.num_of_points) {
      per_point.Save(node.append_child("PerPoint"));
    }
  }

  void ATReport::Results::UpdateCostPercent() {
    double total_time = 0;
    double feature_extract_time = 0;
    double feature_match_time = 0;
    double sparse_time = 0;
    double transform_time = 0;

    TimeInfo &feature_extract = time_infos.feature_extract;
    if (!feature_extract.cost_time.empty()) {
      feature_extract_time = feature_extract.end_time - feature_extract.start_time;
    }

    TimeInfo &feature_match = time_infos.feature_match;
    if (!feature_match.cost_time.empty()) {
      feature_match_time = feature_match.end_time - feature_match.start_time;
    }

    TimeInfo &sparse_reconstruction = time_infos.sparse_reconstruction;
    if(!sparse_reconstruction.cost_time.empty()) {
      sparse_time = sparse_reconstruction.end_time - sparse_reconstruction.start_time;
    }

    TimeInfo &transform_gps = time_infos.transform_gps;
    if (!transform_gps.cost_time.empty()) {
      transform_time = transform_gps.end_time - transform_gps.start_time;
    }

    total_time = feature_extract_time + feature_match_time + sparse_time + transform_time;
    feature_extract.total_cost_time_percent = feature_extract_time / total_time * 100;
    feature_match.total_cost_time_percent = feature_match_time / total_time * 100;
    sparse_reconstruction.total_cost_time_percent = sparse_time / total_time * 100;
    transform_gps.total_cost_time_percent = transform_time / total_time * 100;

    total_cost_time = std::to_string(total_time / 60) + " minute";
  }

  void ATReport::LoadFromXml(const std::string& xml_path) {
    pugi::xml_document doc;
    pugi::xml_parse_result result = doc.load_file(xml_path.c_str());

    settings.Load(doc.child("settings"));

    if (doc.child("results")) {
      results.Load(doc.child("results"));
    }
  }

  void ATReport::ExportXml(const std::string& xml_path) const {
    pugi::xml_document doc;
    pugi::xml_node decl = doc.prepend_child(pugi::node_declaration);
    decl.append_attribute("version") = "1.0";
    decl.append_attribute("encoding") = "utf-8";
    settings.Save(doc.append_child("settings"));
    results.Save(doc.append_child("results"));

    doc.save_file(xml_path.c_str());
  }

  void ATReport::PerPhoto::Load(const pugi::xml_node & node) {
    if (!node) {
      return;
    }
    image_name = node.child("image_name").text().as_string();
    num_of_key_points = node.child("num_of_key_points").text().as_int();
    num_of_points = node.child("num_of_points").text().as_int();
    RMS_of_reprojection_errors = node.child("RMS_of_reprojection_errors").text().as_float();
  }

  void ATReport::PerPhoto::Save(pugi::xml_node & node) const {
    node.append_child("image_name").append_child(pugi::node_pcdata).set_value(image_name);
    node.append_child("num_of_key_points").append_child(pugi::node_pcdata).set_value(num_of_key_points);
    node.append_child("num_of_points").append_child(pugi::node_pcdata).set_value(num_of_points);
    node.append_child("RMS_of_reprojection_errors").append_child(pugi::node_pcdata).set_value(RMS_of_reprojection_errors);
  }

  void ATReport::PerPoint::Load(const pugi::xml_node & node) {
    if (!node) {
      return;
    }
    num_of_image = node.child("num_of_image").text().as_int();
    num_of_regist_image = node.child("num_of_regist_image").text().as_int();
    median_number_of_key_points_per_photo = node.child("median_number_of_key_points_per_photo").text().as_int();
    num_of_points = node.child("num_of_points").text().as_int();
    median_number_of_photos_per_point = node.child("median_number_of_photos_per_point").text().as_int();
    median_number_of_points_per_photo = node.child("median_number_of_points_per_photo").text().as_int();
    RMS_of_reprojection_errors = node.child("RMS_of_reprojection_errors").text().as_float();
  }

  void ATReport::PerPoint::Save(pugi::xml_node & node) const {
    node.append_child("num_of_image").append_child(pugi::node_pcdata).set_value(num_of_image);
    node.append_child("num_of_regist_image").append_child(pugi::node_pcdata).set_value(num_of_regist_image);
    node.append_child("median_number_of_key_points_per_photo").append_child(pugi::node_pcdata).set_value(median_number_of_key_points_per_photo);
    node.append_child("num_of_points").append_child(pugi::node_pcdata).set_value(num_of_points);
    node.append_child("median_number_of_photos_per_point").append_child(pugi::node_pcdata).set_value(median_number_of_photos_per_point);
    node.append_child("median_number_of_points_per_photo").append_child(pugi::node_pcdata).set_value(median_number_of_points_per_photo);
    node.append_child("RMS_of_reprojection_errors").append_child(pugi::node_pcdata).set_value(RMS_of_reprojection_errors);
  }
  
  ATReport::ClusterInfo::ClusterInfo(int id, int image_num) :id(id), image_num(image_num) {
  }

  void ATReport::ClusterInfo::Load(const pugi::xml_node & node) {
    if (!node) {
      return;
    }
    id = node.child("id").text().as_int();
    image_num = node.child("image_num").text().as_int();
  }
  
  void ATReport::ClusterInfo::Save(pugi::xml_node & node) const {
    node.append_child("id").append_child(pugi::node_pcdata).set_value(id);
    node.append_child("image_num").append_child(pugi::node_pcdata).set_value(image_num);
  }

  void ATReport::Connection::Load(const pugi::xml_node & node) {
    if (!node) {
      return;
    }
    camera_id = node.child("camera_id").text().as_int();
    image_id = node.child("image_id").text().as_int();
    image_name = node.child("image_name").text().as_string();
    num_of_tested_pairs = node.child("num_of_tested_pairs").text().as_int();
    num_of_connected_pairs = node.child("num_of_connected_pairs").text().as_int();
  }

  void ATReport::Connection::Save(pugi::xml_node & node) const {
    node.append_child("camera_id").append_child(pugi::node_pcdata).set_value(camera_id);
    node.append_child("image_id").append_child(pugi::node_pcdata).set_value(image_id);
    node.append_child("image_name").append_child(pugi::node_pcdata).set_value(image_name);
    node.append_child("num_of_tested_pairs").append_child(pugi::node_pcdata).set_value(num_of_tested_pairs);
    node.append_child("num_of_connected_pairs").append_child(pugi::node_pcdata).set_value(num_of_connected_pairs);
  }

  void ATReport::Connections::Load(const pugi::xml_node & node) {
    if (!node) {
      return;
    }
    number_of_tested_pairs = node.child("number_of_tested_pairs").text().as_int();
    median_number_of_tested_pairs_per_photo = node.child("median_number_of_tested_pairs_per_photo").text().as_int();
    median_number_of_connected_photos_per_photo = node.child("median_number_of_connected_photos_per_photo").text().as_int();
  }

  void ATReport::Connections::Save(pugi::xml_node & node) const {
    node.append_child("number_of_tested_pairs").append_child(pugi::node_pcdata).set_value(number_of_tested_pairs);
    node.append_child("median_number_of_tested_pairs_per_photo").append_child(pugi::node_pcdata).set_value(median_number_of_tested_pairs_per_photo);
    node.append_child("median_number_of_connected_photos_per_photo").append_child(pugi::node_pcdata).set_value(median_number_of_connected_photos_per_photo);
  }

  void ATReport::TimeInfos::Load(const pugi::xml_node & node) {
    if (node.child("feature_extract")) {
      feature_extract.Load(node.child("feature_extract"));
    }

    if (node.child("feature_match")) {
      feature_match.Load(node.child("feature_match"));
    }

    if (node.child("sparse_reconstruction")) {
      sparse_reconstruction.Load(node.child("sparse_reconstruction"));
    }

    if (node.child("transform_gps")) {
      transform_gps.Load(node.child("transform_gps"));
    }
  }

  void ATReport::TimeInfos::Save(pugi::xml_node & node) const {
    if (!feature_extract.cost_time.empty()) {
      feature_extract.Save(node.append_child("feature_extract"));
    }

    if (!feature_match.cost_time.empty()) {
      feature_match.Save(node.append_child("feature_match"));
    }

    if (!sparse_reconstruction.cost_time.empty()) {
      sparse_reconstruction.Save(node.append_child("sparse_reconstruction"));
    }

    if (!transform_gps.cost_time.empty()) {
      transform_gps.Save(node.append_child("transform_gps"));
    }
  }
}
