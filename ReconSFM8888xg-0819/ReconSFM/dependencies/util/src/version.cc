#include "util/version.h"

namespace sfmrecon {

std::string GetVersionInfo() {
  return StringPrintf("COLMAP %d", 3.6);
}

std::string GetBuildInfo() {
#ifdef CUDA_ENABLED
  const std::string cuda_info = "with CUDA";
#else
  const std::string cuda_info = "without CUDA";
#endif
  return StringPrintf("Commit %d on %s", 3.6
                   , cuda_info.c_str());
}

}  // namespace sfmrecon
