#ifndef COLMAP_SRC_CONTROLLERS_BUNDLE_ADJUSTMENT_WITH_GCP_H_
#define COLMAP_SRC_CONTROLLERS_BUNDLE_ADJUSTMENT_WITH_GCP_H_
#include "util/threading.h"
#include "util/option_manager.h"
#include "base/reconstruction.h"
namespace sfmrecon {
class BundleAdjustmentWithGCPController : public Thread {
public:
	BundleAdjustmentWithGCPController(const OptionManager& options,
		Reconstruction* reconstruction);

	void Run();
	bool AdjustGlobalBundle();

private:
	const OptionManager options_;
	Reconstruction* reconstruction_;
};
}

#endif
