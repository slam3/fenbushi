#pragma once

#include "base/scene_clustering.h"
#include "util/threading.h"
#include "util/string.h"

namespace sfmrecon {
	class ClusterMapperController : public Thread
	{
	public:
		struct Options {
			// The path to the image folder which are used as input.
			std::string image_path;

			// The path to the database file which is used as input.
			std::string database_path;

			//The path to write output.
			std::string workspace_path;

		};

		ClusterMapperController(
			const Options& options,
			const SceneClustering::Options& clustering_options);
		~ClusterMapperController();
		int getClusterNumber();
		std::string getClusterNames();
		std::ofstream* file;
private:
		void Run() override;
		bool writeClusterToFile(std::string file, const SceneClustering::Cluster& cluster, int leafNum);

		const Options options_;
		const SceneClustering::Options clustering_options_;
		int mClusterNum;
		std::string mClusterNames;

	};
}

