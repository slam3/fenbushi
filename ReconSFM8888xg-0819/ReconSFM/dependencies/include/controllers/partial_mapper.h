#pragma once

#include "base/reconstruction_manager.h"
#include "base/scene_clustering.h"
#include "controllers/incremental_mapper.h"
#include "util/threading.h"
#include "util/option_manager.h"

namespace sfmrecon {
	class PartialMapperController : public Thread
	{
	public:
		enum {
			PARTIAL_FINISHED_CALLBACK,
		};
		struct Options {
			// The path to the image folder which are used as input.
			std::string image_path;

			// The path to the database file which is used as input.
			std::string database_path;

			// The maximum number of trials to initialize a cluster.
			int init_num_trials = 10;

			// The cluster define file.
			std::string cluste_path;

			// The job path file.
			std::string job_path;

			// The cluster index.
			int current_index;

			// The number of workers used to reconstruct clusters in parallel.
			int num_workers = -1;

			bool Check() const;
		};

		PartialMapperController(
			const Options& options,
			const IncrementalMapperOptions& mapper_options,
			ReconstructionManager* reconstruction_manager,
			OptionManager option_manager_);
		~PartialMapperController();
		std::ofstream* m_file;
	private:
		void Run() override;
    void Stop() override;
		bool writeReconstrctionToFile(ReconstructionManager &manage);

		const Options options_;
		IncrementalMapperOptions mapper_options_;
		ReconstructionManager* reconstruction_manager_;
		OptionManager option_manager_;

    std::shared_ptr<IncrementalMapperController> mapper_;

	};
}

