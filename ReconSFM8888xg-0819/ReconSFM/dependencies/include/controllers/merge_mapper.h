#pragma once
#include "base/reconstruction_manager.h"
#include "util/threading.h"
#include "base/scene_clustering.h"
#include "util/option_manager.h"

namespace sfmrecon {
	class MergeMapperController : public Thread
	{
	public:
		struct Options {

			// The path to workspace.
			std::string workspace;

			std::string imagePath;
			std::string localoutput;

			// the number of cluster.
			int clusterNum = 2;

			// The maximum number of trials to initialize a cluster.
			int init_num_trials = 10;

			// The number of workers used to reconstruct clusters in parallel.
			int num_workers = -1;

			bool Check() const;
		};
		MergeMapperController(
			const Options& options,
			const SceneClustering::Options& clustering_options,
			OptionManager option_manager_);
		~MergeMapperController();
		std::ofstream *jobfile;
	private:
		std::unordered_map<const SceneClustering::Cluster*, ReconstructionManager> mReconstruction_managers;
		void Run() override;
		bool readClusterFromFile(
			std::string file, 
			SceneClustering::Cluster &sceneCluster);

		const Options options_;
		const SceneClustering::Options clustering_options_;
		OptionManager option_manager_;

	};
}

