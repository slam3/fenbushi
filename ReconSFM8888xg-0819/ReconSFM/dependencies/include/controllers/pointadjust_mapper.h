#pragma once
#include "base/reconstruction_manager.h"
#include "util/threading.h"
#include "util/option_manager.h"
#include "base/undistortion.h"
#include "base/gps.h"
#include <QDir>
#include <QDebug>
#include <QString>
#include "feature/extraction.h"
#include "base/gps.h"

namespace sfmrecon {
	class PointAdjustController : public Thread
	{
	public:
		struct Options {
			std::string workspace;
			std::string gps_file_path;
			std::string database;
			// The path to manage file.
			std::string manageFile;
			std::string localoutput;
			std::string imagePath;

			bool Check() const;
		};
		PointAdjustController(
			const Options& options,
			OptionManager& option_manager_);
		~PointAdjustController();
		std::ofstream* m_file;
	private:
		void Run() override;
		bool RunGeoPos(Reconstruction &reconstruction);

		const Options options_;
		OptionManager option_manager_;

	};
}

