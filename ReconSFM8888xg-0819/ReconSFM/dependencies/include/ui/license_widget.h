#ifndef COLMAP_SRC_UI_LICENSE_WIDGET_H_
#define COLMAP_SRC_UI_LICENSE_WIDGET_H_

#include <QtWidgets>

namespace sfmrecon {

class LicenseWidget : public QTextEdit {
 public:
  explicit LicenseWidget(QWidget* parent);

 private:
  QString GetCOLMAPLicense() const;
  QString GetFLANNLicense() const;
  QString GetGraclusLicense() const;
  QString GetLSDLicense() const;
  QString GetPBALicense() const;
  QString GetPoissonReconLicense() const;
  QString GetSiftGPULicense() const;
  QString GetSQLiteLicense() const;
  QString GetVLFeatLicense() const;
};

} //

#endif  // COLMAP_SRC_UI_LICENSE_WIDGET_H_
