#ifndef COLMAP_SRC_UI_FEATURE_MATCHING_WIDGET_H_
#define COLMAP_SRC_UI_FEATURE_MATCHING_WIDGET_H_

#include <QtCore>
#include <QtWidgets>

#include "util/misc.h"
#include "util/option_manager.h"

namespace sfmrecon {

class FeatureMatchingWidget : public QWidget {
 public:
  FeatureMatchingWidget(QWidget* parent, OptionManager* options);

 private:
  void showEvent(QShowEvent* event);
  void hideEvent(QHideEvent* event);

  QWidget* parent_;
  QTabWidget* tab_widget_;
};

} //

#endif  // COLMAP_SRC_UI_FEATURE_MATCHING_WIDGET_H_
