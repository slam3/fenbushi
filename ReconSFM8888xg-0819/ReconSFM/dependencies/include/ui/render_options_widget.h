#ifndef COLMAP_SRC_UI_RENDER_OPTIONS_WIDGET_H_
#define COLMAP_SRC_UI_RENDER_OPTIONS_WIDGET_H_

#include <QtCore>
#include <QtWidgets>

#include "sfm/incremental_mapper.h"
#include "ui/model_viewer_widget.h"
#include "ui/options_widget.h"

namespace sfmrecon {

class RenderOptionsWidget : public OptionsWidget {
 public:
  RenderOptionsWidget(QWidget* parent, OptionManager* options,
                      ModelViewerWidget* model_viewer_widget);

  size_t counter;
  bool automatic_update;

  QAction* action_render_now;

 private:
  void closeEvent(QCloseEvent* event);

  void Apply();
  void ApplyProjection();
  void ApplyColormap();
  void ApplyBackgroundColor();

  void SelectBackgroundColor();

  void IncreasePointSize();
  void DecreasePointSize();
  void IncreaseCameraSize();
  void DecreaseCameraSize();

  OptionManager* options_;
  ModelViewerWidget* model_viewer_widget_;

  QComboBox* projection_cb_;
  QComboBox* point3D_colormap_cb_;
  double point3D_colormap_scale_;
  double point3D_colormap_min_q_;
  double point3D_colormap_max_q_;
  QDoubleSpinBox* bg_red_spinbox_;
  QDoubleSpinBox* bg_green_spinbox_;
  QDoubleSpinBox* bg_blue_spinbox_;
  double bg_color_[3];
};

} 

#endif  
