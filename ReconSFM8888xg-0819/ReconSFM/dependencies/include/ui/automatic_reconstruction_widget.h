//#ifndef COLMAP_SRC_UI_AUTOMATIC_RECONSTRUCTION_WIDGET_H_
//#define COLMAP_SRC_UI_AUTOMATIC_RECONSTRUCTION_WIDGET_H_
//
//#include "controllers/automatic_reconstruction.h"
//#include "ui/options_widget.h"
//#include "ui/thread_control_widget.h"
//
//namespace sfmrecon {
//
//class MainWindow;
//
//class AutomaticReconstructionWidget : public OptionsWidget {
// public:
//  AutomaticReconstructionWidget(MainWindow* main_window);
//
//  void Run();
//
// private:
//  void RenderResult();
//
//  MainWindow* main_window_;
//  SparseReconstructionController::Options options_;
//  ThreadControlWidget* thread_control_widget_;
//  QComboBox* data_type_cb_;
//  QComboBox* quality_cb_;
//  QAction* render_result_;
//};
//
//} //
//
//#endif  // COLMAP_SRC_UI_AUTOMATIC_RECONSTRUCTION_WIDGET_H_
