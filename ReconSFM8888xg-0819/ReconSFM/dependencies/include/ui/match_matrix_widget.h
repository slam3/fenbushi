#ifndef COLMAP_SRC_UI_MATCH_MATRIX_WIDGET_H_
#define COLMAP_SRC_UI_MATCH_MATRIX_WIDGET_H_

#include "ui/image_viewer_widget.h"
#include "util/option_manager.h"

namespace sfmrecon {

// Widget to visualize match matrix.
class MatchMatrixWidget : public ImageViewerWidget {
 public:
  MatchMatrixWidget(QWidget* parent, OptionManager* options);

  void Show();

 private:
  OptionManager* options_;
};

} //

#endif  // COLMAP_SRC_UI_MATCH_MATRIX_WIDGET_H_
