#ifndef COLMAP_SRC_UI_UNDISTORTION_WIDGET_H_
#define COLMAP_SRC_UI_UNDISTORTION_WIDGET_H_

#include <QtCore>
#include <QtWidgets>

#include "base/reconstruction.h"
#include "base/undistortion.h"
#include "ui/options_widget.h"
#include "ui/thread_control_widget.h"
#include "util/misc.h"
#include "util/option_manager.h"

namespace sfmrecon {

class UndistortionWidget : public OptionsWidget {
 public:
  UndistortionWidget(QWidget* parent, const OptionManager* options);

  void Show(const Reconstruction& reconstruction);
  bool IsValid() const;

 private:
  void Undistort();

  const OptionManager* options_;
  const Reconstruction* reconstruction_;

  ThreadControlWidget* thread_control_widget_;

  QComboBox* output_format_;
  UndistortCameraOptions undistortion_options_;
  std::string output_path_;
};

}  

#endif  
