#ifndef COLMAP_SRC_UI_RECONSTRUCTION_OPTIONS_WIDGET_H_
#define COLMAP_SRC_UI_RECONSTRUCTION_OPTIONS_WIDGET_H_

#include <QtCore>
#include <QtWidgets>

#include "ui/options_widget.h"
#include "util/option_manager.h"

namespace sfmrecon {

class MapperGeneralOptionsWidget : public OptionsWidget {
 public:
  MapperGeneralOptionsWidget(QWidget* parent, OptionManager* options);
};

class MapperTriangulationOptionsWidget : public OptionsWidget {
 public:
  MapperTriangulationOptionsWidget(QWidget* parent, OptionManager* options);
};

class MapperRegistrationOptionsWidget : public OptionsWidget {
 public:
  MapperRegistrationOptionsWidget(QWidget* parent, OptionManager* options);
};

class MapperInitializationOptionsWidget : public OptionsWidget {
 public:
  MapperInitializationOptionsWidget(QWidget* parent, OptionManager* options);
};

class MapperBundleAdjustmentOptionsWidget : public OptionsWidget {
 public:
  MapperBundleAdjustmentOptionsWidget(QWidget* parent, OptionManager* options);
};

class MapperFilteringOptionsWidget : public OptionsWidget {
 public:
  MapperFilteringOptionsWidget(QWidget* parent, OptionManager* options);
};

class ReconstructionOptionsWidget : public QWidget {
 public:
  ReconstructionOptionsWidget(QWidget* parent, OptionManager* options);
};

} //

#endif  // COLMAP_SRC_UI_RECONSTRUCTION_OPTIONS_WIDGET_H_
