#ifndef COLMAP_SRC_UI_BUNDLE_ADJUSTMENT_WIDGET_H_
#define COLMAP_SRC_UI_BUNDLE_ADJUSTMENT_WIDGET_H_

#include <QtCore>
#include <QtWidgets>

#include "base/reconstruction.h"
#include "ui/options_widget.h"
#include "ui/thread_control_widget.h"
#include "util/option_manager.h"

namespace sfmrecon {

class MainWindow;

class BundleAdjustmentWidget : public OptionsWidget {
 public:
  BundleAdjustmentWidget(MainWindow* main_window, OptionManager* options);

  void Show(Reconstruction* reconstruction);

 private:
  void Run();
  void Render();

  MainWindow* main_window_;
  OptionManager* options_;
  Reconstruction* reconstruction_;
  ThreadControlWidget* thread_control_widget_;
  QAction* render_action_;
};

} //

#endif  // COLMAP_SRC_UI_BUNDLE_ADJUSTMENT_WIDGET_H_
