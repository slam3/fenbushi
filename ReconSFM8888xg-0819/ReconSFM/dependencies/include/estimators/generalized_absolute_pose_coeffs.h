#ifndef COLMAP_SRC_ESTIMATORS_GENERALIZED_ABSOLUTE_POSE_COEFFS_H_
#define COLMAP_SRC_ESTIMATORS_GENERALIZED_ABSOLUTE_POSE_COEFFS_H_

#include <Eigen/Core>

namespace sfmrecon{

Eigen::Matrix<double, 9, 1> ComputeDepthsSylvesterCoeffs(
    const Eigen::Matrix<double, 3, 6>& K);

}  // namespace colmap

#endif  // COLMAP_SRC_ESTIMATORS_GENERALIZED_ABSOLUTE_POSE_COEFFS_H_
