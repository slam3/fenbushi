#ifndef COLMAP_SRC_BASE_SCENE_CLUSTERING_H_
#define COLMAP_SRC_BASE_SCENE_CLUSTERING_H_

#include <list>
#include <vector>
#include <set>
#include <unordered_map>

#include "util/types.h"

namespace sfmrecon{

// Scene clustering approach using normalized cuts on the scene graph. The scene
// is hierarchically partitioned into overlapping clusters until a maximum
// number of images is in a leaf node.
class SceneClustering {
 public:
  struct Options {
    // The branching factor of the hierarchical clustering.
    int branching = 2;

    // The number of overlapping images between child clusters.
    int image_overlap = 50;

    // The maximum number of images in a leaf node cluster, otherwise the
    // cluster is further partitioned using the given branching factor. Note
    // that a cluster leaf node will have at most `leaf_max_num_images +
    // overlap` images to satisfy the overlap constraint.
    int leaf_max_num_images = 500;

    bool Check() const;
  };

  struct Cluster {
    std::set<image_t> image_ids;
    std::vector<Cluster> child_clusters;
  };

  SceneClustering(const Options& options);

  void Partition(const std::vector<std::pair<image_t, image_t>>& image_pairs,
                 const std::vector<int>& num_inliers);

  const Cluster* GetRootCluster() const;
  std::vector<const Cluster*> GetLeafClusters() const;
  template<class T>
  static int CheckConnectedDomain(const std::vector<std::pair<T, T>>& edges, bool PrintDomain = false);

  std::unordered_map<int, int> ComputeNormalizedMinGraphCutModified(
      const std::vector<std::pair<int, int>>& edges,
      const std::vector<int>& weights, const int num_parts, Cluster *cluster,
      int image_overlap_threshold);

  void MergeConnectedDomainsPhase1(
      const std::vector<std::pair<int, int>>& edges,
      const std::vector<int>& weights, const int num_parts, Cluster *cluster,
      std::unordered_map<int, int> &labels);
  void MergeConnectedDomainsPhase2(
      const std::vector<std::pair<int, int>>& edges,
      const std::vector<int>& weights, const int num_parts, Cluster *cluster,
      std::unordered_map<int, int> &labels,
      int image_overlap_threshold);

  static bool ClusterCheck(const std::vector<std::pair<int, int>> &edges,
    const Cluster* cluster);

  //if image_pairs has more one domain ,delete less domain, save has most images domain
  static int DeleteOtherDomains(std::vector<std::pair<image_t, image_t>>& image_pairs, std::vector<int>& num_inliers);
  template<class T>
  static void ConnectedDomains(const std::vector<std::pair<T, T>>& edges, std::vector<std::set<T>>& connected_domains);

 private:
  void PartitionCluster(const std::vector<std::pair<int, int>>& edges,
                        const std::vector<int>& weights, Cluster* cluster);

  const Options options_;
  std::unique_ptr<Cluster> root_cluster_;
};

}  // namespace colmap

#endif  // COLMAP_SRC_BASE_SCENE_CLUSTERING_H_
