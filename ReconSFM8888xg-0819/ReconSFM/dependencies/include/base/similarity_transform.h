
#ifndef COLMAP_SRC_BASE_SIMILARITY_TRANSFORM_H_
#define COLMAP_SRC_BASE_SIMILARITY_TRANSFORM_H_

#include <vector>

#include <Eigen/Core>
#include <Eigen/Geometry>
#include <Eigen/Dense>
#include <Eigen/SparseCore>
#include <Eigen/StdVector>
#include <initializer_list>
#include <memory>
#include <vector>

#include "util/alignment.h"
#include "util/types.h"

namespace sfmrecon{

struct RANSACOptions;
class Reconstruction;
using EigenDoubleTraits = Eigen::NumTraits<double>;

/// 3d vector using double internal format
using Vec3 = Eigen::Vector3d;

/// 2d vector using int internal format
using Vec2i = Eigen::Vector2i;

/// 2d vector using float internal format
using Vec2f = Eigen::Vector2f;

/// 3d vector using float internal format
using Vec3f = Eigen::Vector3f;

/// 9d vector using double internal format
using Vec9 = Eigen::Matrix<double, 9, 1>;

/// Quaternion type
using Quaternion = Eigen::Quaternion<double>;

/// 3x3 matrix using double internal format
using Mat3 = Eigen::Matrix<double, 3, 3>;

/// 3x4 matrix using double internal format
using Mat34 = Eigen::Matrix<double, 3, 4>;

/// 2d vector using double internal format
using Vec2 = Eigen::Vector2d;

/// 4d vector using double internal format
using Vec4 = Eigen::Vector4d;

/// 6d vector using double internal format
using Vec6 = Eigen::Matrix<double, 6, 1>;

/// 4x4 matrix using double internal format
using Mat4 = Eigen::Matrix<double, 4, 4>;

/// generic matrix using unsigned int internal format
using Matu = Eigen::Matrix<unsigned int, Eigen::Dynamic, Eigen::Dynamic>;

/// 3x3 matrix using double internal format with RowMajor storage
using RMat3 = Eigen::Matrix<double, 3, 3, Eigen::RowMajor>;

//-- General purpose Matrix and Vector
/// Unconstrained matrix using double internal format
using Mat = Eigen::MatrixXd;

/// Unconstrained vector using double internal format
using Vec = Eigen::VectorXd;

/// Unconstrained vector using unsigned int internal format
using Vecu = Eigen::Matrix<unsigned int, Eigen::Dynamic, 1>;

/// Unconstrained matrix using float internal format
using Matf = Eigen::MatrixXf;

/// Unconstrained vector using float internal format
using Vecf = Eigen::VectorXf;

/// 2xN matrix using double internal format
using Mat2X = Eigen::Matrix<double, 2, Eigen::Dynamic>;
/// 3xN matrix using double internal format
using Mat3X = Eigen::Matrix<double, 3, Eigen::Dynamic>;
/// 4xN matrix using double internal format
using Mat4X = Eigen::Matrix<double, 4, Eigen::Dynamic>;
/// Nx9 matrix using double internal format
using MatX9 = Eigen::Matrix<double, Eigen::Dynamic, 9>;
//-- Sparse Matrix (Column major, and row major)
/// Sparse unconstrained matrix using double internal format
using sMat = Eigen::SparseMatrix<double>;
/// Sparse unconstrained matrix using double internal format and Row Major storage
using sRMat = Eigen::SparseMatrix<double, Eigen::RowMajor>;

// 3D similarity transformation with 7 degrees of freedom.
class SimilarityTransform3 {
 public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  SimilarityTransform3();

  explicit SimilarityTransform3(const Eigen::Matrix3x4d& matrix);

  explicit SimilarityTransform3(
      const Eigen::Transform<double, 3, Eigen::Affine>& transform);

  SimilarityTransform3(const double scale, const Eigen::Vector4d& qvec,
                       const Eigen::Vector3d& tvec);

  void Estimate(const std::vector<Eigen::Vector3d>& src,
                const std::vector<Eigen::Vector3d>& dst);

  SimilarityTransform3 Inverse() const;

  void TransformPoint(Eigen::Vector3d* xyz) const;
  void TransformPose(Eigen::Vector4d* qvec, Eigen::Vector3d* tvec) const;

  Eigen::Matrix4d Matrix() const;
  double Scale() const;
  Eigen::Vector4d Rotation() const;
  Eigen::Vector3d Translation() const;

 private:
  Eigen::Transform<double, 3, Eigen::Affine> transform_;
  double scale_;

  double S;
  Vec3 t;
  Mat3 R;
};

// Robustly compute alignment between reconstructions by finding images that
// are registered in both reconstructions. The alignment is then estimated
// robustly inside RANSAC from corresponding projection centers. An alignment
// is verified by reprojecting common 3D point observations.
// The min_inlier_observations threshold determines how many observations
// in a common image must reproject within the given threshold.
bool ComputeAlignmentBetweenReconstructions(
    const Reconstruction& src_reconstruction,
    const Reconstruction& ref_reconstruction,
    const double min_inlier_observations, const double max_reproj_error,
    Eigen::Matrix3x4d* alignment);

bool ComputeAlignmentBetweenReconstructions(const Reconstruction& src_reconstruction,
  const Reconstruction& ref_reconstruction, Eigen::Matrix3x4d& alignment);

}  // namespace colmap

EIGEN_DEFINE_STL_VECTOR_SPECIALIZATION_CUSTOM(sfmrecon::SimilarityTransform3)

#endif  // COLMAP_SRC_BASE_SIMILARITY_TRANSFORM_H_
