#ifndef COLMAP_SRC_BASE_POINT_CLOUD_FILTER_H_
#define COLMAP_SRC_BASE_POINT_CLOUD_FILTER_H_
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/voxel_grid.h>

#include <base/reconstruction.h>

namespace sfmrecon {
  typedef pcl::PointXYZRGB PointT;
  typedef pcl::PointXYZI PointI;
  typedef pcl::PointCloud<PointT> PointCloud;
  class PointCloudFilter {
  public:
    PointCloudFilter() = default;
    static void Filter(Reconstruction* reconstruction);

  private:
    static PointCloud::Ptr SORFilter(const PointCloud::Ptr &cloud);
    static PointCloud::Ptr VoxelGridFilter(const PointCloud::Ptr &cloud);

  };
}
#endif
