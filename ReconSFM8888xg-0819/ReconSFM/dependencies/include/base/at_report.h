#ifndef COLMAP_SRC_BASE_AT_REPORT_H_
#define COLMAP_SRC_BASE_AT_REPORT_H_
#include <string>
#include <fstream>
#include <pugixml/pugixml.hpp>
#include <vector>

namespace sfmrecon {
  class ATReport {
  public:
    enum PairMode{
      VOCABULARY = 0,
      SPATIAL = 1,
      EXHAUSTIVE = 2,
    };

    enum CameraModel {
      SimplePinhole=0,
      PinholeCamera=1,
      SimpleRadial=2,
      SimpleRadialFisheye=3,
      Radial=4,
      RadialFisheye=5,
      OpenCV=6,
      OpenCVFisheye=7,
      FullOpenCV=8,
      FOV=9,
      ThinPrismFisheye=10,
    };

    struct Settings {
      bool use_pos = true;
      int key_point_density;
      PairMode pair_select_mode = SPATIAL;
      bool estimate_position = true;
      bool estimate_rotation = true;
      bool estimate_tie_point = true;
      bool estimate_focal_length = true;
      bool estimate_principal_point = true;
      bool estimate_radial_distortion = true;
      CameraModel camera_model = SimpleRadial;
      
      void Load(const pugi::xml_node& node);
      void Save(pugi::xml_node& node) const;
    };

    struct TimeInfo {
      long long start_time;
      long long end_time;
      std::string cost_time;
      float total_cost_time_percent = 0;

      void Load(const pugi::xml_node& node);
      void Save(pugi::xml_node& node) const;
    };

    struct PerPhoto {
      std::string image_name;
      int num_of_key_points = 0;
      int num_of_points = 0;
      float RMS_of_reprojection_errors = 0;

      void Load(const pugi::xml_node& node);
      void Save(pugi::xml_node& node) const;
    };

    struct Connection {
      int camera_id = -1;
      std::string image_name;
      int image_id = -1;
      int num_of_tested_pairs = 0;
      int num_of_connected_pairs = 0;

      void Load(const pugi::xml_node& node);
      void Save(pugi::xml_node& node) const;
    };

    struct PerPoint {
      int median_number_of_key_points_per_photo = 0;
      int num_of_points = 0;
      int median_number_of_photos_per_point = 0;
      int median_number_of_points_per_photo = 0;
      float RMS_of_reprojection_errors = 0;
      int num_of_image = 0;
      int num_of_regist_image = 0;

      void Load(const pugi::xml_node& node);
      void Save(pugi::xml_node& node) const;
    };

    struct Connections {
      int number_of_tested_pairs = 0;
      int median_number_of_tested_pairs_per_photo = 0;
      int median_number_of_connected_photos_per_photo = 0;

      void Load(const pugi::xml_node& node);
      void Save(pugi::xml_node& node) const;
    };

    struct ClusterInfo {
      int id;
      int image_num;
      ClusterInfo() = default;
      ClusterInfo(int id, int image_num);

      void Load(const pugi::xml_node& node);
      void Save(pugi::xml_node& node) const;
    };

    struct TimeInfos {
      TimeInfo feature_extract;
      TimeInfo feature_match;
      TimeInfo sparse_reconstruction;
      TimeInfo transform_gps;

      void Load(const pugi::xml_node& node);
      void Save(pugi::xml_node& node) const;
    };

    struct Results {
      TimeInfos time_infos;
      std::string total_cost_time;

      std::vector<ClusterInfo> clusters;
      
      int num_of_points;
      Connections connections_total;
      std::vector<PerPhoto> photo_infos;
     
      std::vector<Connection> connections;
      PerPoint per_point;

      void Load(const pugi::xml_node& node);
      void Save(pugi::xml_node& node) const;
      void UpdateCostPercent();
    };

  public:
    void LoadFromXml(const std::string& xml_path);
    void ExportXml(const std::string& xml_path) const;

  public:
    Settings settings;
    Results results;
  };
}
#endif