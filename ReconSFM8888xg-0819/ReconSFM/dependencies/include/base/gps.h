#ifndef COLMAP_SRC_BASE_GPS_H_
#define COLMAP_SRC_BASE_GPS_H_

#include <vector>

#include <Eigen/Core>

#include "util/alignment.h"
#include "util/types.h"

namespace sfmrecon{

// Transform ellipsoidal GPS coordinates to Cartesian GPS coordinate
// representation and vice versa.
class GPSTransform {
 public:
  enum ELLIPSOID { GRS80, WGS84 };

  explicit GPSTransform(const int ellipsoid = GRS80);

  Eigen::Vector3d EllToXYZ(
      Eigen::Vector3d & ell) const;
  Eigen::Vector3d lla_to_utm(Eigen::Vector3d& ell) const;

  std::vector<Eigen::Vector3d> XYZToEll(
      const std::vector<Eigen::Vector3d>& xyz) const;

 private:
  // Semimajor axis.
  double a_;
  // Semiminor axis.
  double b_;
  // Flattening.
  double f_;
  // Numerical eccentricity.
  double e2_;
};

}  // namespace colmap

#endif  // COLMAP_SRC_BASE_GPS_H_
