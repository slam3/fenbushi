#ifndef COLMAP_SRC_BASE_GCP_H_
#define COLMAP_SRC_BASE_GCP_H_
#include<vector>
#include <Eigen/Core>
namespace gcp {
	struct Observation {
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
		std::string image_path;
		int image_id = -1;
		Eigen::Vector2d pixel_coord;
	};
	struct GCPInfo
	{
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
		int id;
		Eigen::Vector3d coord;
		Eigen::Vector3d utm_coord;
		std::vector<Observation> observations;
	};

	bool ParseGCPInfosFromTxt(const std::string& txt_path,std::vector<GCPInfo>& gcp_infos);
}


#endif