#ifndef COLMAP_SRC_BASE_IMAGE_READER_H_
#define COLMAP_SRC_BASE_IMAGE_READER_H_

#include <unordered_set>

#include "base/database.h"
#include "util/bitmap.h"
#include "util/threading.h"

namespace sfmrecon{

struct ImageReaderOptions {
  // Path to database in which to store the extracted data.
  std::string database_path = "";

  // Root path to folder which contains the images.
  std::string image_path = "";

  // Optional root path to folder which contains image masks. For a given image,
  // the corresponding mask must have the same sub-path below this root as the
  // image has below image_path. The filename must be equal, aside from the
  // added extension .png. For example, for an image image_path/abc/012.jpg, the
  // mask would be mask_path/abc/012.jpg.png. No features will be extracted in
  // regions where the mask image is black (pixel intensity value 0 in
  // grayscale).
  std::string mask_path = "";

  // Optional list of images to read. The list must contain the relative path
  // of the images with respect to the image_path.
  std::vector<std::string> image_list;

  // Name of the camera model.
  std::string camera_model = "SIMPLE_RADIAL";

  // Whether to use the same camera for all images.
  bool single_camera = false;

  // Whether to use the same camera for all images in the same sub-folder.
  bool single_camera_per_folder = true;

  // Whether to explicitly use an existing camera for all images. Note that in
  // this case the specified camera model and parameters are ignored.
  int existing_camera_id = kInvalidCameraId;

  // Manual specification of camera parameters. If empty, camera parameters
  // will be extracted from EXIF, i.e. principal point and focal length.
  std::string camera_params = "";

  // If camera parameters are not specified manually and the image does not
  // have focal length EXIF information, the focal length is set to the
  // value `default_focal_length_factor * max(width, height)`.
  double default_focal_length_factor = 1.2;
  
  // Optional path to an image file specifying a mask for all images. No
  // features will be extracted in regions where the mask is black (pixel
  // intensity value 0 in grayscale).
  std::string camera_mask_path = "";
  std::string gps_file = "";
  bool Check() const;
  std::string focal_length_file = "";
};

// Recursively iterate over the images in a directory. Skips an image if it
// already exists in the database. Extracts the camera intrinsics from EXIF and
// writes the camera information to the database.
class ImageReader {
 public:
  enum class Status {
    FAILURE,
    SUCCESS,
    IMAGE_EXISTS,
    BITMAP_ERROR,
    CAMERA_SINGLE_DIM_ERROR,
    CAMERA_EXIST_DIM_ERROR,
    CAMERA_PARAM_ERROR
  };

  ImageReader(const ImageReaderOptions& options, Database* database);

  Status Next(Camera* camera, Image* image, Bitmap* bitmap, Bitmap* mask);
  Status Next(Camera* camera, Image* image, Bitmap* bitmap, 
	  const Eigen::Vector3d & xyz=Eigen::Vector3d(0,0,0));
  size_t NextIndex() const;
  size_t NumImages() const;
  void SkipNextIndex();
  std::string NextImageName();

  struct FocalLengthInfo
  {
	  std::string group_name;
	  double focal_length;
	  int status;
  };
  EIGEN_STL_UMAP(std::string, class FocalLengthInfo) focal_length_infos_;
  bool LoadFocalLengthInfoFromTextFile(const std::string& focal_path);
 private:
  // Image reader options.
  ImageReaderOptions options_;
  Database* database_;
  // Index of previously processed image.
  size_t image_index_;
  // Previously processed camera.
  Camera prev_camera_;
  EIGEN_STL_UMAP(std::string, class Camera) prev_cameras_;
};



}  // namespace colmap

#endif  // COLMAP_SRC_BASE_IMAGE_READER_H_
