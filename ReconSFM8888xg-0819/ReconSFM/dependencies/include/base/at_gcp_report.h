#ifndef COLMAP_SRC_BASE_AT_REPORT_GCP_H_
#define COLMAP_SRC_BASE_AT_REPORT_GCP_H_
#include <string>
#include <fstream>
#include <pugixml/pugixml.hpp>
#include <vector>
#include "at_report.h"
#include<map>
namespace sfmrecon {
  class ATGCPReport:ATReport {
  public:
    enum PairMode{
      VOCABULARY = 0,
      SPATIAL = 1,
      EXHAUSTIVE = 2,
    };

    enum CameraModel {
      SimplePinhole=0,
      PinholeCamera=1,
      SimpleRadial=2,
      SimpleRadialFisheye=3,
      Radial=4,
      RadialFisheye=5,
      OpenCV=6,
      OpenCVFisheye=7,
      FullOpenCV=8,
      FOV=9,
      ThinPrismFisheye=10,
    };

    struct PerPhoto_ControlPoints {
	  int photogroup = 0;
	  std::string file_name;
	  int before_at_number_of_points = 0;
      float reprojection_squared_errors_sum = 0;//��ʱ�洢����������rms
	  float before_at_rms_of_reprojection_errors = 0;
      float before_at_rms_of_distances_to_rays = 0;
	  int after_at_number_of_points = 0;
      float after_at_rms_of_reprojection_errors = 0;
      float after_at_rms_of_distances_to_rays = 0;

      void Load(const pugi::xml_node& node);
      void Save(pugi::xml_node& node) const;
    };

    struct Connections {
      std::string image_name;
      int num_of_tested_pairs = 0;
      int num_of_connected_pairs = 0;
    };


    struct PerPoint_ControlPoints {
        std::string name;
        std::string category="Full";
        std::string checkpoint;

        int num_of_key_points = 0;


        float horizontal_accuracy = 0;
        float vertical_accuracy = 0;

        int before_at_number_of_photos = 0;
        float before_at_rms_of_reprojection_errors = 0;
        float before_at_rms_of_distances_to_rays = 0;
        float before_at_rms_of_3d_errors = 0;
        float before_at_rms_of_horizontal_errors = 0;
        float before_at_rms_of_vertical_errors = 0;

        int after_at_number_of_photos = 0;
        float after_at_rms_of_reprojection_errors = 0;
        float after_at_rms_of_distances_to_rays = 0;
        float after_at_rms_of_3d_errors = 0;
        float after_at_rms_of_horizontal_errors = 0;
        float after_at_rms_of_vertical_errors = 0;

        void Load(const pugi::xml_node& node);
        void Save(pugi::xml_node& node) const;
    };

	struct global_errors_info
	{
		std::string type;
		
		int before_at_number_of_points = 0;
        float before_at_median_reprojection_error = 0;
        float before_at_rms_of_reprojection_errors = 0;
        float before_at_rms_of_distances_to_rays = 0;
        float before_at_rms_of_3d_errors = 0;
        float before_at_rms_of_horizontal_errors = 0;
        float before_at_rms_of_vertical_errors = 0;

		int after_at_number_of_points = 0;
		float after_at_median_reprojection_error = 0;
		float after_at_rms_of_reprojection_errors = 0;
		float after_at_rms_of_distances_to_rays = 0;
		float after_at_rms_of_3d_errors = 0;
		float after_at_rms_of_horizontal_errors = 0;
		float after_at_rms_of_vertical_errors = 0;

		void Load(const pugi::xml_node& node);
		void Save(pugi::xml_node& node) const;
	};
	struct global_connections_info
	{
		int number_of_tested_pairs=0;
		int median_number_of_tested_pairs_per_photo=0;
		int before_at_median_number_of_connected_photos_per_photo=0;
		int after_at_median_number_of_connected_photos_per_photo=0;
	};


    struct PerPoint_AutomaticTiePoints
    {
        int median_number_of_key_points_per_photo = 0;

        int before_at_number_of_points = 0;
        int before_at_median_number_of_photos_per_point = 0;
        int before_at_median_number_of_points_per_photo = 0;
        float before_at_rms_of_reprojection_errors = 0;
        float before_at_rms_of_distances_to_rays = 0;

        int after_at_number_of_points = 0;
        int after_at_median_number_of_photos_per_point = 0;
        int after_at_median_number_of_points_per_photo = 0;
        float after_at_rms_of_reprojection_errors = 0;
        float after_at_rms_of_distances_to_rays = 0;

        void Load(const pugi::xml_node& node);
        void Save(pugi::xml_node& node) const;
    };
    
	struct GlobalInfo {
        std::string positioning_mode;//Use control points for adjustment
		global_errors_info control_points;//-errors
		global_errors_info automatic_tie_points;//-errors
		global_connections_info conn_info;

		void Load(const pugi::xml_node& node);
		void Save(pugi::xml_node& node) const;
	};
    //II. Results
    struct Results {
        //A. Global:
        GlobalInfo global_info;
        //B.Per photo :
        //std::vector<PerPhoto_ControlPoints> Perphoto_photo_infos;
        std::map<std::string, ATGCPReport::PerPhoto_ControlPoints > Perphoto_photo_infos_map;
        //C. Per point:
        std::map<std::string, ATGCPReport::PerPoint_ControlPoints> Perpoint_control_points_map;
        PerPoint_AutomaticTiePoints PerPoint_automatic_tie_points;


        void Load(const pugi::xml_node& node);
        void Save(pugi::xml_node& node) const;
        void UpdateCostPercent();
    };

  public:
    void LoadFromXml(const std::string& xml_path);
    void ExportXml(const std::string& xml_path) const;

  public:
    Settings settings;
    Results results;
  };
}
#endif