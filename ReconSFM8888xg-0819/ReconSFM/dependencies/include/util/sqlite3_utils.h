#ifndef COLMAP_SRC_UTIL_SQLITE3_UTILS_
#define COLMAP_SRC_UTIL_SQLITE3_UTILS_

#include <cstdio>
#include <cstdlib>
#include <string>

#include "ext/SQLite/sqlite3.h"

namespace sfmrecon{

inline int SQLite3CallHelper(const int result_code, const std::string& filename,
                             const int line_number) {
  switch (result_code) {
    case SQLITE_OK:
    case SQLITE_ROW:
    case SQLITE_DONE:
      return result_code;
    default:
      fprintf(stderr, "SQLite error [%s, line %i]: %s\n", filename.c_str(),
              line_number, sqlite3_errstr(result_code));
      exit(EXIT_FAILURE);
  }
}

#define SQLITE3_CALL(func) SQLite3CallHelper(func, __FILE__, __LINE__)

#define SQLITE3_EXEC(database, sql, callback)                                 \
  {                                                                           \
    char* err_msg = nullptr;                                                  \
    const int result_code =                                                   \
        sqlite3_exec(database, sql, callback, nullptr, &err_msg);             \
    if (result_code != SQLITE_OK) {                                           \
      fprintf(stderr, "SQLite error [%s, line %i]: %s\n", __FILE__, __LINE__, \
              err_msg);                                                       \
      sqlite3_free(err_msg);                                                  \
    }                                                                         \
  }

inline int SAFEEXE(sqlite3* database, const char * sql, int(*callback)(void*, int, char**, char**)) {
	int count = 0;
	while (1) {
		char* err_msg = nullptr;
		const int result_code = sqlite3_exec(database, sql, callback, nullptr, &err_msg);
		
		if (result_code != SQLITE_OK) {
			if (count > 1000) {
				std::string resultStr = err_msg;
				fprintf(stderr, "SQLite error [%s, line %i]: %s\n", __FILE__, __LINE__, err_msg);
				sqlite3_free(err_msg);
				break;
			}
			else {
				if (result_code == SQLITE_LOCKED || result_code == SQLITE_PROTOCOL || result_code == SQLITE_IOERR)
				{
					count++;
					std::this_thread::sleep_for(std::chrono::milliseconds(1000));
					continue;
				}
				else {
					std::string resultStr = err_msg;
					fprintf(stderr, "SQLite error [%s, line %i]: %s\n", __FILE__, __LINE__, err_msg);
					sqlite3_free(err_msg);
					break;
				}
			}
			
		}
		break;
	}
	
}
}  // namespace colmap

#endif  // COLMAP_SRC_UTIL_SQLITE3_UTILS_
