#pragma once
#include <QFile>
#include <QDir>
#include <QDebug>
#include <QString>

namespace sfmrecon {
	bool copyDir(const QString &source, const QString &destination,  bool override=true);

}