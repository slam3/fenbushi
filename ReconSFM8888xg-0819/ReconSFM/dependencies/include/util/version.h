#ifndef COLMAP_SRC_UTIL_VERSION_H_
#define COLMAP_SRC_UTIL_VERSION_H_

#include "misc.h"

namespace sfmrecon {

const static std::string SFMRECON_VERSION = "3.6";
const static int SFMRECON_VERSION_NUMBER = 3400;
const static std::string SFMRECON_COMMIT_ID = "Unknown";
const static std::string SFMRECON_COMMIT_DATE = "Unknown";

std::string GetVersionInfo();

std::string GetBuildInfo();

} //

#endif  
