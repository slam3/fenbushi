
#include "geometry/eigen_alias_definition.hpp"

namespace sfmrecon
{

/**
* @brief Extract a submatrix given a list of column
* @param A Input matrix
* @param columns A vector of columns index to extract
* @return Matrix containing a subset of input matrix columns
* @note columns index start at index 0
* @note Assuming columns contains a list of valid columns index
*/
template <typename TCols>
Mat ExtractColumns
(
  const Eigen::Ref<const Mat> &A,
  const TCols &columns
)
{
  Mat compressed( A.rows(), columns.size() );
  for ( size_t i = 0; i < static_cast<size_t>( columns.size() ); ++i )
  {
    compressed.col( i ) = A.col( columns[i] );
  }
  return compressed;
}

} 
