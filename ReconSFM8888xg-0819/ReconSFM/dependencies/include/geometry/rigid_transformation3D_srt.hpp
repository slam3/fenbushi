#include "geometry/lm.hpp"
#include "geometry/eigen_alias_definition.hpp"

namespace sfmrecon
{
namespace geometry
{

/** 3D rigid transformation estimation (7 dof)
 * Compute a Scale Rotation and Translation rigid transformation.
 * This transformation provide a distortion-free transformation
 * using the following formulation Xb = S * R * Xa + t.
 * "Least-squares estimation of transformation parameters between two point patterns",
 * Shinji Umeyama, PAMI 1991, DOI: 10.1109/34.88573
 *
 * \param[in] x1 The first 3xN matrix of euclidean points
 * \param[in] x2 The second 3xN matrix of euclidean points
 * \param[out] S The scale factor
 * \param[out] t The 3x1 translation
 * \param[out] R The 3x3 rotation
 *
 * \return true if the transformation estimation has succeeded
 *
 * \note Need at least 3 points
 */
  bool FindRTS
  (
    const Eigen::Matrix<double, 3, Eigen::Dynamic> &x1,
    const Eigen::Matrix<double, 3, Eigen::Dynamic> &x2,
    double * S,
	 Eigen::Vector3d * t,
	 Eigen::Matrix3d * R
  );

/**
* @brief Eigen Levemberg-Marquardt functor to refine translation, Rotation and Scale parameter.
*/
struct lm_SRTRefine_functor : Functor<double>
{
  /**
  * @brief Constructor
  * @param inputs Number of inputs (nb elements to refine)
  * @param values Number of samples
  * @param x1 Input samples for first dataset
  * @param x2 Input samples for second dataset
  * @param S Scale
  * @param R Rotation
  * @param t Translation
  */
  lm_SRTRefine_functor( int inputs, int values,
                        const Eigen::MatrixXd &x1, const Eigen::MatrixXd &x2,
                        const double &S, const Eigen::Matrix3d & R, const Eigen::Vector3d &t );

  /**
  * @brief Computes error given a sample
  * @param x a Sample
  * @param[out] fvec Error for each values
  */
  int operator()( const Eigen::VectorXd  &x, Eigen::VectorXd  &fvec ) const;

  Eigen::MatrixXd x1_, x2_;
  Eigen::Vector3d  t_;
  Eigen::Matrix3d R_;
  double S_;
};


/**
* @brief Eigen LM functor to refine Rotation.
*/
struct lm_RRefine_functor : Functor<double>
{
  /**
  * @brief Constructor
  * @param inputs Number of inputs (elements to refine)
  * @param values Number of samples
  * @param x1 Input samples for first dataset
  * @param x2 Input samples for second dataset
  * @param S Scale
  * @param R Rotation
  * @param t Translation
  */
  lm_RRefine_functor( int inputs, int values,
                      const Eigen::MatrixXd &x1, const Eigen::MatrixXd &x2,
                      const double &S, const Eigen::Matrix3d & R, const Eigen::VectorXd &t );

  /**
   * @brief Computes error given a sample
   * @param x a Sample
   * @param[out] fvec Error for each values
   */
  int operator()( const Eigen::VectorXd &x, Eigen::VectorXd &fvec ) const;

  Eigen::MatrixXd x1_, x2_;
  Eigen::Vector3d t_;
  Eigen::Matrix3d R_;
  double S_;
};

/** 3D rigid transformation refinement using LM
 * Refine the Scale, Rotation and translation rigid transformation
 * using a Levenberg-Marquardt opimization.
 *
 * \param[in] x1 The first 3xN matrix of euclidean points
 * \param[in] x2 The second 3xN matrix of euclidean points
 * \param[out] S The initial scale factor
 * \param[out] t The initial 3x1 translation
 * \param[out] R The initial 3x3 rotation
 *
 * \return none
 */
void Refine_RTS
(
	const Eigen::Matrix<double, 3, Eigen::Dynamic> &x1,
	const Eigen::Matrix<double, 3, Eigen::Dynamic> &x2,
  double * S,
	Eigen::Vector3d  * t,
	Eigen::Matrix3d * R
);

} // namespace geometry
} 
