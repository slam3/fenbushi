#include "geometry/eigen_alias_definition.hpp"

namespace sfmrecon
{
namespace geometry
{

/**
* @brief Defines a pose in 3d space
* [R|C] t = -RC
*/
class Pose3
{
  protected:

    /// Orientation matrix
	  Eigen::Matrix3d rotation_;

    /// Center of rotation
	  Eigen::Vector3d center_;

  public:

    /**
    * @brief Constructor
    * @param r Rotation
    * @param c Center
    * @note Default (without args) defines an Identity pose.
    */
    Pose3
    (
      const Eigen::Matrix3d& r = std::move(Eigen::Matrix3d::Identity()),
      const Eigen::Vector3d& c = std::move(Vec3::Zero())
    )
    : rotation_( r ), center_( c ) {}

    /**
    * @brief Get Rotation matrix
    * @return Rotation matrix
    */
    const Eigen::Matrix3d& rotation() const
    {
      return rotation_;
    }

    /**
    * @brief Get Rotation matrix
    * @return Rotation matrix
    */
	Eigen::Matrix3d& rotation()
    {
      return rotation_;
    }

    /**
    * @brief Get center of rotation
    * @return center of rotation
    */
    const Eigen::Vector3d& center() const
    {
      return center_;
    }

    /**
    * @brief Get center of rotation
    * @return Center of rotation
    */
	Eigen::Vector3d& center()
    {
      return center_;
    }

    /**
    * @brief Get translation vector
    * @return translation vector
    * @note t = -RC
    */
    inline Eigen::Vector3d translation() const
    {
      return -( rotation_ * center_ );
    }


    /**
    * @brief Apply pose
    * @param p Point
    * @return transformed point
    */
    template<typename T>
    inline typename T::PlainObject operator() (const T& p) const
    {
      return rotation_ * ( p.colwise() - center_ );
    }
    /// Specialization for Vec3
    inline typename Eigen::Vector3d::PlainObject operator() (const Eigen::Vector3d& p) const
    {
      return rotation_ * ( p - center_ );
    }


    /**
    * @brief Composition of poses
    * @param P a Pose
    * @return Composition of current pose and parameter pose
    */
    Pose3 operator * ( const Pose3& P ) const
    {
      return {rotation_ * P.rotation_,
              P.center_ + P.rotation_.transpose() * center_};
    }


    /**
    * @brief Get inverse of the pose
    * @return Inverse of the pose
    */
    Pose3 inverse() const
    {
      return {rotation_.transpose(),  -( rotation_ * center_ )};
    }

    /**
    * @brief Return the pose as a single Mat34 matrix [R|t]
    * @return The pose as a Mat34 matrix
    */
    inline Mat34 asMatrix() const
    {
      return (Mat34() << rotation_, translation()).finished();
    }

    /**
    * Serialization out
    * @param ar Archive
    */
    //template <class Archive>
    //inline void save( Archive & ar ) const;

    ///**
    //* @brief Serialization in
    //* @param ar Archive
    //*/
    //template <class Archive>
    //inline void load( Archive & ar );
};
} // namespace geometry
} // namespace openMVG


