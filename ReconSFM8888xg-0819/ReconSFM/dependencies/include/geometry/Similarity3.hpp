#include "geometry/pose3.hpp"

namespace sfmrecon
{
namespace geometry
{

/**
* @brief Define a 3D Similarity transform encoded as a 3D pose plus a scale
*/
struct Similarity3
{
  /// Pose
  Pose3 pose_;

  /// Scale
  double scale_;

  /**
  * @brief Default constructor
  * @note This define identity transformation centered at origin of a cartesian frame
  */
  Similarity3();

  /**
  * @brief Constructor
  * @param pose a 3d pose
  * @param scale a scale factor
  */
  Similarity3(sfmrecon::geometry::Pose3 & pose, double scale );

  /**
  * @brief Apply transformation to a point
  * @param point Input point
  * @return transformed point
  */
  Eigen::Vector3d operator () ( const Eigen::Vector3d & point ) const;

  /**
  * @brief Concatenation of pose
  * @param pose Pose to be concatenated with the current one
  * @return Concatenation of poses
  */
  Pose3 operator () ( const Pose3 & pose ) const;

  /**
  * @brief Get inverse of the similarity
  * @return Inverse of the similarity
  */
  Similarity3 inverse() const;

};
sfmrecon::geometry::Pose3 ApplySimilarity(const geometry::Similarity3 & sim, const sfmrecon::geometry::Pose3 pose);
Eigen::Vector3d ApplySimilarityPoint(const geometry::Similarity3 & sim, const Eigen::Vector3d & point);
} // namespace geometry
} // namespace openMVG
