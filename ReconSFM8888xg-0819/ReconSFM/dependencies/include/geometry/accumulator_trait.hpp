/// Accumulator trait to perform safe summation over a specified type
namespace sfmrecon {

template<typename T>
struct Accumulator { using Type = T; };
template<>
struct Accumulator<unsigned char>  { using Type = float; };
template<>
struct Accumulator<unsigned short> { using Type = float; };
template<>
struct Accumulator<unsigned int> { using Type = float; };
template<>
struct Accumulator<char>   { using Type = float; };
template<>
struct Accumulator<short>  { using Type = float; };
template<>
struct Accumulator<int> { using Type = float; };
template<>
struct Accumulator<bool>  { using Type = unsigned int; };

} // namespace openMVG

//#endif //OPENMVG_NUMERIC_ACCUMULATOR_TRAIT_HPP
