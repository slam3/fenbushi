#include "geometry/eigen_alias_definition.hpp"

namespace sfmrecon
{

/**
* @brief Solve linear system
*
* Linear system is given by : \n
* \f$ A x = 0 \f$
* Solution is found using the constraint on x : \f$ \| x \| = 1 \f$
*
* @param[in,out] A Input matrix storing the system to solve
* @param[out] nullspace result vector containing the solution of the system
* @return Singular value corresponding to the solution of the system
*
* @note Computation is made using SVD decomposition of input matrix
* @note Input matrix A content may be modified during computation
* @note Input vector nullspace may be resized to store the full result
*/
double Nullspace
(
  const Eigen::Ref<const Mat> & A,
  Eigen::Ref<Vec> nullspace
);

} 
