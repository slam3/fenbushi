#ifndef COLMAP_SRC_OPTIM_BUNDLE_ADJUSTMENT_WITH_GCP_H_
#define COLMAP_SRC_OPTIM_BUNDLE_ADJUSTMENT_WITH_GCP_H_

#include "optim/bundle_adjustment.h"
namespace sfmrecon {
class BundleAdjustmentWithGCP :public BundleAdjuster {
public:
	BundleAdjustmentWithGCP(const BundleAdjustmentOptions& options,
		const BundleAdjustmentConfig& config):BundleAdjuster(options,config) {}
		
	bool Solve(Reconstruction* reconstruction);
private:
	void SetUp(Reconstruction* reconstruction,
		ceres::LossFunction* loss_function);

	void AddImageToProblem(const image_t image_id, Reconstruction* reconstruction,
		ceres::LossFunction* loss_function);

	void AddPointToProblem(const point3D_t point3D_id,
		Reconstruction* reconstruction,
		ceres::LossFunction* loss_function);

	void AddGCPPointsToProblem(Reconstruction* reconstruction,
		ceres::LossFunction* loss_function);
};

template <typename CameraModel>
struct WeightedCostFunction {
  explicit WeightedCostFunction(const Eigen::Vector2d& point2D)
    : weight_(1.0), observed_x_(point2D(0)), observed_y_(point2D(1)) {}

	explicit WeightedCostFunction(const Eigen::Vector2d& point2D, double weight)
    : weight_(weight), observed_x_(point2D(0)), observed_y_(point2D(1)) {}

  static ceres::CostFunction* Create(const Eigen::Vector2d& point2D, double weight) {
    return (new ceres::AutoDiffCostFunction<
      WeightedCostFunction<CameraModel>, 2, 4, 3, 3,
      CameraModel::kNumParams>(
        new WeightedCostFunction(point2D, weight)));
  }

	template <typename T>
	bool operator()(
		const T* const qvec, const T* const tvec,
		const T* const point3D, const T* const camera_params,
		T* residuals) const {
    // Rotate and translate.
    T projection[3];
    ceres::UnitQuaternionRotatePoint(qvec, point3D, projection);
    projection[0] += tvec[0];
    projection[1] += tvec[1];
    projection[2] += tvec[2];

    // Project to image plane.
    projection[0] /= projection[2];
    projection[1] /= projection[2];

    // Distort and transform to pixel space.
    CameraModel::WorldToImage(camera_params, projection[0], projection[1],
      &residuals[0], &residuals[1]);

    // Re-projection error.
    residuals[0] -= T(observed_x_);
    residuals[1] -= T(observed_y_);

    residuals[0] *= weight_;
    residuals[1] *= weight_;

    //std::cout << "residuals: " << residuals[0] << "\t" << residuals[1] << std::endl;
    return true;
	}

private:
  const double weight_;
  const double observed_x_;
  const double observed_y_;
};
}

#endif