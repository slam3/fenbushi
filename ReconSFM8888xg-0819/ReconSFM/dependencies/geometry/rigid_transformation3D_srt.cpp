
#include "geometry/rigid_transformation3D_srt.hpp"

#include <unsupported/Eigen/NonLinearOptimization>
#include <unsupported/Eigen/NumericalDiff>
#include <fstream>
#include<stdlib.h>
#include<iostream>
namespace sfmrecon {
namespace geometry{

bool FindRTS
(
  const Eigen::Matrix<double, 3, Eigen::Dynamic> &x1,
  const Eigen::Matrix<double, 3, Eigen::Dynamic> &x2,
  double * S,
  Eigen::Vector3d * t,
  Eigen::Matrix3d * R
)
{
  if ( x1.cols() < 3 || x2.cols() < 3 )
  {
    return false;
  } 
  
  //std::cout << x1.col(3) << x2.col(3) << std::endl;

  //std::cout << "kaishile2" << std::endl;
 
  const Eigen::Matrix4d transform = Eigen::umeyama( x1, x2, true );
  //std::cout << "kaishile" << std::endl;
  //// Check critical cases
  *R = transform.topLeftCorner<3, 3>();
  if ( R->determinant() < 0 )
  {
    return false;
  }
  *S = pow( R->determinant(), 1.0 / 3.0 );
  // Check for degenerate case (if all points have the same value...)
  if ( *S < std::numeric_limits<double>::epsilon() )
  {
    return false;
  }

  // Extract transformation parameters
  *S = pow( R->determinant(), 1.0 / 3.0 );
  *R /= *S;
  *t = transform.topRightCorner<3, 1>();

  return true;
}

lm_SRTRefine_functor::lm_SRTRefine_functor( int inputs, int values,
                      const Eigen::MatrixXd  &x1, const const Eigen::MatrixXd  &x2,
                      const double &S, const const Eigen::Matrix3d  & R, const Eigen::Vector3d  &t ): Functor<double>( inputs, values ),
  x1_( x1 ), x2_( x2 ), t_( t ), R_( R ), S_( S ) { }

int lm_SRTRefine_functor::operator()( const Eigen::VectorXd  &x, Eigen::VectorXd  &fvec ) const
{
  // convert x to rotation matrix and a translation vector and a Scale factor
  // x = {tx,ty,tz,anglex,angley,anglez,S}
	Eigen::Vector3d  transAdd = x.block<3, 1>( 0, 0 );
	Eigen::Vector3d  rot = x.block<3, 1>( 3, 0 );
  double Sadd = x( 6 );

  //Build the rotation matrix
  Eigen::Matrix3d  Rcor =
    ( Eigen::AngleAxis<double>( rot( 0 ), Eigen::Vector3d::UnitX() )
      * Eigen::AngleAxis<double>( rot( 1 ), Eigen::Vector3d::UnitY() )
      * Eigen::AngleAxis<double>( rot( 2 ), Eigen::Vector3d::UnitZ() ) ).toRotationMatrix();

  const Eigen::Matrix3d  nR  = R_ * Rcor;
  const Eigen::Vector3d nt = t_ + transAdd;
  const double nS = S_ + Sadd;

  // Evaluate re-projection errors
  Eigen::Vector3d proj;
  for (Eigen::MatrixXd::Index i = 0; i < x1_.cols(); ++i )
  {
    proj = x2_.col( i ) -  ( nS *  nR * ( x1_.col( i ) ) + nt );
    fvec[i * 3]   = proj( 0 );
    fvec[i * 3 + 1] = proj( 1 );
    fvec[i * 3 + 2] = proj( 2 );
  }
  return 0;
}


lm_RRefine_functor::lm_RRefine_functor(int inputs, int values,
	const Eigen::MatrixXd &x1, const Eigen::MatrixXd &x2,
	const double &S, const Eigen::Matrix3d & R, const Eigen::VectorXd &t): Functor<double>( inputs, values ),
  x1_( x1 ), x2_( x2 ), t_( t ), R_( R ), S_( S ) { }

int lm_RRefine_functor::operator()( const Eigen::VectorXd &x, Eigen::VectorXd &fvec ) const
{
  // convert x to rotation matrix
  // x = {anglex,angley,anglez}
	Eigen::Vector3d rot = x.block<3, 1>( 0, 0 );

  //Build the rotation matrix
	Eigen::Matrix3d Rcor =
    ( Eigen::AngleAxis<double>( rot( 0 ), Eigen::Vector3d::UnitX() )
      * Eigen::AngleAxis<double>( rot( 1 ), Eigen::Vector3d::UnitY() )
      * Eigen::AngleAxis<double>( rot( 2 ), Eigen::Vector3d::UnitZ() ) ).toRotationMatrix();

  const Eigen::Matrix3d nR  = R_ * Rcor;
  const Eigen::Vector3d nt = t_;
  const double nS = S_;

  // Evaluate re-projection errors
  Eigen::Vector3d proj;
  for (Eigen::MatrixXd::Index i = 0; i < x1_.cols(); ++i )
  {
    proj = x2_.col( i ) -  ( nS *  nR * ( x1_.col( i ) ) + nt );
    fvec[i * 3]   = proj( 0 );
    fvec[i * 3 + 1] = proj( 1 );
    fvec[i * 3 + 2] = proj( 2 );
  }
  return 0;
}

void Refine_RTS
(
	const Eigen::Matrix<double, 3, Eigen::Dynamic> &x1,
	const Eigen::Matrix<double, 3, Eigen::Dynamic> &x2,
   double * S,
	Eigen::Vector3d  * t,
	Eigen::Matrix3d * R
)
{
  {
    lm_SRTRefine_functor functor( 7, 3 * x1.cols(), x1, x2, *S, *R, *t );

    Eigen::NumericalDiff<lm_SRTRefine_functor> numDiff( functor );

    Eigen::LevenbergMarquardt<Eigen::NumericalDiff<lm_SRTRefine_functor>> lm( numDiff );
    lm.parameters.maxfev = 1000;
	Eigen::VectorXd  xlm = Eigen::VectorXd::Zero( 7 ); // The deviation vector {tx,ty,tz,rotX,rotY,rotZ,S}

    lm.minimize( xlm );

    const Eigen::Vector3d transAdd = xlm.block<3, 1>( 0, 0 );
    const Eigen::Vector3d  rot = xlm.block<3, 1>( 3, 0 );
    const double SAdd = xlm( 6 );

    //Build the rotation matrix
    const Eigen::Matrix3d Rcor =
      ( Eigen::AngleAxis<double>( rot( 0 ), Eigen::Vector3d::UnitX() )
        * Eigen::AngleAxis<double>( rot( 1 ), Eigen::Vector3d::UnitY() )
        * Eigen::AngleAxis<double>( rot( 2 ), Eigen::Vector3d::UnitZ() ) ).toRotationMatrix();

    *R = ( *R ) * Rcor;
    *t = ( *t ) + transAdd;
    *S = ( *S ) + SAdd;
  }

  // Refine rotation
  {
    lm_RRefine_functor functor( 3, 3 * x1.cols(), x1, x2, *S, *R, *t );

    Eigen::NumericalDiff<lm_RRefine_functor> numDiff( functor );

    Eigen::LevenbergMarquardt<Eigen::NumericalDiff<lm_RRefine_functor>> lm( numDiff );
    lm.parameters.maxfev = 1000;
	Eigen::VectorXd  xlm = Eigen::VectorXd::Zero( 3 ); // The deviation vector {rotX,rotY,rotZ}

    lm.minimize( xlm );

    const Eigen::Vector3d  rot = xlm.block<3, 1>( 0, 0 );

    //Build the rotation matrix
    const Eigen::Matrix3d Rcor =
      ( Eigen::AngleAxis<double>( rot( 0 ), Eigen::Vector3d::UnitX() )
        * Eigen::AngleAxis<double>( rot( 1 ), Eigen::Vector3d::UnitY() )
        * Eigen::AngleAxis<double>( rot( 2 ), Eigen::Vector3d::UnitZ() ) ).toRotationMatrix();

    *R = ( *R ) * Rcor;
  }
}

} // namespace geometry
}
