#include "controllers/bundle_adjustment_with_gcp.h"
#include "util/misc.h"
#include "optim/bundle_adjustment.h"
#include "controllers/incremental_mapper.h"
#include "optim/bundle_adjustment_with_gcp.h"
#include "base/projection.h"

namespace sfmrecon {
BundleAdjustmentWithGCPController::BundleAdjustmentWithGCPController(const OptionManager& options, Reconstruction* reconstruction)
    : options_(options), reconstruction_(reconstruction) {}

void BundleAdjustmentWithGCPController::Run() {
	CHECK_NOTNULL(reconstruction_);
	PrintHeading1("Global bundle adjustment with gcp");
	
	reconstruction_->FilterObservationsWithNegativeDepth(); 
	            
	//reconstruction_->AlignWithGCP();
  const std::vector<gcp::GCPInfo> gcp_infos = reconstruction_->GCPInfos();

  double cost = 0;
  for (auto& gcp_info : gcp_infos) {
    double sum_error = 0;
    for (auto& observation : gcp_info.observations) {
      const Image& image = reconstruction_->Image(observation.image_id);
      const Camera& camera = reconstruction_->Camera(image.CameraId());
      Eigen::Vector2d pixel_coord = ProjectPointToImage(gcp_info.utm_coord, image.ProjectionMatrix(), camera);
      sum_error += (observation.pixel_coord - pixel_coord).norm();
      cost += 0.5*(observation.pixel_coord - pixel_coord).squaredNorm();
    }
    std::cout << "gcp_info " << gcp_info.id << " projection error: " << sum_error / gcp_info.observations.size() << std::endl;
    
 }

  std::cout << "gcp cost: " << cost << std::endl;

  double cost1 = 0;
  for (auto& image_id : reconstruction_->RegImageIds()) {
    cost1 += reconstruction_->ComputePoint3DCost(image_id);
  }
  std::cout << "point3d cost: " << cost1 << std::endl;
  
  std::cout << "all cost: " << cost1 + cost << std::endl;
  AdjustGlobalBundle();
}

bool BundleAdjustmentWithGCPController::AdjustGlobalBundle() {
	IncrementalMapperOptions options;
	BundleAdjustmentOptions ba_options = options.GlobalBundleAdjustment();
  ba_options.refine_principal_point = false;
  ba_options.refine_focal_length = false;
  ba_options.refine_extra_params = false;
	
	BundleAdjustmentConfig ba_config;
	const std::vector<image_t>& reg_image_ids = reconstruction_->RegImageIds();
	for (const image_t image_id : reg_image_ids) {
		ba_config.AddImage(image_id);
	}
	BundleAdjustmentWithGCP bundle_adjuster(ba_options, ba_config);
	if (!bundle_adjuster.Solve(reconstruction_)) {
		return false;
	}
}

}