#include "controllers/merge_mapper.h"
#include "util/misc.h"
#include <unordered_map>
#include "util/fileFun.h"
#include "base/at_report.h"

namespace sfmrecon {
	namespace {

		void MergeClusters(
			const SceneClustering::Cluster& cluster,
			std::unordered_map<const SceneClustering::Cluster*, ReconstructionManager>*
			reconstruction_managers) {
			// Extract all reconstructions from all child clusters.
			// 遍历所有的cluster，将所有的重构结果存储在vector中
			std::cout << "begin to merge" << std::endl;
			std::cout << "cluster root address:" << &cluster << std::endl;
			std::cout << "current cluster size:" << cluster.image_ids.size() << std::endl;
			for (const auto& child_cluster : cluster.child_clusters) {
				std::cout << "child ids:" << child_cluster.image_ids.size() << std::endl;
				std::cout << "child size:" << child_cluster.child_clusters.size() << std::endl;
			}
			std::cout << "=============" << std::endl;
			std::vector<Reconstruction*> reconstructions;
			for (const auto& child_cluster : cluster.child_clusters) {
				if (!child_cluster.child_clusters.empty()) {  // 有子节点，递归调用
					std::cout << "grand child not empty" << std::endl;
					MergeClusters(child_cluster, reconstruction_managers);
				}
				//存储reconstruction
				std::cout << "grand child empty" << std::endl;
				std::cout << "child ids:" << child_cluster.image_ids.size() << std::endl;
				std::cout << "read child address:" << &child_cluster << std::endl;
				auto& reconstruction_manager = reconstruction_managers->at(&child_cluster);
				for (size_t i = 0; i < reconstruction_manager.Size(); ++i) {
					reconstructions.push_back(&reconstruction_manager.Get(i));
				}
			}
      std::sort(reconstructions.begin(), reconstructions.end(), [](const Reconstruction* re1,
        const Reconstruction* re2) { return re1->NumRegImages() > re2->NumRegImages(); });
			std::cout << "get reconstructions ok" << std::endl;
			std::cout << "reconstructions size:" << reconstructions.size() << std::endl;

			// Try to merge all child cluster reconstruction.
			std::cout << "merge=============" << std::endl;
			while (reconstructions.size() > 1) {
#if 1
        int num_filtered = reconstructions[0]->FilterObservationsWithNegativeDepth();
        num_filtered += reconstructions[1]->FilterObservationsWithNegativeDepth();
        LOG(INFO) << "before reconstuction merge filter negative depth observation num: "
          << num_filtered << std::endl;
        LOG(INFO) << "merged reconstructions with regist images: " << reconstructions[0]->NumRegImages()
          << "\t" << reconstructions[1]->NumRegImages() << std::endl;
        const double kMaxReprojError = 12.0;

        if (reconstructions[0]->Merge(*reconstructions[1], kMaxReprojError)) {
          //AdjustGlobalBundle(*reconstructions[0]);
          //IncrementalMapper::Options option;
          //int num_filtered = reconstructions[0]->FilterAllPoints3D(option.filter_max_reproj_error, 
          //option.filter_min_tri_angle);
          const std::unordered_set<point3D_t>& point3D_ids = reconstructions[0]->Point3DIds();
          size_t num_filtered = 0;
          num_filtered +=
            reconstructions[0]->FilterPoints3DWithLargeReprojectionError(kMaxReprojError, point3D_ids);
          LOG(INFO) << "filter observations: " << num_filtered;
          /*int num_merged = MergeTracks(*reconstructions[0]);
          LOG(INFO) << "merged observations: " << num_merged;*/
          reconstructions.erase(reconstructions.begin() + 1);
        }
        else {
          break;
        }
#else
				bool merge_success = false;
				// 依次合并
				for (size_t i = 0; i < reconstructions.size(); ++i) {
					for (size_t j = 0; j < i; ++j) {
						const double kMaxReprojError = 8.0;////10->15；
						 // 如果可以合并，则合并删除被合并的reconstructions，一旦合并成功，则退出循环，重新遍历vector，因为vector结构已经改变
						if (reconstructions[i]->Merge(*reconstructions[j], kMaxReprojError)) {
							reconstructions.erase(reconstructions.begin() + j);
							merge_success = true;
							break;
						}
					}

					if (merge_success) {
						break;
					}
				}

				if (!merge_success) {
					break;
				}
#endif
			}
			std::cout << "merge ok =============" << std::endl;
			// Insert a new reconstruction manager for merged cluster.
			// 合并后，建立新的manage存储reconstruction列表
			std::cout << "create new manage" << std::endl;
			auto& reconstruction_manager = (*reconstruction_managers)[&cluster];
			for (const auto& reconstruction : reconstructions) {
				reconstruction_manager.Add();
				reconstruction_manager.Get(reconstruction_manager.Size() - 1) =
					*reconstruction;
			}
			std::cout << "delete cluster" << std::endl;
			// 删除合并后子节点的managers.
			// Delete all merged child cluster reconstruction managers.
			for (const auto &child_cluster : cluster.child_clusters) {
				reconstruction_managers->erase(&child_cluster);
			}
			std::cout << "merge finish" << std::endl;
		}


		void createNode(SceneClustering::Cluster& currentCluster, int currentIndex, std::string file,
			std::unordered_map<int, std::string> imageIndex,
			std::unordered_map<int, std::string> childIndex,
			std::unordered_map<const SceneClustering::Cluster*, ReconstructionManager> &reconstruction_managers) {
			//create node
			std::cout << "------------------------------------------------" << std::endl;
			std::cout << "currentaddress:" << &currentCluster << std::endl;
			std::cout << "currentIndex:" << currentIndex << std::endl;
			std::cout << "set images:" << std::endl;
			std::string imageIds = imageIndex.at(currentIndex);
			std::cout << "imageIds:" << imageIds << std::endl;
			const std::vector<std::string> id_elems = StringSplit(imageIds, ",");
			currentCluster.image_ids.clear();
			for (const auto image_id : id_elems) {
				image_t imgId = str2int(image_id);
				currentCluster.image_ids.insert(imgId);
			}
			std::cout << "get image size:" << currentCluster.image_ids.size() << std::endl;
			std::cout << "set childs:" << std::endl;
			if (childIndex.at(currentIndex) == "nil,nil") {
				//is leaf
				std::cout << "create manage:" << std::endl;
				std::cout << "currentaddress:" << &currentCluster << std::endl;
				std::cout << "reconstruction_managers size:" << reconstruction_managers.size() << std::endl;
				ReconstructionManager &currentManage = reconstruction_managers[&currentCluster];

				//get fold names
				std::string slash = "/";
				std::string subpath = "clusterJob_" + int2str(currentIndex);
				std::string managePath = JoinPaths(file, slash + subpath);
				std::cout << "managePath:" << managePath << std::endl;
				if (ExistsDir(managePath)) {
					//currentManage.Read(managePath + slash);
					//reconstruction_managers[&currentCluster] = currentManage;

					auto dir_list = GetDirList(managePath + slash);
					std::sort(dir_list.begin(), dir_list.end());
					if (dir_list.size() > 0) {
						for (const auto& dir : dir_list) {
							std::cout << "dir:" << dir << std::endl;
							std::string realDir = StringReplace(dir, "\\", "/") + slash;
							std::cout << "realDir:" << realDir << std::endl;
							currentManage.Read(realDir);
							//reconstruction_managers[&currentCluster].Read(realDir);
						}
					}
					else {
						std::cout << std::endl
							<< "WARNING: Reconstruction empty"
							<< std::endl;
						//return false;
					}
					std::cout << "read finish" << std::endl;
					//reconstruction_managers[&currentCluster] = currentManage;
				}
				else {
					std::cout << std::endl
						<< "WARNING: Reconstruction empty"
						<< std::endl;
					//return false;
				}
			}
			else {
				//no  leaf
				std::string childstr = childIndex.at(currentIndex);
				std::cout << "childstr:" << childstr << std::endl;
				const std::vector<std::string> childs = StringSplit(childstr, ",");
				if (childs.size() != 2) {
					std::cout << "ERROR, wrong construct" << std::endl;
					return;
				}
				SceneClustering::Cluster childClusters[2];
				for (int i = 0; i < 2; i++) {
					currentCluster.child_clusters.push_back(childClusters[i]);
				}
				for (int i = 0; i < 2; i++) {
					int cIndex = str2int(childs[i]);
					std::cout << "cIndex:" << cIndex << std::endl;
					createNode(currentCluster.child_clusters[i], cIndex, file, imageIndex, childIndex, reconstruction_managers);
				}
			}
		}
	}

	MergeMapperController::MergeMapperController(
		const Options& options,
		const SceneClustering::Options& clustering_options,
		OptionManager option_manager_)
		: options_(options),
		clustering_options_(clustering_options),
		option_manager_(option_manager_)
	{

		CHECK(options_.Check());
		CHECK(clustering_options_.Check());
		CHECK_EQ(clustering_options_.branching, 2);

	}


	MergeMapperController::~MergeMapperController()
	{
	}

	//read the cluster structure from file
	bool MergeMapperController::readClusterFromFile(
		std::string file,
		SceneClustering::Cluster &rootCluster) {
		//TODO
		//first, get cluster index
		std::string slash = "/";
		std::string subpath = "clusterstructure.txt";
		std::string clusterPath = JoinPaths(file, slash + subpath);
		*jobfile << "clusterPath:" << clusterPath << std::endl;
		std::ifstream strcutfile;
		strcutfile.open(clusterPath);
		CHECK(strcutfile.is_open()) << clusterPath;
		std::string line;

		std::unordered_map<int, std::string> imageIndex;
		std::unordered_map<int, std::string> childIndex;
		std::unordered_map<SceneClustering::Cluster*, int> leafIndex;
		while (std::getline(strcutfile, line)) {
			StringTrim(&line);
			if (line == "") {
				*jobfile << "ERROR: cluster is empty" << std::endl;
				continue;
			}
			else if (line.find("#") == 0) {
				*jobfile << "is comment" << std::endl;
				continue;
			}
			else {
				*jobfile << "line:" << line << std::endl;
				const std::vector<std::string> line_elems = StringSplit(line, ";");

				int index = str2int(line_elems[0]);
				*jobfile << "index:" << index << std::endl;
				std::string childstr = line_elems[1];
				std::string idsStr = line_elems[2];
				imageIndex.insert(std::make_pair(index, idsStr));
				childIndex.insert(std::make_pair(index, childstr));
			}
		}
		*jobfile << "======================================" << std::endl;
		*jobfile << "imageIndex.size:" << imageIndex.size() << std::endl;
		*jobfile << "childIndex.size:" << childIndex.size() << std::endl;
		*jobfile << "======================================" << std::endl;
		//reconstruct node

		createNode(rootCluster, 0, file, imageIndex, childIndex, mReconstruction_managers);


	
		strcutfile.close();
		*jobfile << "=================================================" << std::endl;

		*jobfile << "print nanage========:" << std::endl;
		*jobfile << " the manages size is" << mReconstruction_managers.size() << std::endl;
		for (std::unordered_map<const SceneClustering::Cluster*, ReconstructionManager>::iterator iter = mReconstruction_managers.begin(); iter != mReconstruction_managers.end(); iter++)
		{
			//std::cout << "key value is" << iter->first;
			*jobfile << "transfer child address:" << (iter->first) << std::endl;
			*jobfile << " the cluster size is" << iter->first->image_ids.size() << std::endl;
			*jobfile << " the manage size is" << iter->second.Size() << std::endl;
		}
		*jobfile << "print end ========:" << std::endl;

		*jobfile << "read finish" << std::endl;
		
		return true;
	}

	bool MergeMapperController::Options::Check() const {
		CHECK_OPTION_GT(init_num_trials, -1);
		CHECK_OPTION_GE(num_workers, -1);
		return true;
	}

	void MergeMapperController::Run() {
		//////////////////////////////////////////////////////////////////////////////
		// Merge clusters  合并结果
		//////////////////////////////////////////////////////////////////////////////

		PrintHeading1("Merging clusters");
		//TODO
		std::string slash = "/";
		std::string filename = options_.workspace + slash + "clusterFile";
		//SceneClustering scene_clustering(clustering_options_);
		SceneClustering::Cluster rootCluster;
		//std::unordered_map<const SceneClustering::Cluster*, ReconstructionManager> reconstruction_managers;
		mReconstruction_managers.reserve(options_.clusterNum);
		readClusterFromFile(filename, rootCluster);
		MergeClusters(rootCluster, &mReconstruction_managers);

		CHECK_EQ(mReconstruction_managers.size(), 1);
		ReconstructionManager reconstruction_manager_;
		reconstruction_manager_ = std::move(mReconstruction_managers.begin()->second);

		const auto mergeFile = options_.localoutput + slash + "merge";
		CreateDirIfNotExists(mergeFile);
		*jobfile << "mergeFile:" << mergeFile << std::endl;
		const auto denseFile = options_.localoutput + slash + "dense";
		CreateDirIfNotExists(denseFile);
		*jobfile << "denseFile:" << denseFile << std::endl;
		if (reconstruction_manager_.Size() > 0) {
			std::vector<int> recon_size;
			for (size_t i = 0; i < reconstruction_manager_.Size(); ++i)
			{
				recon_size.push_back(reconstruction_manager_.Get(i).Images().size());
			}
			auto maxPosition = max_element(recon_size.begin(), recon_size.end());
			int ij = maxPosition - recon_size.begin();

      auto& reconstruction = reconstruction_manager_.Get(ij);
      
      reconstruction.ExportXMLFile(denseFile + "/AT_no_filter.xml", options_.imagePath);
      
#if 0
      int num_filter = 0;
      num_filter = reconstruction.FilterPoints3D(2);
      LOG(INFO) << "reconstruction has points3d: " << reconstruction.NumPoints3D();
      LOG(INFO) << "track length less than 2 filter observation: " << num_filter;
      LOG(INFO) << "reconstruction has points3d: " << reconstruction.NumPoints3D();
      double max_reproj_error = 0, min_tri_angle = 0;
      if (reconstruction.NumRegImages() < 2000) {
        max_reproj_error = 20;
        min_tri_angle = 6;
      } else if (reconstruction.NumRegImages() < 7000) {
        max_reproj_error = 10;
        min_tri_angle = 5;
      } else {
        max_reproj_error = 100;
        min_tri_angle = 10;
      }
      num_filter = reconstruction.FilterAllPoints3D(max_reproj_error, min_tri_angle);
      LOG(INFO) << "based on max_reproj_error and min_tri_angle filter observation: " << num_filter;
      LOG(INFO) << "reconstruction has points3d: " << reconstruction.NumPoints3D();
#endif
      reconstruction.ExportXMLFile(denseFile + "/AT.xml", options_.imagePath);

      ATReport at_report;
      std::string report_path = options_.workspace + "/at_report.xml";
      at_report.LoadFromXml(report_path);
      reconstruction.ExportATReport(at_report);
      at_report.ExportXml(report_path);
		}

    

		reconstruction_manager_.Write(mergeFile, &option_manager_);
		std::string destMergePath = options_.workspace + "/merge";
		copyDir(QString::fromStdString(mergeFile), QString::fromStdString(destMergePath));
		std::string destDensePath = options_.workspace + "/dense";
		copyDir(QString::fromStdString(denseFile), QString::fromStdString(destDensePath));
		*jobfile << std::endl;
		GetTimer().PrintMinutes();

	}
}
