
#include "controllers/hierarchical_mapper.h"

#include "base/scene_clustering.h"
#include "util/misc.h"

namespace sfmrecon {
void HierarchicalMapperController::MergeClusters(
    const SceneClustering::Cluster& cluster,
    std::unordered_map<const SceneClustering::Cluster*, ReconstructionManager>*
        reconstruction_managers) {
  // Extract all reconstructions from all child clusters.
  std::vector<Reconstruction*> reconstructions;
  for (const auto& child_cluster : cluster.child_clusters) {
    if (!child_cluster.child_clusters.empty()) {
      MergeClusters(child_cluster, reconstruction_managers);
    }

    auto& reconstruction_manager = reconstruction_managers->at(&child_cluster);
    for (size_t i = 0; i < reconstruction_manager.Size(); ++i) {
      reconstructions.push_back(&reconstruction_manager.Get(i));
    }
  }

  std::sort(reconstructions.begin(), reconstructions.end(), [](const Reconstruction* re1,
    const Reconstruction* re2) { return re1->NumRegImages() > re2->NumRegImages(); });

  // Try to merge all child cluster reconstruction.
  while (reconstructions.size() > 1) {
#if 1
    int num_filtered = reconstructions[0]->FilterObservationsWithNegativeDepth();
    num_filtered += reconstructions[1]->FilterObservationsWithNegativeDepth();
    LOG(INFO) << "before reconstuction merge filter negative depth observation num: " 
      << num_filtered << std::endl;
    LOG(INFO) << "merged reconstructions with regist images: " << reconstructions[0]->NumRegImages()
      << "\t" << reconstructions[1]->NumRegImages() << std::endl;
    const double kMaxReprojError = 12.0;

    if (reconstructions[0]->Merge(*reconstructions[1], kMaxReprojError)) {
      //AdjustGlobalBundle(*reconstructions[0]);
      //IncrementalMapper::Options option;
      //int num_filtered = reconstructions[0]->FilterAllPoints3D(option.filter_max_reproj_error, 
        //option.filter_min_tri_angle);
      const std::unordered_set<point3D_t>& point3D_ids = reconstructions[0]->Point3DIds();
      size_t num_filtered = 0;
      num_filtered +=
        reconstructions[0]->FilterPoints3DWithLargeReprojectionError(kMaxReprojError, point3D_ids);
      LOG(INFO) << "filter observations: " << num_filtered;
      /*int num_merged = MergeTracks(*reconstructions[0]);
      LOG(INFO) << "merged observations: " << num_merged;*/
      reconstructions.erase(reconstructions.begin() + 1);
    } else {
      break;
    }
#else
    bool merge_success = false;
    for (size_t i = 0; i < reconstructions.size(); ++i) {
      for (size_t j = 0; j < i; ++j) {
        const double kMaxReprojError = 12.0;////10->15
        int num_filtered = reconstructions[i]->FilterObservationsWithNegativeDepth();
        num_filtered += reconstructions[j]->FilterObservationsWithNegativeDepth();
        LOG(INFO) << "before reconstuction merge filter negative depth observation num: " << num_filtered << std::endl;
        if (reconstructions[i]->Merge(*reconstructions[j], kMaxReprojError)) {
          reconstructions.erase(reconstructions.begin() + j);
          merge_success = true;
          break;
        }
      }

      if (merge_success) {
        break;
      }
    }

    if (!merge_success) {
      break;
    }
#endif
  }

  // Insert a new reconstruction manager for merged cluster.
  auto& reconstruction_manager = (*reconstruction_managers)[&cluster];
  for (const auto& reconstruction : reconstructions) {
    reconstruction_manager.Add();
    reconstruction_manager.Get(reconstruction_manager.Size() - 1) =
        *reconstruction;
  }

  // Delete all merged child cluster reconstruction managers.
  for (const auto& child_cluster : cluster.child_clusters) {
    reconstruction_managers->erase(&child_cluster);
  }
}

bool HierarchicalMapperController::Options::Check() const {
  CHECK_OPTION_GT(init_num_trials, -1);
  CHECK_OPTION_GE(num_workers, -1);
  return true;
}

HierarchicalMapperController::HierarchicalMapperController(
    const Options& options, const SceneClustering::Options& clustering_options,
    const IncrementalMapperOptions& mapper_options,
    ReconstructionManager* reconstruction_manager)
    : options_(options),
      clustering_options_(clustering_options),
      mapper_options_(mapper_options),
      reconstruction_manager_(reconstruction_manager) {
  CHECK(options_.Check());
  CHECK(clustering_options_.Check());
  CHECK(mapper_options_.Check());
  CHECK_EQ(clustering_options_.branching, 2);
}

void HierarchicalMapperController::Run() {
  PrintHeading1("Partitioning the scene");

  //////////////////////////////////////////////////////////////////////////////
  // Cluster scene
  //////////////////////////////////////////////////////////////////////////////

  SceneClustering scene_clustering(clustering_options_);

  std::unordered_map<image_t, std::string> image_id_to_name;

  
  Database database(options_.database_path);

  std::cout << "Reading images..." << std::endl;
  const auto images = database.ReadAllImages();
  for (const auto& image : images) {
    image_id_to_name.emplace(image.ImageId(), image.Name());
  }

  std::cout << "Reading scene graph..." << std::endl;
  std::vector<std::pair<image_t, image_t>> image_pairs;
  std::vector<int> num_inliers;
  database.ReadTwoViewGeometryNumInliers(&image_pairs, &num_inliers);

  std::cout << "before filter num_inliers_size: " << num_inliers.size() << std::endl;
  for (int i = 0; i < num_inliers.size(); ++i) {
    if (num_inliers[i] < mapper_options_.min_num_matches) {
      num_inliers.erase(num_inliers.begin() + i);
      image_pairs.erase(image_pairs.begin() + i);
      --i;
    }
  }
  std::cout << "after filter num_inliers_size: " << num_inliers.size() << std::endl << std::endl;
  int domain_size = SceneClustering::CheckConnectedDomain(image_pairs, true);
  std::cout << "before sence graph connected domain num: " << domain_size << std::endl;
  if (domain_size > 1) {
    SceneClustering::DeleteOtherDomains(image_pairs, num_inliers);
  }
  std::cout << "Partitioning scene graph..." << std::endl;

  scene_clustering.Partition(image_pairs, num_inliers);
  

  auto leaf_clusters = scene_clustering.GetLeafClusters();
  // Start reconstructing the bigger clusters first for resource usage.
  std::sort(leaf_clusters.begin(), leaf_clusters.end(),
    [](const SceneClustering::Cluster* cluster1,
      const SceneClustering::Cluster* cluster2) {
    return cluster1->image_ids.size() > cluster2->image_ids.size();
  });
  size_t total_num_images = 0;
  for (size_t i = 0; i < leaf_clusters.size(); ++i) {
    total_num_images += leaf_clusters[i]->image_ids.size();
    std::vector<std::pair<image_t, image_t>> edges;
    for (const auto &image_pair : image_pairs) {
      if (leaf_clusters[i]->image_ids.count(image_pair.first) && leaf_clusters[i]->image_ids.count(image_pair.second)) {
        edges.push_back(image_pair);
      }
    }
    std::cout << StringPrintf("  Cluster %d with %d images", i + 1,
      leaf_clusters[i]->image_ids.size()) << "  connected domain  "
      << SceneClustering::CheckConnectedDomain(edges) << std::endl;
  }

  std::cout << StringPrintf("Clusters have %d images", total_num_images)
            << std::endl;

  std::set<image_t> image_ids;
  for (const auto& leaf_cluster : leaf_clusters) {
    image_ids.insert(leaf_cluster->image_ids.begin(), leaf_cluster->image_ids.end());
  }
  
  std::cout << "parted image_ids: " << image_ids.size() << std::endl;
  //////////////////////////////////////////////////////////////////////////////
  // Reconstruct clusters
  //////////////////////////////////////////////////////////////////////////////

  PrintHeading1("Reconstructing clusters");
  auto ReconstructCluster = [&, this](
	  const SceneClustering::Cluster& cluster,
	  ReconstructionManager* reconstruction_manager, int cluster_id) {
	  if (cluster.image_ids.empty()) {
		  return;
	  }

	  IncrementalMapperOptions custom_options = mapper_options_;
	  custom_options.init_num_trials = options_.init_num_trials;
	  for (const auto image_id : cluster.image_ids) {
		  custom_options.image_names.insert(image_id_to_name.at(image_id));
	  }

	  IncrementalMapperController mapper(&custom_options, options_.image_path,
		  options_.database_path,
		  reconstruction_manager);
	  mapper.Start();
	  mapper.Wait();

    CreateDirIfNotExists(options_.out_path);
    std::string xml_export_path = options_.out_path + "/cluster_" + std::to_string(cluster_id) + ".xml";
    reconstruction_manager->Get(0).ExportXMLFile(xml_export_path, options_.image_path + "/");

    std::string center_export_path = options_.out_path + "/cneter_" + std::to_string(cluster_id) + ".txt";
    reconstruction_manager->Get(0).ExportCameraCenter(center_export_path);
    for (size_t i = 0; i < reconstruction_manager->Size(); ++i)
    {
      LOG(INFO) << "cluster_"<<cluster_id<<"\treconstruction id: " << i << "\t image_size: " <<
        reconstruction_manager->Get(i).Images().size() << "\t regist_image_size: " <<
        reconstruction_manager->Get(i).NumRegImages();
    }
  };

  // Start the reconstruction workers.
  std::unordered_map<const SceneClustering::Cluster*, ReconstructionManager>
      reconstruction_managers;
  reconstruction_managers.reserve(leaf_clusters.size());

  int cluster_id = 0;
  for (const auto& cluster : leaf_clusters) {
	  ReconstructCluster(*cluster, &reconstruction_managers[cluster],cluster_id++);
  }
 
  //////////////////////////////////////////////////////////////////////////////
  // Merge clusters
  //////////////////////////////////////////////////////////////////////////////

  PrintHeading1("Merging clusters");

  MergeClusters(*scene_clustering.GetRootCluster(), &reconstruction_managers);

  CHECK_EQ(reconstruction_managers.size(), 1);
  *reconstruction_manager_ = std::move(reconstruction_managers.begin()->second);

  std::cout << std::endl;
  GetTimer().PrintMinutes();
}

size_t HierarchicalMapperController::MergeTracks(Reconstruction& reconstruction) {
  IncrementalMapperOptions inc_options;
  Database database(options_.database_path);
  std::vector<image_pair_t> image_pair_ids;
  std::vector<TwoViewGeometry> two_view_geometries;

  database.ReadTwoViewGeometries(&image_pair_ids, &two_view_geometries);
  const std::vector<class Image> images = database.ReadAllImages();

  auto UseInlierMatchesCheck = [&inc_options](
    const TwoViewGeometry& two_view_geometry) {
    return static_cast<size_t>(two_view_geometry.inlier_matches.size()) >=
      inc_options.min_num_matches &&
      (!inc_options.ignore_watermarks ||
        two_view_geometry.config != TwoViewGeometry::WATERMARK);
  };

  auto &reg_image_ids = reconstruction.RegImageIds();
  std::set<image_t> image_ids(reg_image_ids.begin(), reg_image_ids.end());
  class CorrespondenceGraph correspondence_graph;

  for (const auto& image : images) {
    if (!image_ids.count(image.ImageId())) continue;
    correspondence_graph.AddImage(image.ImageId(), image.NumPoints2D());
  }

  for (size_t i = 0; i < image_pair_ids.size(); ++i) {
    if (UseInlierMatchesCheck(two_view_geometries[i])) {
      image_t image_id1;
      image_t image_id2;
      Database::PairIdToImagePair(image_pair_ids[i], &image_id1, &image_id2);
      if (image_ids.count(image_id1) > 0 && image_ids.count(image_id2) > 0) {
        correspondence_graph.AddCorrespondences(
          image_id1, image_id2, two_view_geometries[i].inlier_matches);
      }
    }
  }

  correspondence_graph.Finalize();

  IncrementalTriangulator triangulator(&correspondence_graph, &reconstruction);
  size_t num_merged_observations = triangulator.MergeAllTracks(inc_options.Triangulation());
  return num_merged_observations;
}
}  // namespace sfmrecon
