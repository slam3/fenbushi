#include "controllers/pointadjust_mapper.h"
#include "util/misc.h"
#include <unordered_map>
#include "util/fileFun.h"
#include<iomanip>
#include<base/point_cloud_filter.h>

namespace sfmrecon {
	bool PointAdjustController::RunGeoPos(Reconstruction &reconstruction)
	{
		int min_common_images = 3;
		bool robust_alignment = false;
		RANSACOptions ransac_options;
		ransac_options.max_error = 7.5;
		OptionManager options;
		GPSTransform gps_transform;

		if (!ExistsFile(options_.gps_file_path))
		{
			std::cout << "Please Make Sure POS File is Existing!" << std::endl;
			return EXIT_FAILURE;
		}

		if (robust_alignment && ransac_options.max_error <= 0) {
			std::cout << "ERROR: You must provide a maximum alignment error > 0"
				<< std::endl;
			return EXIT_FAILURE;
		}
		std::vector<std::string> ref_image_names;
		std::vector<Eigen::Vector3d> ref_locations;

		
		std::vector<Image> images_;
		Database database(options_.database);
		images_ = database.ReadAllImages();
		std::vector<std::string> prior_image_names;
		std::vector<Eigen::Vector3d> prior_ref_locations;
		for (auto& image : images_) {
			prior_image_names.push_back(image.Name());
			std::cout << "name:" << image.Name() << std::endl;
			Eigen::Vector3d   xyzs;
			
			xyzs(0) = image.TvecPrior(0);
			xyzs(1) = image.TvecPrior(1);
			xyzs(2) = image.TvecPrior(2);
			
			std::cout << std::fixed << std::setprecision(3) << "xyzs(0):" << image.TvecPrior(0) << std::endl;
			std::cout << std::fixed << std::setprecision(3) << "xyzs(1):" << image.TvecPrior(1) << std::endl;
			std::cout << std::fixed << std::setprecision(3) << "xyzs(2):" << image.TvecPrior(2) << std::endl;
			prior_ref_locations.push_back(xyzs);
			ref_image_names.push_back(image.Name());
			 ref_locations.push_back(xyzs);
		}
		
		bool alignment_success = reconstruction.Align(ref_image_names, ref_locations, min_common_images);
		
		if (alignment_success) {
			std::cout << " => Alignment succeeded" << std::endl;

		}
		else {
			std::cout << " => Alignment failed" << std::endl;
		}
		return EXIT_SUCCESS;
	}

	PointAdjustController::PointAdjustController(
		const Options& options,
		OptionManager& option_manager_)
		: options_(options),
		option_manager_(option_manager_)
	{
		CHECK(options_.Check());

	}


	PointAdjustController::~PointAdjustController()
	{
	}


	bool PointAdjustController::Options::Check() const {
		//CHECK_OPTION_NE(manageFile, "");
		//CHECK_OPTION_NE(workspace, "");
		if (manageFile == "" || workspace == "") {
			return false;
		}
		return true;
	}

	void PointAdjustController::Run() {

		PrintHeading1("point adjust");

		std::string managePath = options_.manageFile;
		*m_file << "managePath:" << managePath << std::endl;
		ReconstructionManager reconstruction_manager_;
		if (ExistsDir(managePath)) {
			//currentManage.Read(managePath + slash);
			//reconstruction_managers[&currentCluster] = currentManage;
			std::string slash = "/";
			auto dir_list = GetDirList(managePath + slash);
			std::sort(dir_list.begin(), dir_list.end());
			if (dir_list.size() > 0) {
				for (const auto& dir : dir_list) {
					*m_file << "dir:" << dir << std::endl;
					std::string realDir = StringReplace(dir, "\\", "/") + slash;
					*m_file << "realDir:" << realDir << std::endl;
					reconstruction_manager_.Read(realDir);
					//reconstruction_managers[&currentCluster].Read(realDir);
				}
			}
			else {
				*m_file << std::endl
					<< "WARNING: Reconstruction empty"
					<< std::endl;
				return;
			}
			*m_file << "read finish" << std::endl;
			//reconstruction_managers[&currentCluster] = currentManage;
		}
		else {
			*m_file << std::endl
				<< "WARNING: Reconstruction empty"
				<< std::endl;
			return;
		}

		std::vector<int> recon_size;
		for (size_t i = 0; i < reconstruction_manager_.Size(); ++i)
		{
			recon_size.push_back(reconstruction_manager_.Get(i).Images().size());
		}
		auto maxPosition = max_element(recon_size.begin(), recon_size.end());
		int ij = maxPosition - recon_size.begin();

		*m_file << "Max Position is:" << ij << std::endl;
		if (IsStopped()) {
			return;
		}
		std::string slash = "/";
		std::string dense_path = JoinPaths(options_.localoutput, slash + "dense");

		Reconstruction reconstruction = reconstruction_manager_.Get(ij);
		if (ExistsFile(options_.gps_file_path)) {

			RunGeoPos(reconstruction);
		}
		
		CreateDirIfNotExists(dense_path);
		
    //sfmrecon::PointCloudFilter::Filter(&reconstruction);

		reconstruction.ExportGPSXMLFile(dense_path + "/GPS.xml", options_.imagePath);
		std::string bundle_path = dense_path + "/" + "sfmResult.out";
		std::string bundle_path_list = dense_path + "/" + "sfmResult_list.txt";
		reconstruction.ExportBundler(bundle_path, bundle_path_list);
		reconstruction.Write(dense_path);
		//copy to main work path
		std::string destPath = options_.workspace + "/dense";
		CreateDirIfNotExists(destPath);
		copyDir(QString::fromStdString(dense_path), QString::fromStdString(destPath));

    ATReport at_report;
    std::string report_path = options_.workspace + "/at_report.xml";
    at_report.LoadFromXml(report_path);
    reconstruction.ExportATReport(at_report);
    at_report.ExportXml(report_path);
	}




}
