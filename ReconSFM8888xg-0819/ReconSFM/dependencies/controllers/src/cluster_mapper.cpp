#include "controllers/cluster_mapper.h"
#include "base/scene_clustering.h"
#include "util/misc.h"
#include "base/database.h"
#include <vector>
#include <QFile>
#include <base/at_report.h>
//#include "util/logging.h"

namespace sfmrecon {

	ClusterMapperController::ClusterMapperController(const Options& options,
		const SceneClustering::Options& clustering_options) 
		: options_(options),
		clustering_options_(clustering_options){
		CHECK(clustering_options_.Check());
		CHECK_EQ(clustering_options_.branching, 2);
		mClusterNum = 0;
	}


	ClusterMapperController::~ClusterMapperController()
	{
	}

	//write the cluster structure to file
	bool ClusterMapperController::writeClusterToFile(std::string path, const SceneClustering::Cluster& cluster, int leafNum) {
		//TODO
		std::cout << "write cluste file -------------------" << std::endl;
		if (NULL == &cluster) {
			std::cout << "ERROR: empty cluster" << std::endl;
			return false;
		}
		std::string slash = "/";
		std::string clusterFilePath = JoinPaths(path, slash + "clusterFile");
		std::cout << "clusterFilePath:" << clusterFilePath  << std::endl;
		CreateDirIfNotExists(clusterFilePath);
		std::string clusterFileName = JoinPaths(clusterFilePath, slash + "clusterstructure.txt");
		std::cout << "clusterFileName:" << clusterFileName << std::endl;
		std::ofstream clusterfile(clusterFileName, std::ios::trunc);
		CHECK(clusterfile.is_open()) << clusterFileName;
		clusterfile << "#"<< leafNum << std::endl;
		std::vector<SceneClustering::Cluster> clusterVector;
		clusterVector.push_back(cluster);
		std::string leafNames = "";
		int pre = 0;
		int last = 1;
		while (pre < clusterVector.size()) {    //each level
			last = clusterVector.size();      //pre is the begin node of this level, last is the end node of this level
			while (pre < last) {
				//index
				int currentIndex = pre;
				clusterfile << currentIndex << ";";
				//child
				int childSize = clusterVector[currentIndex].child_clusters.size();
				bool isLeaf = false;
				if (childSize > 0) { //not leaf node
					isLeaf = false;
					for (int i = 0; i < childSize; i++) {
						int vetcorSize = clusterVector.size(); // also is the child index, i.e. last position
						SceneClustering::Cluster item = clusterVector[currentIndex].child_clusters[i];
						clusterVector.push_back(item);
						if (i == childSize - 1) {
							clusterfile << vetcorSize << ";";
						}
						else {
							clusterfile << vetcorSize << ",";
						}
					}
				}
				else { // is leaf node
					isLeaf = true;
					clusterfile << "nil,nil;";
				}
				//ids
				if (isLeaf) {
					if (leafNames == "") {
						leafNames += int2str(currentIndex);
					}
					else {
						leafNames += "," + int2str(currentIndex);
					}
					std::string slash = "/";
					std::string leafFilePath = JoinPaths(clusterFilePath, slash + "clusterJob_" + int2str(currentIndex));
					CreateDirIfNotExists(leafFilePath);
					std::string leafFileName = JoinPaths(leafFilePath, slash + "cluster" + int2str(currentIndex) + ".txt");
					std::ofstream leaffile(leafFileName, std::ios::trunc);
					CHECK(leaffile.is_open()) << leafFileName;
					
          std::vector<image_t> image_ids(clusterVector[currentIndex].image_ids.begin(), clusterVector[currentIndex].image_ids.end());
          int idSize = clusterVector[currentIndex].image_ids.size();
					for (int i = 0; i < idSize; i++) {
						image_t id = image_ids[i];
						if (i == idSize - 1) {
							leaffile << id;
						}
						else {
							leaffile << id << ",";
						}
					}
					leaffile.close();
					
				}
        std::vector<image_t> image_ids(clusterVector[currentIndex].image_ids.begin(), clusterVector[currentIndex].image_ids.end());   
        int idSize = clusterVector[currentIndex].image_ids.size();
				for (int i = 0; i < idSize; i++) {
					image_t id = image_ids[i];
					if (i == idSize - 1) {
						clusterfile << id;
					}
					else {
						clusterfile << id << ",";
					}
				}
				clusterfile << std::endl;
				pre++;
			}
		}
		std::cout << "leaf numbers :" << leafNames << std::endl;
		clusterfile.close();
		mClusterNames = leafNames;
		return true;
	}

	int ClusterMapperController::getClusterNumber() {
		return mClusterNum;
	}

	std::string ClusterMapperController::getClusterNames() {
		return mClusterNames;
	}

	void ClusterMapperController::Run() {
		PrintHeading1("Partitioning the scene");

		//////////////////////////////////////////////////////////////////////////////
		// Cluster scene 
		//////////////////////////////////////////////////////////////////////////////
    std::string at_report_path = options_.workspace_path + "/at_report.xml";
    ATReport at_report;
    at_report.LoadFromXml(at_report_path);
		SceneClustering scene_clustering(clustering_options_);
		{
			//read all images
			Database database(options_.database_path);

			//read scene graph, get image pair, and num_inliers
			*file << "Reading scene graph..." << std::endl;
			std::vector<std::pair<image_t, image_t>> image_pairs;
			std::vector<int> num_inliers;
			database.ReadTwoViewGeometryNumInliers(&image_pairs, &num_inliers);       //???????????????????????????
      int domain_size = SceneClustering::CheckConnectedDomain(image_pairs, true);
      std::cout << "before sence graph connected domain num: " << domain_size << std::endl;
      if (domain_size > 1) {
        SceneClustering::DeleteOtherDomains(image_pairs, num_inliers);
      }
      //Partitioning 
			*file << "Partitioning scene graph..." << std::endl;
			scene_clustering.Partition(image_pairs, num_inliers);

      auto AddMap = [](std::unordered_map<int, int>& map, int key) {
        if (map.find(key) == map.end()) {
          map[key] = 1;
        }
        else {
          ++map[key];
        }
      };
      std::unordered_map<int, int> num_of_test_pairs;
      for (auto& pair : image_pairs) {
        AddMap(num_of_test_pairs, pair.first);
        AddMap(num_of_test_pairs, pair.second);
      }
		  
      std::vector<ATReport::Connection> &connections = at_report.results.connections;
      for (auto& pair : num_of_test_pairs) {
        ATReport::Connection connection;
        connection.image_id = pair.first;
        connection.num_of_tested_pairs = pair.second;
        connections.emplace_back(connection);
      }

      ATReport::Connections &connections_total = at_report.results.connections_total;
      connections_total.number_of_tested_pairs = image_pairs.size();
      std::sort(connections.begin(), connections.end(), [](const ATReport::Connection& conn1,
        const ATReport::Connection& conn2) {return conn1.num_of_tested_pairs < conn2.num_of_tested_pairs; });
      connections_total.median_number_of_tested_pairs_per_photo = connections[connections.size() / 2].num_of_tested_pairs;

      std::sort(connections.begin(), connections.end(), [](const ATReport::Connection& conn1,
        const ATReport::Connection& conn2) {return conn1.image_id < conn2.image_id; });
    }


		auto leaf_clusters = scene_clustering.GetLeafClusters();
    
		//print leaf number
		size_t total_num_images = 0;
		for (size_t i = 0; i < leaf_clusters.size(); ++i) {
      at_report.results.clusters.emplace_back(i, leaf_clusters[i]->image_ids.size());
			total_num_images += leaf_clusters[i]->image_ids.size();
			*file << StringPrintf("  Cluster %d with %d images", i + 1,
				leaf_clusters[i]->image_ids.size())
				<< std::endl;
		}
    at_report.ExportXml(at_report_path);

		*file << StringPrintf("Clusters have %d images", total_num_images)
			<< std::endl;

		//write to file
		//TODO
		mClusterNum = leaf_clusters.size();
		

		std::string filepath = options_.workspace_path;
		writeClusterToFile(filepath, *scene_clustering.GetRootCluster(), mClusterNum);
	}

}
