#include "controllers/partial_mapper.h"
#include "util/misc.h"
#include <QFile>

namespace sfmrecon {
	PartialMapperController::PartialMapperController(const Options& options,
		const IncrementalMapperOptions& mapper_options,
		ReconstructionManager* reconstruction_manager,
		OptionManager option_manager_)
		: options_(options),
		mapper_options_(mapper_options),
		reconstruction_manager_(reconstruction_manager),
		option_manager_(option_manager_) {
		CHECK(options_.Check());
		CHECK(mapper_options_.Check());
		RegisterCallback(PARTIAL_FINISHED_CALLBACK);
	}

	PartialMapperController::~PartialMapperController()
	{
	}

	bool PartialMapperController::Options::Check() const {
		CHECK_OPTION_GT(init_num_trials, -1);
		CHECK_OPTION_GE(num_workers, -1);
		return true;
	}

  void PartialMapperController::Stop() {
    if (!mapper_->IsFinished()) {
      mapper_->Stop();
    }

    Thread::Stop();
  }

	void PartialMapperController::Run() {
		//////////////////////////////////////////////////////////////////////////////
		// Reconstruct clusters
		//////////////////////////////////////////////////////////////////////////////

		PrintHeading1("Reconstructing clusters");
		
		std::cout << "Reading images..." << std::endl;
		std::unordered_map<image_t, std::string> image_id_to_name;
		std::string slash = "/";
		
		Database database(options_.database_path);
		const auto images = database.ReadAllImages();

		//fclose(lock);
		for (const auto& image : images) {
			image_id_to_name.emplace(image.ImageId(), image.Name());
		}

		// Determine the number of workers and threads per worker.
		const int kMaxNumThreads = -1;
		const int num_eff_threads = GetEffectiveNumThreads(kMaxNumThreads);
		const int kDefaultNumWorkers = 8;
		const int num_eff_workers =
			options_.num_workers < 1
			? std::min(1, std::min(kDefaultNumWorkers, num_eff_threads))
			: options_.num_workers;
		const int num_threads_per_worker =
			std::max(1, num_eff_threads / num_eff_workers);

		//get cluster
		ReconstructionManager reconstruction_manager;
		int currentIndex = options_.current_index;

		const auto clusterFile = options_.cluste_path + slash + "cluster" + int2str(currentIndex) + ".txt";
		std::cout << "clusterFile:" << clusterFile << std::endl;
		std::ifstream file(clusterFile);
		CHECK(file.is_open()) << clusterFile;
		std::string line;
		std::string item;
		std::getline(file, line);
		StringTrim(&line);
		if (line == "") {
			std::cout << "ERROR: cluster is empty" << std::endl;
			return;
		}
		SceneClustering::Cluster currentCluster;
		currentCluster.image_ids.clear();         //---------------
		std::vector<image_t> image_ids;
		std::vector<SceneClustering::Cluster> child_clusters;
		const std::vector<std::string> line_elems = StringSplit(line, ",");
		for (const auto image_id : line_elems) {
			image_t imgId = str2int(image_id);
			currentCluster.image_ids.insert(imgId);       //------------
		}
		//currentCluster.image_ids = image_ids;         //------------
		file.close();

		IncrementalMapperOptions incre_options = mapper_options_;
		incre_options.max_model_overlap = 3;
		incre_options.init_num_trials = options_.init_num_trials;
		incre_options.num_threads = num_threads_per_worker;
		//incre_options.num_threads = 1;
		incre_options.lock_path = options_.job_path;

		for (const auto image_id : currentCluster.image_ids) {
			incre_options.image_names.insert(image_id_to_name.at(image_id));
		}

		/*IncrementalMapperController mapper(&incre_options, options_.image_path, options_.database_path, &reconstruction_manager);
    mapper.Start();
    mapper.Wait();*/

    mapper_.reset(new IncrementalMapperController(&incre_options, options_.image_path, options_.database_path, &reconstruction_manager));
    mapper_->Start();
    mapper_->Wait();
    if (mapper_->IsStopped()) {
      return;
    }
		//mapper.jobfile = m_file;
		
		std::cout << "back from incremental ..." << std::endl;
		writeReconstrctionToFile(reconstruction_manager);
		std::cout << "write file finish ..." << std::endl;
		std::cout << "delete temp db ..." << options_.database_path << std::endl;
	}

	bool PartialMapperController::writeReconstrctionToFile(ReconstructionManager &manage) {
		manage.Write(options_.cluste_path, &option_manager_);
		return true;
	}
}

