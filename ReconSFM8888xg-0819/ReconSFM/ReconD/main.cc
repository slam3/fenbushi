#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>

#include "base/similarity_transform.h"
#include "controllers/bundle_adjustment.h"
#include "estimators/coordinate_frame.h"
#include "feature/extraction.h"
#include "feature/matching.h"
#include "feature/utils.h"
#include "retrieval/visual_index.h"
#include "ui/main_window.h"
#include "util/opengl_utils.h"
#include "util/version.h"

using namespace sfmrecon;

#ifdef CUDA_ENABLED
const bool kUseOpenGL = false;
#else
const bool kUseOpenGL = true;
#endif
int main(int argc, char** argv) {

  InitializeGlog(argv);
  OptionManager options;
  options.AddAllOptions();
  options.AddMapperOptions();


#if (QT_VERSION >= QT_VERSION_CHECK(5, 6, 0))
  QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif
  Q_INIT_RESOURCE(resources);
  QApplication app(argc, argv);
  MainWindow main_window(options);
  main_window.show();

  return app.exec();

 // return ShowHelp(commands);
}
