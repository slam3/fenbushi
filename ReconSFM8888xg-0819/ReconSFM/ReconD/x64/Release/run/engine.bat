@echo off
setlocal enabledelayedexpansion
set YYYYmmdd=%date:~0,4%%date:~5,2%%date:~8,2%
set hhmmss=%time:~0,2%%time:~3,2%%time:~6,2%
set "hhmmss=%hhmmss: =0%"
set aFile=enginelog-%YYYYmmdd%-%hhmmss%.txt
echo %aFile%
echo "input engine.txt:"
for /f "tokens=1,2 delims=#" %%i in (.\conf.txt) do (
 echo ----------------------
 echo %%i
 echo %%j
 if %%i==outputPath (
   set pPath=%%j
 )
)
..\SparseRecon.exe -t 2 -p .\conf.txt > !pPath!\%aFile%

PAUSE