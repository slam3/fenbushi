set cxmc=SparseRecon.exe
 
:START_CHECK
tasklist | findstr "%cxmc%" || goto STARTPRO
 @echo process is running
 
ping -n 10 127.0.0.1 > nul
goto START_CHECK
 
 
:STARTPRO
@echo ------------------------clear engine...------------------------------
Call clear.bat
@echo ------------------------clear engine finish.------------------------
@echo ------------------------rollback engine...------------------------------
Call rollback.bat
@echo ------------------------rollback engine finish.------------------------
@echo ------------------------start process...------------------------
start cmd /k engine.bat
 
ping -n 10 127.0.0.1 > nul
goto START_CHECK
