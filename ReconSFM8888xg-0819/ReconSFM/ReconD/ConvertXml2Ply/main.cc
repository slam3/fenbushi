#include <base/reconstruction.h>
#include <util/string.h>
#include <base/point_cloud_filter.h>

void Run_0() {
  std::string at_file_path = "E:\\data\\test_result\\v1.2.3f\\neimeng-gps-gcp-5487-28\\neimeng-gps-gcp-5487-28_AT\\neimeng-gps-gcp-5487-28_GPS.xml";
  std::string image_path = "E:\\data\\neimeng-gps-gcp-5487-28\\photo";
  //std::string ply_path = "E:\\data\\test_result\\v1.2.0f\\neimeng-gps-gcp-300-3\\neimeng-gps-gcp-300-3_AT\\neimeng-gps-gcp-300-3_GPS.ply";

  at_file_path = sfmrecon::StringReplace(at_file_path, "\\", "/");
  image_path = sfmrecon::StringReplace(image_path, "\\", "/");
  std::string ply_path = sfmrecon::StringReplace(at_file_path, ".xml", ".ply");
  sfmrecon::Reconstruction reconstruction;
  std::cout << "begin load at file" << std::endl;
  reconstruction.LoadFromXMLFile(at_file_path, image_path);
  std::cout << "load at file success" << std::endl;
  reconstruction.ExportPLY(ply_path);
  std::cout << "export ply file success" << std::endl;
}

void Run_1() {
  std::string at_file_path = "E:\\data\\test_result\\v1.2.3f\\neimeng-gps-gcp-5487-28\\neimeng-gps-gcp-5487-28_AT\\neimeng-gps-gcp-5487-28_GPS.xml";
  std::string image_path = "E:\\data\\neimeng-gps-gcp-5487-28\\photo";
  std::string filter_path = "E:\\data\\test_result\\v1.2.3f\\neimeng-gps-gcp-5487-28\\neimeng-gps-gcp-5487-28_AT\\neimeng-gps-gcp-5487-28_GPS_filter.xml";
  at_file_path = sfmrecon::StringReplace(at_file_path, "\\", "/");
  image_path = sfmrecon::StringReplace(image_path, "\\", "/");
  filter_path = sfmrecon::StringReplace(filter_path, "\\", "/");

  sfmrecon::Reconstruction reconstruction;
  std::cout << "begin load at file" << std::endl;
  reconstruction.LoadFromXMLFile(at_file_path, image_path);
  std::cout << "load at file success" << std::endl;

  sfmrecon::PointCloudFilter::Filter(&reconstruction);

  reconstruction.ExportGPSXMLFile(filter_path, image_path);
}

int main() {
  Run_1();
}